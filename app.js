/**
 * Copyright 2015 EK3 Technologies Inc.
 */
var SitePreview = require('./src/SitePreview'),
	cors = require('cors'), // Cross Origin
	winston = require('winston'),
	express = require('express'),
	app = express(),
	http = require('http').Server(app),
	io = require('socket.io')(http); // To view connected users and keep their sessions

app.use(express.static('public/views'));
app.use(express.static('public/controllers'));
app.use(express.static('public/opt'));
app.use(express.static('public/downloaded_assets'));
app.use(express.static('bower_components'));
app.use(cors()); // Allow cross origin domain requests

winston.add(winston.transports.File, { filename: '/var/log/sitepreview.log' });

winston.level = 'debug'; // Change this to pull from a file or parameter

var sitePreview = new SitePreview(io, app, http);
if( sitePreview ) {
	sitePreview.initialize('kiwi'); // Hardcoded kiwi for testing
	sitePreview.start();
} else {
	winston.debug('Error instantiating SitePreview()');
}