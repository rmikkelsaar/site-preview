/*global
  EventService
  ConfigurationFactory
  SchedulerFactory
  RegionPlayerFactory
  instance
  inArray
  */

/**
 * The playback manager manages the overall playback at the player
 * @param YUI Y The instantiated YUI instance
 */
function PlaybackManager(Y) {
  PlaybackManager.Y = Y;
  PlaybackManager.message_service;
  this.scheduler;
  this.interval;
}
PlaybackManager.pluginTypes = [];
PlaybackManager.loadedPlugins = [];
PlaybackManager.playbackEvents = [];
PlaybackManager.blockSchedule = false;

/**
 * Static method to gain access to YUI instance
 * @return YUI The instantiated YUI instance
 */
PlaybackManager.getYInstance = function() {
  return this.Y;
};

/**
 * Static method for debug logging
 * @param string messsage The log message
 * @param string type The log type
 */
PlaybackManager.log = function(message, type) {
	if(type === 'debug' && ConfigurationFactory.getInstance().getValue('debug', 'no') === 'no') { return; }
  var now = new Date();
  message = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + " - " + message;
  PlaybackManager.getYInstance().log(message, type);
};

/**
 * Sends a customized event to anybody subscribing
 * @param string name The event name
 * @param mixed arg_1 Optional argument to the event
 */
PlaybackManager.sendEvent = function(name, arg_1, arg_2, arg_3, arg_4) {
  PlaybackManager.getYInstance().fire(name, arg_1, arg_2, arg_3, arg_4);
};

/**
 * Sends a targeted message through the message service
 * @param string target The message target
 * @param string topic The message topic
 * @param object data The data to pass with the message.
 */
PlaybackManager.sendMessage = function(target, topic, data) {
  PlaybackManager.message_service.sendTo(target, topic, data);
};

/**
 * Sends a broadcast message through the message service
 * @param string topic The message topic
 * @param object data The data to pass with the message.
 */
PlaybackManager.broadcastMessage = function(topic, data) {
  PlaybackManager.message_service.broadcast(topic, data);
};

/**
 * Publishes an event under the playback manager event manager (global)
 * @param name
 * @param options
 * @return
 */
PlaybackManager.publishEvent = function(name, options) {
  PlaybackManager.getYInstance().publish(name, options);
};

/**
 * init() - Initializes the player determining configuration and resource capabilities.
 * This should only be called once per playback session.
 * @return boolean True on success and exception on failure.
 */
PlaybackManager.prototype.init = function() {
  //INIT ENV
  PlaybackManager.log("PlaybackManager::init", "info");
  //Is there not a better way to do timers?
  PlaybackManager.publishEvent("global::timer", { broadcast: 2 });

  //Publish events - scheduler
  PlaybackManager.publishEvent("scheduler::new", { broadcast: 2 });
  PlaybackManager.publishEvent("scheduler::remove", { broadcast: 2 });
  PlaybackManager.publishEvent("scheduler::no_schedule", { broadcast: 2 });
  PlaybackManager.publishEvent("scheduler::time_event", { broadcast: 2 });

  //region_player
  PlaybackManager.publishEvent("region_player::new", { broadcast: 2 });
  PlaybackManager.publishEvent("region_player::remove", { broadcast: 2 });
  PlaybackManager.publishEvent("region_player::no_playlist", { broadcast: 2 });
  PlaybackManager.publishEvent("region_player::looped", { broadcast: 2 });
  PlaybackManager.publishEvent("region_player::time_event", { broadcast: 2 });
  PlaybackManager.publishEvent("region_player::destroy", { broadcast: 2 });

  //player
  PlaybackManager.publishEvent("player::play", { broadcast: 2 });
  PlaybackManager.publishEvent("player::prepare", { broadcast: 2 });
  PlaybackManager.publishEvent("player::almost_done", { broadcast: 2 });
  PlaybackManager.publishEvent("player::complete", { broadcast: 2 });
  PlaybackManager.publishEvent("player::stop", { broadcast: 2 });
  PlaybackManager.publishEvent("player::error", { broadcast: 2 });
  PlaybackManager.publishEvent("player::time_event", { broadcast: 2 });

  //media
  PlaybackManager.publishEvent("media::extend", { broadcast: 2 });
  PlaybackManager.publishEvent("media::subscribe", { broadcast: 2 });
  PlaybackManager.publishEvent("media::send", { broadcast: 2 });
  PlaybackManager.publishEvent("media::broadcast", { broadcast: 2 });
  PlaybackManager.publishEvent("media::add_dp_notify", { broadcast: 2 });

  //Message
  PlaybackManager.publishEvent("message_service::connect", { broadcast: 2 });
  PlaybackManager.publishEvent("message_service::broadcast", { broadcast: 2 });
  PlaybackManager.publishEvent("message_service::message", { broadcast: 2 });

  this.initSupportModules();

  PlaybackManager.message_service = new EventService();
  PlaybackManager.message_service.initialize(ConfigurationFactory.getInstance().getValue('screen_name'));
};

PlaybackManager.prototype.initSupportModules = function() {
  var config = ConfigurationFactory.getInstance();
  var support_modules = config.getValue('support_modules', []);
  for(var i=0;i<support_modules.length;i++) {

    if(!PlaybackManager.pluginTypes[support_modules[i]]) {
      PlaybackManager.log("Unable to load support module: " + support_modules[i], "warn");
      continue;
    }

    var concrete = PlaybackManager.pluginTypes[support_modules[i]];
    PlaybackManager.log("PlaybackManager::new plugin: " + concrete, "info");
    eval("var instance = new " + concrete + "()");
    instance.initialize();
    PlaybackManager.loadedPlugins.push(instance);
  }
};

PlaybackManager.prototype.checkSchedule = function() {
  this.scheduler.loadCurrentSchedule();
};

PlaybackManager.prototype.getSchedule = function() {
  return this.scheduler;
};

/**
 * Starts playback from an initialized playback manager
 */
PlaybackManager.prototype.start = function() {
  //START PLAYING
  PlaybackManager.log("PlaybackManager::start", "info");
  this.scheduler = SchedulerFactory.loadScheduler();
  this.scheduler.loadCurrentSchedule();
  window.playback = this;
  //this.interval = window.setInterval('PlaybackManager.sendEvent("global::timer")', 1000);
};

/**
 * Name of this object - used for debugging.
 * @return The object name
 */
PlaybackManager.prototype.toString = function() {
  return "PlaybackManager";
};
