/*global
  BaseData
  */


/**
 * Media information
 * @param hash data_object Optional contruction parameter with properly formatted intialization data
 */
function MediaData(data_object) {
  this.length;
  this.playback_command;
  this.media_asset_id;
  if(data_object.values) { data_object.meta_values = data_object.values; }
  if(data_object) {
    this.load(data_object.id, data_object.meta_values, data_object.media_asset_id, data_object.length, data_object.playback_command);
  }
}
MediaData.prototype = new BaseData(); //Javascript inheritance

MediaData.prototype.getAssetId = function() {
  return this.media_asset_id;
};

MediaData.prototype.getLength = function() {
  return this.length;
};

/**
 * Returns the media command that can be used to play this media
 * @return string The player command for the media asset
 */
MediaData.prototype.getPlayCommand = function() {
  return this.playback_command;
};

/**
 * Initializes the media data object
 * @param id
 * @param values
 * @param media_path
 * @param media_type
 * @param play_command
 * @return
 */
MediaData.prototype.load = function(id, values, asset_id, length, play_command) {
  this.id = id;
  if(values) { this.values = values; }
  this.media_asset_id = asset_id;
  this.length = length;
  this.playback_command = play_command;
};

/**
 * Outputs a string representation of the media - used for debugging
 * @return string The string rep of the data
 */
MediaData.prototype.toString = function() {
  var string = "MediaData\n";
  string += "\tid=" + this.id + "\n";
  string += "\tasset_id=" + this.media_asset_id + "\n";
  string += "\tlength=" + this.length + "\n";
  string += "\tplay_command=" + this.playback_command + "\n";
  return string;
};
