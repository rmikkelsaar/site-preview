/*global
  ScheduleData
  TimeToPlayData
  RegionData
  PlaylistData
  MediaData
  ConfigurationFactory
  PlaybackManager
  inArray
  console
  */

/**
 * Impelements the data access using local JSON playscripts
 * @return
 */
function DataAccessJSON() {
  this.schedules;
  this.regions;
  this.times;
  this.playlists;
  this.medias;
}

/**
 * Loads and initializes all schedule data objects based on the json configuration
 * @return ScheduleData[] The array of schedule data for this playback instance
 */
DataAccessJSON.prototype.loadSchedules = function() {
  if(!this.schedules) {
    this.schedules = this._loadData('schedule.json', ScheduleData);
  }
  return this.schedules;
};

/**
 * Returns the list of times to plays that match the provided list of ids
 * @param integer ttp_ids[] The array of time to play ids
 * @return TimeToPlayData[] The array of matching times
 */
DataAccessJSON.prototype.loadTimesToPlay = function(ttp_ids) {
  if(!this.times) {
    this.times = this._loadData('time_to_play.json', TimeToPlayData);
  }
  return this._filter(this.times, ttp_ids);
};

/**
 * Returns the list of regions matching the provided ids
 * @param integer region_ids[] The array of regions to load
 * @return RegionData[] The array of region data
 */
DataAccessJSON.prototype.loadRegions = function(region_ids) {
  if(!this.regions) {
    this.regions = this._loadData('region.json', RegionData);
  }
  return this._filter(this.regions, region_ids);
};

/**
 * Returns the list of playlists for the specified schedule and region id
 * @param integer schedule_id The id of the schedule to load for
 * @param integer region_id The id of the region to load for
 * @return PlaylistData[] The array of matching playlists
 */
DataAccessJSON.prototype.loadPlaylistsByScheduleRegion = function(schedule_id, region_id) {
  if(!this.playlists) {
    this.playlists = this._loadData('playlist.json', PlaylistData);
  }
  var match = [];
  for(var i=0;i<this.playlists.length;i++) {
    if(this.playlists[i].getScheduleId() === schedule_id &&
       this.playlists[i].getRegionId() === region_id) {
      match.push(this.playlists[i]);
    }
  }
  return match;
};

/**
 * Returns the media details tha tmatch the provided media id
 * @param integer media_id The id of the media asset to load
 * @return MediaData The media to load
 */
DataAccessJSON.prototype.loadMediaById = function(media_id) {
  if(!this.medias) {
    this.medias = this._loadData('media.json', MediaData);
  }
  for(var i=0;i<this.medias.length;i++) {
    if(this.medias[i].getId() === media_id) {
      return this.medias[i];
    }
  }
};

/**
 * Returns the list of matching media assets in the same order as requested
 * @param integer[] media_ids The list of media assets to load
 * @return MediaData[] The array of media assets.
 */
DataAccessJSON.prototype.loadMediaByIds = function(media_ids) {
  if(!this.medias) {
    this.medias = this._loadData('media.json', MediaData);
  }
  var matching_media = [];
  for(var j=0;j<media_ids.length;j++) {
    for(var i=0;i<this.medias.length;i++) {
      if(this.medias[i].getId() === media_ids[j]) {
        matching_media.push(this.medias[i]);
        //console.log(this.medias[i]);
        break;
      }
    }
  }
//  PlaybackManager.log("DataAccessJSON::loadMediaByIds:matching_media","debug");
//  console.log(matching_media);
  return matching_media;
};

/**
 * Protected method to load data from the service using JSON
 */
DataAccessJSON.prototype._loadData = function(data_name, DataObject) {
  var config = ConfigurationFactory.getInstance();
  var base_path = config.getValue('playscript_path', '');
  var url = base_path + data_name + '?serial=' + Date.now();
  var request = PlaybackManager.getYInstance().io(url, { sync: true });
  try {
    var data = PlaybackManager.getYInstance().JSON.parse(request.responseText);
    var data_objects = [];
    for(var i=0;i<data.length;i++) {
      data_objects.push(new DataObject(data[i]));
    }
    if ( data_name === "schedule.json" ){
      PlaybackManager.log("**Schedules**","debug");
      console.log(data_objects);
    } if ( data_name === "region.json" ){
      PlaybackManager.log("**Regions**","debug");
      console.log(data_objects);
    } if ( data_name === "time_to_play.json" ){
      PlaybackManager.log("**Time_To_Play**","debug");
      console.log(data_objects);
    } if ( data_name === "playlist.json" ){
      PlaybackManager.log("**Playlists**","debug");
      console.log(data_objects);
    }
    return data_objects;
  } catch(e) {
    PlaybackManager.log("Error in parsing " + data_name, "error");
    return [];
  }
};

/**
 * Protected method to filter based on ids
 */
DataAccessJSON.prototype._filter = function(object_array, object_ids) {
  var match = [];
  for(var i=0;i<object_array.length;i++) {
    if(inArray(object_array[i].getId(), object_ids)) {
      match.push(object_array[i]);
    }
  }
  return match;
};
