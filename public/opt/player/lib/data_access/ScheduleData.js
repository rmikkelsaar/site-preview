/*global
  BaseData
  DataAccessFactory
  */

/**
 * Schedule data contains the high level dates and dayparts that will drive what regions are being
 * display currently. This obejct extends the base data object
 * @param hash data_object Optional contruction parameter with properly formatted intialization data
 */
function ScheduleData(data_object) {
  this.start_date;
  this.end_date;
  this.layout_height;
  this.layout_width;
  this.time_to_play_ids;
  this.region_ids;
  if(data_object) {
    this.load(data_object.id, data_object.meta_values, data_object.start_date, data_object.end_date, data_object.layout_width, data_object.layout_height, data_object.time_to_play_ids, data_object.region_ids);
  }
}
ScheduleData.prototype = new BaseData(); //Javascript inheritance

/**
 * Returns the start date of this schdule as a Date object
 * @return Date The start date
 */
ScheduleData.prototype.getStartDate = function() {
  return this.start_date;
};

/**
 * Returns the end date of this schedule as a Date object
 * @return Date the end date
 */
ScheduleData.prototype.getEndDate = function() {
  return this.end_date;
};

/**
 * Returns the width of the layout for this schedule
 * @return integer The width of the layout in pixels
 */
ScheduleData.prototype.getLayoutWidth = function() {
  return this.layout_width;
};

/**
 * Returns the height of the layout for this schedule
 * @return integer The height of the layout in pixels
 */
ScheduleData.prototype.getLayoutHeight = function() {
  return this.layout_height;
};

/**
 * Retuers the list of times to play fro this schedule
 * @return TimeToPlayData[] The array of times to play associated with this schedule
 */
ScheduleData.prototype.getTimeToPlays = function() {
  return DataAccessFactory.getInstance().loadTimesToPlay(this.time_to_play_ids);
};

/**
 * Returns the regions that will be played within this schedule
 * @return RegionData[] The array of regions
 */
ScheduleData.prototype.getRegions = function() {
  return DataAccessFactory.getInstance().loadRegions(this.region_ids);
};

/**
 * Initalizes the the schedule data
 * @param id
 * @param values
 * @param start_date
 * @param end_date
 * @param time_to_play_ids
 * @param region_ids
 */
ScheduleData.prototype.load = function(id, values, start_date, end_date, layout_width, layout_height, time_to_play_ids, region_ids) {
  this.id = id;
  if(values && values.length) { this.values = values; }
  this.start_date = new Date(start_date);
  this.end_date = new Date(end_date);
  this.layout_width = parseInt(layout_width,10);
  this.layout_height = parseInt(layout_height,10);
  this.time_to_play_ids = time_to_play_ids;
  this.region_ids = region_ids;
};

/**
 * Outputs a string representation of the schedule - used for debugging
 * @return string The string rep of the data
 */
ScheduleData.prototype.toString = function() {
  var string = "ScheduleData\n";
  string += "\tid=" + this.id + "\n";
  string += "\tstart_date=" + this.start_date + "\n";
  string += "\tend_date=" + this.end_date + "\n";
  string += "\tlayout_width=" + this.layout_width + "\n";
  string += "\tlayout_height=" + this.layout_height + "\n";
  string += "\ttime_to_play_ids=" + this.time_to_play_ids.join(',') + "\n";
  string += "\tregion_ids=" + this.region_ids.join(',') + "\n";
  return string;
};
