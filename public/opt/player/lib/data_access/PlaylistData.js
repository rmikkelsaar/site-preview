/*global
  BaseData
  DataAccessFactory
  */

/**
 * Playlists define the list of media assets that will play within a schedule region
 * @param hash data_object Optional contruction parameter with properly formatted intialization data
 */
function PlaylistData(data_object) {
  this.schedule_id;
  this.region_id;
  this.events;
  this.start_date;
  this.end_date;
  this.assignment_ids = [];
  this.time_to_play_ids = [];
  if(data_object) {
    this.load(data_object.id, data_object.meta_values, data_object.schedule_id, data_object.region_id, data_object.events, data_object.start_date, data_object.end_date, data_object.assignment_ids, data_object.time_to_play_ids);
  }
}
PlaylistData.prototype = new BaseData(); //Javascript inheritance

/**
 * Returns the id of the schedule that this playlist is for
 * @return integer The schedule id
 */
PlaylistData.prototype.getScheduleId = function() {
  return this.schedule_id;
};

/**
 * Returns the id of the region that this playlist is for
 * @return integer The region id
 */
PlaylistData.prototype.getRegionId = function() {
  return this.region_id;
};

/**
 * Returns the list of any events this playlist applies to
 * @return string[] The array of events that must be active
 */
PlaylistData.prototype.getEvents = function() {
  return this.events;
};

/**
 * Returns the list of assignments for this playlist
 * @return integer[] The array of assignments
 */
PlaylistData.prototype.getAssignmentIds = function() {
  return this.assignment_ids;
};

/**
 * Returns the assignment at the requested index
 * @param integer index The index of the assignment to retrieve
 * @return integer The assignment id at that index or empty if none
 */
PlaylistData.prototype.getAssignmentAtIndex = function(index) {
  if(this.assignment_ids[index]) {
    return this.assignment_ids[index];
  }
};

/**
 * Returns the start date for this playlist
 * @return Date The start date of the playlist
 */
PlaylistData.prototype.getStartDate = function() {
  return this.start_date;
};

/**
 * Returns the end date for this playlist
 * @return Data The end date of the playlist
 */
PlaylistData.prototype.getEndDate = function() {
  return this.end_date;
};

/**
 * Retuers the list of times to play for the playlsit
 * @return TimeToPlayData[] The array of times to play
 */
PlaylistData.prototype.getTimeToPlays = function() {
  return DataAccessFactory.getInstance().loadTimesToPlay(this.time_to_play_ids);
};

/**
 * The playlist data loader
 * @param id
 * @param values
 * @param schedule_id
 * @param region_id
 * @param events
 * @param assignments -
 * @return
 */
PlaylistData.prototype.load = function(id, values, schedule_id, region_id, events, start_date, end_date, assignment_ids, ttp_ids) {
  this.id = id;
  if(values) { this.values = values; }
  this.schedule_id = schedule_id;
  this.region_id = region_id;
  this.start_date = new Date(start_date);
  this.end_date = new Date(end_date);
  this.events = events;
  this.assignment_ids = assignment_ids;
  this.time_to_play_ids = ttp_ids;
};

/**
 * Outputs a string representation of the playlist - used for debugging
 * @return string The string rep of the data
 */
PlaylistData.prototype.toString = function() {
  var string = "PlaylistData\n";
  string += "\tid=" + this.id + "\n";
  string += "\tschedule_id=" + this.schedule_id + "\n";
  string += "\tregion_id=" + this.region_id + "\n";
  string += "\tevents=" + this.events.join(',') + "\n";
  string += "\tstart_date=" + this.start_date + "\n";
  string += "\tend_date=" + this.end_date + "\n";
  string += "\tassignment_ids=" + this.assignment_ids.length + "\n";
  string += "\tttp_ids=" + this.time_to_play_ids.join(',') + "\n";
  return string;
};
