/*global
  PlaybackManager
  */

/**
 * Simple shared object taht all data object are extend from
 */
function BaseData() {
  this.id = 0;
  this.values = [];
  this.json_encode = "";
}

/**
 * Retruns the id of the data object
 * @return integer The data id for the object
 */
BaseData.prototype.getId = function() {
  return this.id;
};

/**
 * Returns the requested meta value or the provided default if not found
 * @param name The meta value to load
 * @param default_value The default value to return if no information is found
 * @return string The requested value or default
 */
BaseData.prototype.getValue = function(name, default_value) {
  if(this.values[name]) { return this.values[name]; }
  return default_value;
};

/**
 * Prints out all values for the current object
 */
BaseData.prototype.stringValues = function() {
  var value = "";
  for(var i in this.values) {
    value += (i + "=" + this.values[i] + "\n");
  }
  return value;
};

BaseData.prototype.toJson = function() {
  if(this.json_encode.length === 0) {
    this.json_encode = PlaybackManager.getYInstance().JSON.stringify(this);
  }
  return this.json_encode;
};
