/*global
  BaseData
*/

/**
 * Time to play data for distinct start, end and day units. Provides some simple helpers to help faciliate validation and checking
 * @param hash data_object Optional contruction parameter with properly formatted intialization data
 */
function TimeToPlayData(data_object) {
  this.start_time;
  this.end_time;
  this.days_of_week;

  this.start_value = [];
  this.end_value = [];
  this.day_value = [];

  if(data_object) {
    this.load(data_object.id, data_object.meta_values, data_object.start_time, data_object.end_time, data_object.days_of_week);
  }
}
TimeToPlayData.prototype = new BaseData(); //Javascript inheritance

/**
 * Returns the start time for this time to play
 * @return string The full start time
 */
TimeToPlayData.prototype.getStartTime = function() {
  return this.start_time;
};

/**
 * Returns the end time for this time to play
 * @return string The full end time
 */
TimeToPlayData.prototype.getEndTime = function() {
  return this.end_time;
};

/**
 * Returns the string array for the days of the week
 * @return string[] The array of days using three letters
 */
TimeToPlayData.prototype.getDaysOfWeek = function() {
  return this.days_of_week;
};

/**
 * Returns the start hour
 * @return integer The start hour 00 -> 23
 */
TimeToPlayData.prototype.getStartHour = function() {
  if(this.start_value[0]) { return parseInt(this.start_value[0], 10); }
};

/**
 * Returns the start minute
 * @return integer The start minute 00 -> 59
 */
TimeToPlayData.prototype.getStartMinute = function() {
  if(this.start_value[1]) { return parseInt(this.start_value[1], 10); }
};

/**
 * Returns the start seconds
 * @return integer The start seconds 00 -> 59
 */
TimeToPlayData.prototype.getStartSecond = function() {
  if(this.start_value[2]) { return parseInt(this.start_value[2], 10); }
};

/**
 * Returns the end hour
 * @return integer The end hour 00 -> 23
 */
TimeToPlayData.prototype.getEndHour = function() {
  if(this.end_value[0]) { return parseInt(this.end_value[0], 10); }
};

/**
 * Returns the end minute
 * @return integer The end minute 00 -> 59
 */
TimeToPlayData.prototype.getEndMinute = function() {
  if(this.end_value[1]) { return parseInt(this.end_value[1], 10); }
};

/**
 * Returns the end seconds
 * @return integer The end seconds 00 -> 59
 */
TimeToPlayData.prototype.getEndSecond = function() {
  if(this.end_value[2]) { return parseInt(this.end_value[2], 10); }
};

/**
 * Returns the numeric value of the start time
 * @return integer The numeric value of the start time
 */
TimeToPlayData.prototype.getStartValue = function() {
  return this.getStartHour() * 3600 + this.getStartMinute() * 60 + this.getStartSecond();
};

/**
 * Returns the numeric value of the end time
 * @return integer The numeric value of the end time
 */
TimeToPlayData.prototype.getEndValue = function() {
  return this.getEndHour() * 3600 + this.getEndMinute() * 60 + this.getEndSecond();
};

/**
 * Returns the day of the week values for this time to play
 * @return integer[] The array of time to play day values (0 = sunday)
 */
TimeToPlayData.prototype.getDayOfWeekValues = function() {
  return this.day_value;
};

/**
 * Initalizes the the time data
 * @param id
 * @param values
 * @param start_date
 * @param end_date
 * @param time_to_play_ids
 * @param region_ids
 */
TimeToPlayData.prototype.load = function(id, values, start_time, end_time, days_of_week) {
  this.id = id;
  if(values) { this.values = values; }
  this.start_time = start_time;
  this.end_time = end_time;
  this.days_of_week = days_of_week;

  this.start_value = start_time.split(':');
  this.end_value = end_time.split(':');
  for(var i=0;i<this.days_of_week.length;i++) {
    if(this.days_of_week[i].toLowerCase() === 'mon'){ this.day_value.push(1); }
    else if(this.days_of_week[i].toLowerCase() === 'tue') { this.day_value.push(2); }
    else if(this.days_of_week[i].toLowerCase() === 'wed') { this.day_value.push(3); }
    else if(this.days_of_week[i].toLowerCase() === 'thu') { this.day_value.push(4); }
    else if(this.days_of_week[i].toLowerCase() === 'fri') { this.day_value.push(5); }
    else if(this.days_of_week[i].toLowerCase() === 'sat') { this.day_value.push(6); }
    else if(this.days_of_week[i].toLowerCase() === 'sun') { this.day_value.push(0); }
  }
};

/**
 * Returns the string represnetion of the data object
 * @return string The string rep for this time to play
 */
TimeToPlayData.prototype.toString = function() {
  var string = "TimeToPlayData\n";
  string += "\tid=" + this.id + "\n";
  string += "\tstart_time=" + this.start_time + "\n";
  string += "\tstart_value=" + this.start_value + "\n";
  string += "\tend_time=" + this.end_time + "\n";
  string += "\tend_value=" + this.end_value + "\n";
  string += "\tdays_of_week=" + this.days_of_week.join(',') + "\n";
  string += "\tday_value=" + this.day_value.join(',') + "\n";
  return string;
};
