/*global
  ConfigurationFactory
  PlaybackManager
  */

/**
 * The data access factory instantiates the concrete access based on the configuration
 */
function DataAccessFactory() {
  this.instance = null;
}

/**
 * Retrieves the current configuration instance object (Singleton)
 */
DataAccessFactory.getInstance = function() {
  if(this.instance === null || this.instance === undefined) {
    var config = ConfigurationFactory.getInstance();
    var concrete = config.getValue('data_access', 'DataAccessJSON');
    PlaybackManager.log("DataAccessFactory::getInstance - loading new " + concrete, "info");
    eval("this.instance = new " + concrete + "()");
  }
  return this.instance;
};
