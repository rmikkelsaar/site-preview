/*global
  BaseData
  */

/**
 * Access for region data information
 * @param hash data_object Optional contruction parameter with properly formatted intialization data
 */
function RegionData(data_object) {
  this.z_index;
  this.top;
  this.left;
  this.width;
  this.height;
  this.player_type;

  if(data_object) {
    this.load(data_object.id, data_object.meta_values, data_object.top, data_object.left, data_object.width, data_object.height, data_object.zindex, data_object.playback_type);
  }
}
RegionData.prototype = new BaseData(); //Javascript inheritance

/**
 * Returns the top pixel index of the region
 * @return integer The top pixel for the region
 */
RegionData.prototype.getTop = function() {
  return this.top;
};

/**
 * Regions the left pixel index of the regino
 * @return integer The left pixel of the region
 */
RegionData.prototype.getLeft = function() {
  return this.left;
};

/**
 * Returns the width of the region
 * @return integer The width of the region
 */
RegionData.prototype.getWidth = function() {
  return this.width;
};

/**
 * Returns the height of the region
 * @return integer The height of the region
 */
RegionData.prototype.getHeight = function() {
  return this.height;
};

/**
 * Returns the z index of the region
 * @return integer The z omdex of the region
 */
RegionData.prototype.getZIndex = function() {
  return this.z_index;
};

/**
 * Returns the type of player to instantiate for this region
 * @return string The name o fthe player to instantiate for the region
 */
RegionData.prototype.getPlayerType = function() {
  return this.player_type;
};

/**
 * Initializes the resource data
 * @param id
 * @param values
 * @param top
 * @param left
 * @param width
 * @param height
 * @param z_index
 * @param player_type
 * @return
 */
RegionData.prototype.load = function(id, values, top, left, width, height, z_index, player_type) {
  this.id = id;
  if(values) { this.values = values; }
  this.z_index = z_index;
  this.top = top;
  this.left = left;
  this.width = width;
  this.height = height;
  this.player_type = player_type;
};

/**
 * Returns the string represnetion of the data object
 * @return string The string rep for this region
 */
RegionData.prototype.toString = function() {
  var string = "RegionData\n";
  string += "\tid=" + this.id + "\n";
  string += "\ttop=" + this.top + "\n";
  string += "\tleft=" + this.left + "\n";
  string += "\twidth=" + this.width + "\n";
  string += "\theight=" + this.height + "\n";
  string += "\tz_index=" + this.z_index + "\n";
  string += "\tplayer_type=" + this.player_type + "\n";
  return string;
};
