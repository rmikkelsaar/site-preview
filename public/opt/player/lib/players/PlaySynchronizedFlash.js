/*global
  PlayFlash
  PlayFactory
  PlayVideo
  */

function PlaySynchronizedFlash() {
  this.video_name = 'sv';
  this.params = {
    play: "true",
    scale: "exactfit",
    loop: "false",
    wmode: "transparent"
  };
  this.peer;
}
PlaySynchronizedFlash.prototype = new PlayFlash(); //Javascript inheritance
//Add this player to the list of available types
PlayFactory.playTypes.SynchronizedFlash = 'PlaySynchronizedFlash';

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlaySynchronizedFlash.prototype.prepareRegion = function(region_node, assignment) {
  PlayVideo.prototype.prepareRegion.call(this, region_node, assignment);
  /*if(!this.peer) {
    this.initSynchronized();
    }*/
};

/**
 * Initializes the synchronized event for the video elem
 */
/*PlaySynchronizedFlash.prototype.initSynchronized = function() {
  if(ConfigurationFactory.getInstance().getValue('is_master') == 'yes') {
  var role = new wsync.Leader();
  role.minFollowers = 1;
  role.minFollowersTimeoutMs = 5000;
  } else {
  var role = new wsync.Follower();
  role.leaderTimeoutMs = 5000;
  }

  var synchronizer = new wsync.SynchronizedHtmlMedia(this.video_elem);
  var bridge = new wmsg.SocketIoEventChannel(new wmsg.SocketIoClient(), new wmsg.EventChannel()).bind();
  var peer = new wsync.SynchronizedPeer(role, synchronizer, bridge, this.region_data.getValue('name')).bind();
  bridge.client.onWebOpened.subscribe(function() { peer.join(); bridge.client.onWebOpened.unsubscribe(on_web_opened); });
  bridge.client.connect(ConfigurationFactory.getInstance().getValue('message_url'));
  this.peer = peer;
  }*/

/**
 * Name of this object - used for debugging.
 * @return
 */
PlaySynchronizedFlash.prototype.toString = function() {
  return "PlaySynchronizedFlash";
};
