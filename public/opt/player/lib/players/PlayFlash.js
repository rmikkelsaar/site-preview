/*global
  PlayFactory
  PlaybackManager
  swfobject
  getMediaPath
  inArray
  console
  */

/**
 * Plays back video media using HTML5 video tag
 */
function PlayFlash() {
  this.region_data;
  this.assignment_data;
  this.flash_elem;
  this.height;
  this.current = 0;
  this.flash_players;
  this.seenMedia;
  this.timers;
  this.prev_div = null;
  //  this.playTimer;
  this.params = {
    play: "true",
    scale: "exactfit",
    loop: "true",
    wmode: "transparent"
  };
}

//Add this player to the list of available types
PlayFactory.playTypes.Flash = 'PlayFlash';

//Stores a list of current players against their video element ids. This simplifies the event processing at the expense of cleanliness
PlayFlash.playerLookups = [];

/**
 * Intializes the video player with the appropriate data
 * @param RegionData region_data The region information for the target of this video
 * @param AssignmentData assignment_data The assignment information to play
 * @return
 */
PlayFlash.prototype.initialize = function(region_data) {
  PlaybackManager.log("PlayFlash::initialize", "debug");
  this.flash_players = [];
  this.timers = [];
  this.seenMedia = [];
  this.region_data = region_data;

  //Subscribe to events
  if(!this.on) {
    PlaybackManager.getYInstance().augment(PlayFlash, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  this.on('region_player::time_event', function(e) {
    if(e.details[0].getId() === this.region_data.getId() && this.flash_elem.event) {
      this.flash_elem.event(this.region_data.toJson(),
                            this.assignment_data.toJson(),
                            'region_player::time_event',
                            PlaybackManager.getYInstance().JSON.stringify({'time': e.details[2]}));
    }
  });
  this.on('media::extend', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId()) {
      this.clearTimers();
      this.setupTimers(e.details[2]);
    }
  });
  this.on('media::send', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId()) {
      PlaybackManager.log("PlayFlash.sendEvent: " + e.details[2] + ":" + this.region_data.getId() + ":" + this.assignment_data.getId() + ":" + e.details[3] + ":" + e.details[4] + ":" + e.details[5], "debug");
      PlaybackManager.sendEvent(e.details[2], this.region_data, this.assignment_data, e.details[3], e.details[4], e.details[5]);
    }
  });
  this.on('media::broadcast', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId()) {
      PlaybackManager.log("PlayFlash.broadcastEvent: " + e.details[2] + ":" + this.region_data.getId() + ":" + this.assignment_data.getId() + ":" + e.details[3] + ":" + e.details[4] + ":" + e.details[5], "debug");
      PlaybackManager.broadcastMessage(e.details[2], {'region': this.region_data, 'assignment': this.assignment_data, 'arguments': [e.details[3], e.details[4], e.details[5]] });
    }
  });
  this.on('media::subscribe', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId() && this.flash_elem.event) {
      PlaybackManager.log("PlayFlash.subscribe: " + e.details[2] + ":" + e.details[3], "debug");
      this.on(e.details[2], function(e) {
        this.flash_elem.event(this.region_data.toJson(),
                              this.assignment_data.toJson(),
                              e.type,
                              PlaybackManager.getYInstance().JSON.stringify(e.details));
      });
    }
  });
};

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayFlash.prototype.prepareRegion = function(region_node, assignment) {
  PlaybackManager.log("PlayFlash::prepareRegion", "debug");
  var self = this;
  this.assignment_data = assignment;
  //PlaybackManager.sendEvent('player::prepare', this.region_data, this.assignment_data);

  var div_id = 'fp_' + this.region_data.getId() + assignment.getId();
  var flash_elem = document.getElementById(div_id);
  if(!flash_elem) {
    region_node.append('<div style="width: 100%; height: 100%; border: none;" id="' + div_id + '"></div>');
    PlayFlash.playerLookups[div_id] = this; //Allows for event lookup from elems
  }

  var flashvars = {};
  var attributes = {};

  if ( this.seenMedia.length > 0 ) {
    flashvars = {lastMediaId: this.seenMedia.pop()};
  }
  this.seenMedia.push(this.assignment_data.getId());

  swfobject.embedSWF(getMediaPath(assignment), div_id, region_node.get('clientWidth'), region_node.get('clientHeight'), "9.0.0", '#000000', flashvars, this.params, attributes);

  flash_elem = document.getElementById(div_id);
  flash_elem.style.position = 'absolute';
  flash_elem.style.left = '0px';
  flash_elem.style.top = '0px';
  flash_elem.style.visibility = 'hidden';

  this.current_id = flash_elem.id;

  if (flash_elem ) {
    PlaybackManager.log("flash_elem:"+flash_elem.id, "debug");
    if ( this.prev_elem ) {
      PlaybackManager.log("this.prev_elem:"+this.prev_elem.id, "debug"); }
  }

  if(this.last_id) {
    if( this.last_id !== this.current_id ) {
      var delay = 1000;
      var last_id = self.last_id;
      window.setTimeout(function() {
        PlaybackManager.log("PlayFlash::prepareRegion:removing element " +self.last_id, "debug");
        self.remove(last_id);
      }, delay);
    }
  } else {
    PlaybackManager.log("PlayFlash::prepareRegion: continue using object", "debug");
  }

  // for use else where
  this.flash_elem = flash_elem;
  this.prev_elem = this.flash_elem;
  //  for when we return here
  this.last_id = this.prev_elem.id;

  PlaybackManager.log("PlayFlash::PREPARE", "debug");
};

/**
 * Disables any elements currently on the screen for this player
 */
PlayFlash.prototype.hide = function(div_id) {
  PlaybackManager.log("PlayFlash::hide", "debug");
  if (!div_id){
    this.flash_elem.style.visibility = 'hidden';//display = 'none';
  } else {
    var flash_elem = document.getElementById(div_id);
    if ( flash_elem ) {
      PlaybackManager.log("PlayFlash:: hiding " +flash_elem.id, "debug");
      flash_elem.style.visibility = 'hidden';
    }
  }
};

PlayFlash.prototype._realPlay = function() {
  PlaybackManager.log("PlayFlash::play", "debug");

  if(this.flash_elem.play) {
    var firstPlay = false;
    if(!inArray(this.assignment_data.getId(), this.seenMedia)) {
      firstPlay = true;
      this.seenMedia.push(this.assignment_data.getId());
    }
    this.flash_elem.play(this.region_data.toJson(), this.assignment_data.toJson(), firstPlay);
  }

  this.setupTimers(this.assignment_data.getLength() * 1000);
  PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
};

/**
 * Plays the video specified by the assignment data
 */
PlayFlash.prototype.play = function() {
  var self = this;
  this.clearTimers();
  this.timers.push(window.setTimeout(function() { self._realPlay(); }, 1000));
  this.flash_elem.style.visibility = 'visible';
  if(this.prev_elem && this.prev_elem !== this.flash_elem) {
    this.prev_elem.style.visibility = 'hidden';
  }
};

/**
 * Configures the timers for the events and playlist switch
 * @param output_name
 */
PlayFlash.prototype.setupTimers = function(length) {
  var self = this;
  //two second offset added because flash timing is wacked!
  var offsets = {1:-3000,5:-7000,10:-12000};
  for(var offset in offsets) {
    if(length + offsets[offset] > 0) {
      //Uses closure to get value of variable
      this.timers.push(window.setTimeout((
        function(time_value, r_instance) {
        return function () {
          r_instance.timeEvent(time_value);
          PlaybackManager.log("Ending Flash Element: "+self.flash_elem.id+" in:"+time_value,"debug");
        };
      } (offset, self)),
      length + offsets[offset],
      this.flash_elem.id));
    }
  }
  this.timers.push(window.setTimeout(function() {
    PlaybackManager.log("Ending Flash Element: "+self.flash_elem.id+" length:"+length,"debug");
    self.ended(self.flash_elem.id);
  }, length, self.flash_elem.id));
};

/**
 * Clears all remaining timers
 */
PlayFlash.prototype.clearTimers = function() {
  for(var t in this.timers) {
    if(this.timers[t]) { window.clearTimeout(this.timers[t]); }
  }
};

/**
 * Pauses the playback of the media on the screen - this should pause timer for restart (currently does not)
 */
PlayFlash.prototype.pause = function() {
  PlaybackManager.log("PlayFlash::pause", "debug");
  this.clearTimers();
  if(this.flash_elem.pause) {
    this.flash_elem.pause(this.region_data.toJson(), this.assignment_data.toJson());
  }
  PlaybackManager.sendEvent('player::pause', this.region_data, this.assignment_data);
};

/**
 * callEvent on the flash media using two args
 */
PlayFlash.prototype.timeEvent = function(time_left) {
  if(this.flash_elem.event) {
    this.flash_elem.event(this.region_data.toJson(),
                          this.assignment_data.toJson(),
                          'player::time_event',
                          PlaybackManager.getYInstance().JSON.stringify({'time': time_left}));
  }
};

/**
 * Stops playback and tears down player
 */
PlayFlash.prototype.stop = function() {
  PlaybackManager.log("PlayFlash::stop", "debug");
  this.clearTimers();
  if(this.flash_elem.stop) {
    this.flash_elem.stop(this.region_data.toJson(), this.assignment_data.toJson());
  }
  this.remove(this.flash_elem.id);
  PlaybackManager.sendEvent('player::stop', this.region_data, this.assignment_data);
};

/**
 * removes the div
 */
PlayFlash.prototype.remove = function(div_id) {
  PlaybackManager.log("PlayFlash::remove_div", "debug");
  var local_div = div_id;
  this.hide(local_div);
  window.setTimeout( function () {
    PlaybackManager.log("deleting div_id: " + local_div,"debug");
    swfobject.removeSWF(local_div);
  },1000);

};

/**
 * Is called from internal onEnded listener to stop timer and send 'real' event to playback manager
 */
PlayFlash.prototype.ended = function() {
  PlaybackManager.log("PlayFlash::ended", "debug");

  if(this.flash_elem.ended) {
    this.flash_elem.ended(this.region_data.toJson(), this.assignment_data.toJson());
  }
  PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlayFlash.prototype.toString = function() {
  return "PlayFlash";
};


