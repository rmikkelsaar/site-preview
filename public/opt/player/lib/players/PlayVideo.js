/*global
  PlayFactory
  Error
  getMediaPath
  PlaybackManager
  console
  */

/**
 * Higher performance playback but blips and scales to aspect
 */
function PlayVideo() {
  this.region_data;
  this.assignment_data;
  this.video_elem;
  this.timer;
  this.timerDonePlay = null;
  this.video_name = 'vp';
  this.state;
  this.id;
}

//Add this player to the list of available types
PlayFactory.playTypes.Video = 'PlayVideo';
//Stores a list of current players against their video element ids. This simplifies the event processing at the expense of cleanliness
PlayVideo.playerLookups = [];
PlayVideo._ended = function(id) {
  if(PlayVideo.playerLookups[id]) {
    PlayVideo.playerLookups[id].ended();
  }
};

/**
 * Intializes the video player with the appropriate data
 * @param RegionData region_data The region information for the target of this video
 * @param AssignmentData assignment_data The assignment information to play
 * @return
 */
PlayVideo.prototype.initialize = function(region_data) {
  this.region_data = region_data;
};

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayVideo.prototype.prepareRegion = function(region_node, assignment) {
  this.assignment_data = assignment;
  //  PlaybackManager.sendEvent('player::prepare', this.region_data, this.assignment_data);
  var video_id = this.video_name + '_' + this.region_data.getId();
  this.id=video_id;
  if(!this.video_elem || !document.getElementById(video_id)) {
    //Build video player, fill to region and add listener
    region_node.append('<video style="display: none; border: none;" id="' + video_id + '"></video>');
    this.video_elem = document.getElementById(video_id);
    this.video_elem.style.width = region_node.get('clientWidth') + 'px';
    this.video_elem.style.height = region_node.get('clientHeight') + 'px';
    if(!this.video_elem) {
      throw new Error('Unable to create video element for region: ' + this.region_data.getId());
    }
    this.video_elem.addEventListener("ended", function() {
      if(PlayVideo.playerLookups[this.id]) {
        if(this.timer) {
          window.clearTimer(this.timer);
        }
        if (this.timerDonePlay) {
          window.clearTimeout(this.timerDonePlay);
          this.timerDonePlay = null;
        }
        PlayVideo.playerLookups[this.id].ended();
      }
    }, false );
    this.video_elem.addEventListener("error", function() {
      if(PlayVideo.playerLookups[this.id]) {
        PlayVideo.playerLookups[this.id].error();
      }
    }, false );
    PlayVideo.playerLookups[video_id] = this; //Allows for event lookup from elems
  }
  if(this.video_elem.src !== getMediaPath(this.assignment_data)) {
    this.video_elem.src = getMediaPath(this.assignment_data);
    this.video_elem.load();
  }
};

PlayVideo.prototype.checkPlayback = function() {
  if(this.video_elem && (this.video_elem.ended || this.video_elem.paused)) {
    this.ended();
  } else if(this.state === 'play') {
    var self = this;
    this.timer = window.setTimeout(function() { self.checkPlayback(); }, 1000);
  }
};

/**
 * Disables any elements currently on the screen for this player
 */
PlayVideo.prototype.hide = function() {
  if(this.timer) {
    window.clearTimeout(this.timer);
  }
  this.video_elem.style.display = 'none';
};

/**
 * Plays the video specified by the assignment data
 */
PlayVideo.prototype.play = function() {
  var self = this;
  this.video_elem.play();
  this.video_elem.style.display = 'block';
  this.state = 'play';
  this.timerDonePlay = window.setTimeout(function() {
    PlayVideo._ended(self.video_elem.id);
  }, self.assignment_data.length*1000+500);

  this.timer = window.setTimeout(function() { self.checkPlayback(); }, 1000);
  PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
};


/**
 * Pauses the playback of the media on the screen - this should pause timer for restart (currently does not)
 */
PlayVideo.prototype.pause = function() {
  this.video_elem.pause();
  if(this.state === 'play') {
    this.state = 'pause';
    PlaybackManager.sendEvent('player::pause', this.region_data, this.assignment_data);
  }
};

/**
 * Stops playback and tears down player
 */
PlayVideo.prototype.stop = function() {
  this.hide();
  this.video_elem.pause();
  if(this.state === 'play') {
    this.state = 'stop';
    PlaybackManager.sendEvent('player::stop', this.region_data, this.assignment_data);
  }
};

/**
 * Called when an error occurs in the video playback
 */
PlayVideo.prototype.error = function() {
  if(this.state === 'play') {
    this.state = 'error';
    PlaybackManager.sendEvent('player::error', this.region_data, this.assignment_data);
  }
};

/**
 * Is called from internal onEnded listener to stop timer and send 'real' event to playback manager
 */
PlayVideo.prototype.ended = function() {
  PlaybackManager.log("***PlayVideo.prototype.ENDED*** regionId:"+this.region_data.getId()+" "+ this.assignment_data+"timer:"+ this.timer,"debug");

  if(this.timer) {
    window.clearTimeout(this.timer);
  }
  if(this.timerDonePlay !== null ) {
    window.clearTimeout(this.timerDonePlay);
    this.timerDonePlay = null;
  }
  if(this.state === 'play') {
    this.state = 'ended';
    PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlayVideo.prototype.toString = function() {
  return "PlayVideo";
};
