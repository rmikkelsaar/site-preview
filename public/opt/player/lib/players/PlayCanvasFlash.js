/*global
  PlayFlash
  PlayFactory
  PlaybackManager
  swfobject
  getMediaPath
  inArray
  ConfigurationFactory
  Wait
  console
  */

/**
 * Plays back video media using HTML5 video tag
 */
function PlayCanvasFlash() {
  this.playTimers= [];
  this.flash_elem_future = null;
  this.transitional = false;
  this.region_node = null;
  this.params = {
    play: "true",
    scale: "exactfit",
    loop: "true",
    wmode: "transparent",
    bgcolor: "#000000"
  };
  this.playCommand = null;
  this.timerFlashLoader = null;
  this.prev_elem = null;
  this.prev_div = null;
}


PlayCanvasFlash.prototype = new PlayFlash(); //Javascript inheritance
//Add this player to the list of available types
PlayFactory.playTypes.CanvasFlash = 'PlayCanvasFlash';
PlayFactory.playTypes.SynchronizedFlash = 'PlayCanvasFlash';
PlayFactory.playTypes.SynchronizedCanvasFlash = 'PlayCanvasFlash';

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayCanvasFlash.prototype.prepareRegion = function(region_node, assignment, assignmentFuture, playlists) {

  PlaybackManager.log("PlayCanvasFlash::prepareRegion","debug");
  PlaybackManager.log("region_node: "+region_node,"debug");
  PlaybackManager.log("assignment: "+assignment,"debug");
  PlaybackManager.log("assignmentFuture: "+assignmentFuture,"debug");
  var self = this;
  this.assignment_data = assignment;
  this.region_node = region_node;
  var playerVersion = swfobject.getFlashPlayerVersion();
  playerVersion = playerVersion.major + "." + playerVersion.minor + "." + playerVersion.release;

  var div_id = 'fp_' + this.region_data.getId() + assignment.getId();
  var flash_elem = swfobject.getObjectById(div_id);
  // see if a flash element is already there
  // if not initialize
  if ( !flash_elem ) {
    PlaybackManager.log("No flash preload found!: " + div_id,"debug");
    PlaybackManager.log("Reboot or Default Content started or ended: " + div_id,"debug");
    this.flash_elem = null;
    this.seenMedia = [];
    // this piece supports old CanvasFlash
    region_node.append('<div style="width: 100%; height: 100%; border: none;" id="' + div_id + '"></div>');
    PlayFlash.playerLookups[div_id] = this; //Allows for event lookup from elems
    PlaybackManager.log("Loading Flash Object> "+div_id,"debug");
    var attributes = null;
    if ( self.transitional === false ) {
      attributes = { data:getMediaPath(assignment), width:1, height:1 };
    } else {
      attributes = { data:getMediaPath(assignment), width:region_node.get('clientWidth'), height:region_node.get('clientHeight') };
    }
    swfobject.createSWF(attributes, this.params, div_id);
    flash_elem = swfobject.getObjectById(div_id);
    flash_elem.style.position = 'absolute';
    flash_elem.style.left = '0px';
    flash_elem.style.top = '0px';
    flash_elem.style.visibility = 'hidden';//(region_node.get('clientHeight')-1) + 'px';
    this.playCommand = this.assignment_data.getPlayCommand();
    //end of old CanvasFlash support
    //check if in transitional mode
    window.setTimeout(function() {
      if ( flash_elem && flash_elem.play ) {
        PlaybackManager.log("*Found Transitional Content: " + div_id,"debug");
        if ( self.transitional === false ) {
          self.transitional = true;
        }
      }
    }, 5000);
  } else {
    PlaybackManager.log("Found flash preloaded: " + div_id,"debug");
    PlaybackManager.log("New daypart: " + div_id,"debug");
    // clear this for first play fade options to work on daypart change
    this.seenMedia = [];
    flash_elem.width = this.region_node.get('clientWidth');
    flash_elem.height = this.region_node.get('clientHeight');
  }

  // if there is a future assignment
  if ( assignmentFuture ){
    var div_id_future = 'fp_' + this.region_data.getId() + assignmentFuture.getId();
    // just in case there is still a div around background for 5 seconds and wait for remove to blow it away
    window.setTimeout(function() {
      var flash_elem_future = swfobject.getObjectById(div_id_future);
      // this.timerFlashLoader makes sure flash will not attempt to load itself more than once
      if(!flash_elem_future && ( self.timerFlashLoader === null ) ) {
        // get ready to background and preload
        var setupFutureFlash = function () {
          // get the future flash to play if already loaded
          region_node.append('<div style="width: 100%; height: 100%; border: none;" id="' + div_id_future + '"></div>');
          PlayFlash.playerLookups[div_id_future] = self; //Allows for event lookup from elems
          PlaybackManager.log("Loading Future Flash Object>"+div_id_future,"debug");
          var attributes = { data:getMediaPath(assignmentFuture), width:1, height:1 };
          swfobject.createSWF(attributes, self.params, div_id_future);

          flash_elem_future = swfobject.getObjectById(div_id_future);
          flash_elem_future.style.position = 'absolute';
          flash_elem_future.style.left = '0px';
          flash_elem_future.style.top = '0px';
          flash_elem_future.style.visibility = 'hidden';
          self.flash_elem_future = flash_elem_future;
        };
        // timer to start background flash load
        var now = new Date();
        var offset = -13000;

        var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
        var time = playlists[output_name].getTimeToPlays();
        var time_till_end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), time[0].getEndHour(), time[0].getEndMinute(), time[0].getEndSecond(), 0) - now;
        if (time_till_end > 0) {
          if(time_till_end + offset > 0) {
            PlaybackManager.log("Timer started to load Future Flash Object in "+((time_till_end+offset)/1000)+"sec for "+div_id_future,"debug");
            window.setTimeout(function() {
              setupFutureFlash();
              self.timerFlashLoader = null;
            },(time_till_end+offset), div_id_future);
            self.timerFlashLoader = true;
          }
        }
      }
    }, 5000);
  }

  this.current_id = flash_elem.id;

  if (flash_elem ) {
    PlaybackManager.log("flash_elem:"+flash_elem.id, "debug");
    if ( this.prev_elem ) {
      PlaybackManager.log("this.prev_elem:"+this.prev_elem.id, "debug"); }
  }

  if(this.last_id) {
    if( this.last_id !== this.current_id ) {
      var delay = 1000;
      var last_id = self.last_id;
      if ( self.playCommand === "SynchronizedCanvasFlash" ) {
        delay = 4000;
        window.setTimeout(function() {
          PlaybackManager.log("PlayCanvasFlash::prepareRegion:removing element " +self.last_id, "debug");
          self.remove(last_id);
        }, delay);
      } else {
        window.setTimeout(function() {
          PlaybackManager.log("PlayCanvasFlash::prepareRegion:removing element " +self.last_id, "debug");
          self.remove(last_id);
        }, delay);
      }
    } else {
      PlaybackManager.log("PlayCanvasFlash::prepareRegion: continue using object", "debug");
    }
  }

  // for use else where
  this.flash_elem = flash_elem;
  this.prev_elem = this.flash_elem;
  //  for when we return here
  this.last_id = this.prev_elem.id;
};


PlayCanvasFlash.prototype._realPlay = function(WaitToPlay) {
  var firstPlay = false;
  if ( WaitToPlay && this.transitional === true ){
    if (this.flash_elem.play && WaitToPlay.value !== true ) {
      if(!inArray(this.assignment_data.getId(), this.seenMedia)) {
        firstPlay = true;
        this.flash_elem.style.visibility = 'visible';
        if(this.prev_elem && this.prev_elem !== this.flash_elem) {
          this.prev_elem.style.visibility = 'hidden';
        }
        // HIT PLAY!
        this.flash_elem.play(this.region_data.toJson(), this.assignment_data.toJson(), firstPlay);
        this.seenMedia.push(this.assignment_data.getId());
        PlaybackManager.log("PlayCanvasFlash::firstPlay", "debug");
        PlaybackManager.log("this.seenMedia:"+this.seenMedia,"debug");
        this.setupTimers(this.assignment_data.getLength() * 1000);
      }
      //clear the interval that got me here
      var interval = this.playTimers.shift();
      PlaybackManager.log("PlayCanvasFlash::_Realplay:clearInterval#" + interval,"debug");
      clearInterval(interval);
      PlaybackManager.log("PlayCanvasFlash::play:intervals left (should be none): " + this.playTimer, "debug");
      PlaybackManager.log("PlayCanvasFlash-transitional::play", "debug");
      PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
    }
  } else {
    this.flash_elem.width = this.region_node.get('clientWidth');
    this.flash_elem.height = this.region_node.get('clientHeight');
    // expand preloaded flash element to fill the screen
    if(this.flash_elem.play) {
      PlaybackManager.log("PlayCanvasFlash::play", "debug");
      if(!inArray(this.assignment_data.getId(), this.seenMedia)) {
        this.seenMedia.push(this.assignment_data.getId());
      }
      this.flash_elem.play(this.region_data.toJson(), this.assignment_data.toJson(), false);
    }
    this.setupTimers(this.assignment_data.getLength() * 1000);

    this.flash_elem.style.visibility = 'visible';
    if(this.prev_elem && this.prev_elem !== this.flash_elem) {
      this.prev_elem.style.visibility = 'hidden';
    }
    PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
  }
};

PlayCanvasFlash.prototype.clearIntervalTimers = function() {
  //clear play intervals left over because we are swtiching to new flash
  //otherwise run away timers may occur
  for(var pt in this.playTimers) {
    if(this.playTimers[pt]) {
      clearInterval(this.playTimers[pt]); }
      PlaybackManager.log("PlayCanvasFlash::play:intervalCleaner#" + pt,"debug");
  }
};

/**
 * Plays the video specified by the assignment data
 */
// regionPlayerBasic will set WaitToPlay if not set maybe RegionPlayerSync is calling?
// New playlist loaded event will set it for daypart transitional syncing
PlayCanvasFlash.prototype.play = function(WaitToPlay) {
  var self = this;
  var interval;
  self.playAttempts = 0;
  this.clearTimers();
  //validate if Im transitional or not
  if ( !WaitToPlay || this.transitional === false ) {
    // make sure the file is completely loaded (xml and all) before trying to play
    // ensures sync
    if ( self.playCommand === "SynchronizedCanvasFlash" ) {
      this.timers.push(window.setTimeout(function() { self._realPlay(); }, 4000));
    } else {
      this.timers.push(window.setTimeout(function() { self._realPlay(); }, 1000));
    }
  } else {
    //call_realPlay until sucessful for transistional mode
    window.setTimeout(function() {
      interval = window.setInterval(function() {
        if (self.playAttempts > 1000) { // 20 sec load time
          PlaybackManager.log("PlayCanvasFlash::play: clearInterval(interval):" + clearInterval(self.playTimers.shift()), "debug");
          this.play();
        } else {
          PlaybackManager.log("PlayCanvasFlash::play attempt " + self.playAttempts, "debug");
          self._realPlay(WaitToPlay);
          self.playAttempts++;
        }
      }, 50);
      self.playTimers.push(interval);
      PlaybackManager.log("PlayCanvasFlash::play: NEW Interval:" + interval, "debug");
    },1350);
  }
};

/**
 * Is called from internal onEnded listener to stop timer and send 'real' event to playback manager
 */
PlayCanvasFlash.prototype.ended = function() {
  PlaybackManager.log("PlayCanvasFlash::ended", "debug");
  var self = this;

  if(this.flash_elem.ended) {
    this.flash_elem.ended(this.region_data.toJson(), this.assignment_data.toJson());
  }

  PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
};

