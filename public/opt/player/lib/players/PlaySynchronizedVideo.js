/*global
  PlayFactory
  PlayVideo
  ConfigurationFactory
  wsync
  wmsg
  */

/**
 * Higher performance playback but blips and scales to aspect
 */
function PlaySynchronizedVideo() {
  this.video_name = 'sv';
  this.peer;
}
PlaySynchronizedVideo.prototype = new PlayVideo(); //Javascript inheritance
//Add this player to the list of available types
PlayFactory.playTypes.SynchronizedVideo = 'PlaySynchronizedVideo';
//Stores a list of current players against their video element ids. This simplifies the event processing at the expense of cleanliness
PlayVideo.playerLookups = [];

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlaySynchronizedVideo.prototype.prepareRegion = function(region_node, assignment) {
  PlayVideo.prototype.prepareRegion.call(this, region_node, assignment);
  if(!this.peer) {
    this.initSynchronized();
  }
};

/**
 * Initializes the synchronized event for the video elem
 */
PlaySynchronizedVideo.prototype.initSynchronized = function() {
  var role;
  if(ConfigurationFactory.getInstance().getValue('is_master') === 'yes') {
    role = new wsync.Leader();
    role.minFollowers = 1;
    role.minFollowersTimeoutMs = 5000;
  } else {
    role = new wsync.Follower();
    role.leaderTimeoutMs = 5000;
  }

  var synchronizer = new wsync.SynchronizedHtmlMedia(this.video_elem);
  var bridge = new wmsg.SocketIoEventChannel(new wmsg.SocketIoClient(), new wmsg.EventChannel()).bind();
  var peer = new wsync.SynchronizedPeer(role, synchronizer, bridge, this.region_data.getValue('name')).bind();
  bridge.client.onWebOpened.subscribe(function() { peer.join(); });
  bridge.client.connect(ConfigurationFactory.getInstance().getValue('message_url'));
  this.peer = peer;
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlaySynchronizedVideo.prototype.toString = function() {
  return "PlaySynchronizedVideo";
};
