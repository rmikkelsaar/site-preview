/*global
  PlayFactory
  PlaybackManager
  PlayHtmlInterface
  MediaIntegration
  getMediaPath
  inArray
  Error
  */

/**
 * Plays back HTML media
 */
function PlayHtml() {
  this.region_data;
  this.assignment_data;
  this.html_elem;
  this.html_doc;
  this.timers = [];
  this.state;
  this.seenMedia = [];
}

//Add this player to the list of available types
PlayFactory.playTypes.Html = 'PlayHtml';
PlayFactory.playTypes.CanvasHtml = 'PlayHtml';
PlayFactory.playTypes.SynchronizedHtml = 'PlayHtml';
PlayFactory.playTypes.SynchronizedCanvasHtml = 'PlayHtml';
//Stores a list of current players against their HTML element ids. This simplifies the event processing at the expense of cleanliness
PlayHtml.playerLookups = [];
//Called from window timeout to complete HTML playback
PlayHtml._ended = function(id) {
  if(PlayHtml.playerLookups[id]) {
    PlayHtml.playerLookups[id].ended();
  }
};

/**
 * Intializes the HTML player with the appropriate data - only called once
 * @param RegionData region_data The region information for the target of this video
 * @return
 */
PlayHtml.prototype.initialize = function(region_data) {
  this.region_data = region_data;
  //Subscribe to events
  if(!this.on) {
    PlaybackManager.getYInstance().augment(PlayHtml, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  this.on('region_player::time_event', function(e) {
    if(e.details[0].getId() === this.region_data.getId() && this.state === 'play' && this.html_doc.event) {
      this.html_doc.event(this.region_data.toJson(),
                          this.assignment_data.toJson(),
                          'region_player::time_event',
                          PlaybackManager.getYInstance().JSON.stringify({'time': e.details[2]}));
    }
  });
  this.on('media::extend', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId()) {
      this.clearTimers();
      this.setupTimers(e.details[2]);
    }
  });
  this.on('media::send', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId()) {
      PlaybackManager.log("PlayHtml.sendEvent: " + e.details[2] + ":" + this.region_data.getId() + ":" + this.assignment_data.getId() + ":" + e.details[3] + ":" + e.details[4] + ":" + e.details[5], "debug");
      PlaybackManager.sendEvent(e.details[2], this.region_data, this.assignment_data, e.details[3], e.details[4], e.details[5]);
    }
  });
  this.on('media::broadcast', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId()) {
      PlaybackManager.log("PlayHtml.broadcastEvent: " + e.details[2] + ":" + this.region_data.getId() + ":" + this.assignment_data.getId() + ":" + e.details[3] + ":" + e.details[4] + ":" + e.details[5], "debug");
      PlaybackManager.broadcastMessage(e.details[2], {'region': this.region_data, 'assignment': this.assignment_data, 'arguments': [e.details[3], e.details[4], e.details[5]] });
    }
  });
  this.on('media::subscribe', function(e) {
    if(e.details[0] === this.region_data.getId() && e.details[1] === this.assignment_data.getId() && this.html_doc.event) {
      PlaybackManager.log("PlayHtml.subscribe: " + e.details[2] + ":" + e.details[3], "debug");
      this.on(e.details[2], function(e) {
        this.html_doc.event(this.region_data.toJson(),
                            this.assignment_data.toJson(),
                            e.type,
                            PlaybackManager.getYInstance().JSON.stringify(e.details));
      });
    }
  });
};

/**
 * Prepares the target for playback
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayHtml.prototype.prepareRegion = function(region_node, assignment) {
  PlaybackManager.sendEvent('player::prepare', this.region_data, assignment);
  var html_id = 'html_' + this.region_data.getId();
  if(!this.html_elem || !document.getElementById(html_id)) {
    region_node.append('<iframe scrolling="no" style="display: none; overflow: none; width: 100%; height: 100%; border: none;" id="' + html_id + '" />');
    this.html_elem = document.getElementById(html_id);
    if(!this.html_elem) {
      throw new Error('Unable to create HTML element for region: ' + this.region_data.getId());
    }
    if(!window.playerInterface) {
      window.playerInterface = new PlayHtmlInterface();
      if(MediaIntegration) {
        window.mediaInterface = MediaIntegration;
      }
    }

    PlayHtml.playerLookups[html_id] = this;
  }
  if(!this.assignment_data || this.assignment_data.getId() !== assignment.getId()) {
    this.assignment_data = assignment;
    this.html_elem.src = getMediaPath(this.assignment_data) + "/index.html";
  }
};

/**
 * Disables any elements currently on the screen for this player
 */
PlayHtml.prototype.hide = function() {
  this.html_elem.style.display = 'none';
  this.state = 'hide';
};

PlayHtml.prototype._play = function() {
  this.html_elem.style.display = 'block';
  this.html_doc = this.html_elem.contentWindow || this.html_elem.contentDocument;
  if(this.html_doc.document) {
    this.html_doc = this.html_doc.document;
  }
  if(this.html_doc.play) {
    var firstPlay = false;
    if(!inArray(this.assignment_data.getId(), this.seenMedia)) {
      firstPlay = true;
      this.seenMedia.push(this.assignment_data.getId());
    }
    this.html_doc.play(this.region_data.toJson(), this.assignment_data.toJson(), firstPlay);
  }
  this.clearTimers();
  this.setupTimers(this.assignment_data.getLength() * 1000);
  this.state = 'play';
  PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
};

/**
 * Configures the timers for the events and playlist switch
 * @param output_name
 */
PlayHtml.prototype.setupTimers = function(length) {
  var self = this;
  var offsets = {1:-1000,5:-5000,15:-15000};
  for(var offset in offsets) {
    if(length + offsets[offset] > 0) {
      //Uses closure to get value of variable
      this.timers.push(window.setTimeout(
        (function(time_value, r_instance) { return function () { r_instance.timeEvent(time_value); }; }(offset, self)),length + offsets[offset],this.html_elem.id));
    }
  }
  this.timers.push(window.setTimeout(function() { self.ended(); }, length, this.html_elem.id));
};

/**
 * Clears all remaining timers
 */
PlayHtml.prototype.clearTimers = function() {
  for(var t in this.timers) {
    if(this.timers[t]) { window.clearTimeout(this.timers[t]); }
  }
};

/**
 * Plays the video specified by the assignment data
 */
PlayHtml.prototype.play = function() {
  var self = this;
  window.setTimeout(function() { self._play(); }, 500);
};

/**
 * Pauses the playback of the media on the screen - this should pause timer for restart (currently does not)
 */
PlayHtml.prototype.pause = function() {
  this.clearTimers();
  if(this.state === 'play') {
    if(this.html_doc.pause) {
      this.html_doc.pause(this.region_data.toJson(), this.assignment_data.toJson());
    }
    this.state = 'pause';
    PlaybackManager.sendEvent('player::pause', this.region_data, this.assignment_data);
  }
};

/**
 * Stops playback and tears down player
 */
PlayHtml.prototype.stop = function() {
  this.clearTimers();
  this.html_elem.style.display = 'none';
  if(this.state === 'play') {
    if(this.html_doc.stop) {
      this.html_doc.stop(this.region_data.toJson(), this.assignment_data.toJson());
    }
    this.state = 'stop';
    PlaybackManager.sendEvent('player::stop', this.region_data, this.assignment_data);
  }
};

PlayHtml.prototype.timeEvent = function(time_left) {
  if(this.html_doc.event) {
    this.html_doc.event(this.region_data.toJson(),
                        this.assignment_data.toJson(),
                        'player::time_event',
                        PlaybackManager.getYInstance().JSON.stringify({'time': time_left}));
  }
};

/**
 * Bubbles up complete event from timer
 */
PlayHtml.prototype.ended = function() {
  if(this.state === 'play') {
    if(this.html_doc.ended) {
      this.html_doc.ended(this.region_data.toJson(), this.assignment_data.toJson());
    }
    this.state = 'ended';
    PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlayHtml.prototype.toString = function() {
  return "PlayHtml";
};
