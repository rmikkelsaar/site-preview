/*global
  PlayFactory
  PlaybackManager
  getMediaPath
  Error
  */

/**
 * Plays back image media
 */
function PlayImage() {
  this.region_data;
  this.assignment_data;
  this.image_elem;
  this.timer;
  this.state;
}

//Add this player to the list of available types
PlayFactory.playTypes.Image = 'PlayImage';
PlayFactory.playTypes.CanvasImage = 'PlayImage';
PlayFactory.playTypes.SynchronizedImage = 'PlayImage';
PlayFactory.playTypes.SynchronizedCanvasImage = 'PlayImage';
//Stores a list of current players against their image element ids. This simplifies the event processing at the expense of cleanliness
PlayImage.playerLookups = [];
//Called from window timeout to complete image playback
PlayImage._ended = function(id) {
  if(PlayImage.playerLookups[id]) {
    PlayImage.playerLookups[id].ended();
  }
};

/**
 * Intializes the image player with the appropriate data - only called once
 * @param RegionData region_data The region information for the target of this video
 * @return
 */
PlayImage.prototype.initialize = function(region_data) {
  this.region_data = region_data;
};

/**
 * Prepares the target for playback
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayImage.prototype.prepareRegion = function(region_node, assignment) {
  this.assignment_data = assignment;
  PlaybackManager.sendEvent('player::prepare', this.region_data, this.assignment_data);
  var image_id = 'img_' + this.region_data.getId();
  if(!this.image_elem || !document.getElementById(image_id)) {
    region_node.append('<img style="width: 100%; height: 100%; border: none;" id="' + image_id + '" />');
    this.image_elem = document.getElementById(image_id);
    if(!this.image_elem) {
      throw new Error('Unable to create image element for region: ' + this.region_data.getId());
    }
    PlayImage.playerLookups[image_id] = this;
  }
};

/**
 * Disables any elements currently on the screen for this player
 */
PlayImage.prototype.hide = function() {
  this.image_elem.style.display = 'none';
};

/**
 * Plays the video specified by the assignment data
 */
PlayImage.prototype.play = function() {
  this.image_elem.src = getMediaPath(this.assignment_data);
  this.image_elem.style.display = 'block';
  this.timer = window.setTimeout(PlayImage._ended, this.assignment_data.getLength() *
                                 1000, this.image_elem.id);
  this.state = 'play';
  PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
};

/**
 * Pauses the playback of the media on the screen - this should pause timer for restart (currently does not)
 */
PlayImage.prototype.pause = function() {
  window.clearTimeout(this.timer);
  this.state = 'pause';
  PlaybackManager.sendEvent('player::pause', this.region_data, this.assignment_data);
};

/**
 * Stops playback and tears down player
 */
PlayImage.prototype.stop = function() {
  window.clearTimeout(this.timer);
  this.hide();
  this.state = 'stop';
  PlaybackManager.sendEvent('player::stop', this.region_data, this.assignment_data);
};

/**
 * Bubbles up complete event from timer
 */
PlayImage.prototype.ended = function() {
  if(this.state === 'play') {
    PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlayImage.prototype.toString = function() {
  return "PlayImage";
};
