/*global
  ConfigurationFactory
  PlaybackManager
  */

/**
 * Interface for playing HTML media to access and control parent options
 */
function PlayHtmlInterface() {
}

PlayHtmlInterface.prototype.getDataURL = function() {
  return ConfigurationFactory.getInstance().getSupportBase() + 'Data.php';
};

PlayHtmlInterface.prototype.getData = function(name, format) {
  if(!format) { format = 'raw'; }
  var url = this.getDataURL() + '?dataname=' + name + '&format=' + format + '&serial=' + Date.now();
  var request = PlaybackManager.getYInstance().io(url, { sync: true });
  return request.responseText;
};

PlayHtmlInterface.prototype.parseJson = function(data) {
  return PlaybackManager.getYInstance().JSON.parse(data);
};

