/*global
  PlayFactory
  PlaybackManager
  Error
  getMediaPath
  */

/**
 * Higher performance playback but blips and scales to aspect
 */
function PlayAudio() {
  this.region_data;
  this.assignment_data;
  this.element;
}

//Add this player to the list of available types
PlayFactory.playTypes.Audio = 'PlayAudio';
//Stores a list of current players against their video element ids. This simplifies the event processing at the expense of cleanliness
PlayAudio.playerLookups = [];
PlayAudio._ended = function(id) {
  if(PlayAudio.playerLookups[id]) {
    PlayAudio.playerLookups[id].ended();
  }
};

/**
 * Intializes the video player with the appropriate data
 * @param RegionData region_data The region information for the target of this video
 * @param AssignmentData assignment_data The assignment information to play
 * @return
 */
PlayAudio.prototype.initialize = function(region_data) {
  this.region_data = region_data;
};

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayAudio.prototype.prepareRegion = function(region_node, assignment) {
  this.assignment_data = assignment;
  PlaybackManager.sendEvent('player::prepare', this.region_data, this.assignment_data);
  if(!this.element) {
    //Build video player, fill to region and add listener
    var elem_id = 'ap_' + this.region_data.getId();
    region_node.append('<audio id="' + elem_id + '"></audio>');
    this.element = document.getElementById(elem_id);
    if(!this.element) {
      throw new Error('Unable to create video element for region: ' + this.region_data.getId());
    }
    this.element.addEventListener("ended", function() {
      if(PlayAudio.playerLookups[this.id]) {
        PlayAudio.playerLookups[this.id].ended();
      }
    });
    this.element.addEventListener("error", function() {
      if(PlayAudio.playerLookups[this.id]) {
        PlayAudio.playerLookups[this.id].error();
      }
    });
    PlayAudio.playerLookups[elem_id] = this; //Allows for event lookup from elems
  }
  if(this.element.src !== getMediaPath(this.assignment_data)) {
    this.element.src = getMediaPath(this.assignment_data);
    this.element.load();
  }
};

/**
 * Disables any elements currently on the screen for this player
 */
PlayAudio.prototype.hide = function() {
  //	this.element.stop();
};

/**
 * Plays the video specified by the assignment data
 */
PlayAudio.prototype.play = function() {
  this.element.play();
  PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
};


/**
 * Pauses the playback of the media on the screen - this should pause timer for restart (currently does not)
 */
PlayAudio.prototype.pause = function() {
  this.element.pause();
  PlaybackManager.sendEvent('player::pause', this.region_data, this.assignment_data);
};

/**
 * Stops playback and tears down player
 */
PlayAudio.prototype.stop = function() {
  this.element.pause();
  this.hide();
  PlaybackManager.sendEvent('player::stop', this.region_data, this.assignment_data);
};

/**
 * Called when an error occurs in the video playback
 */
PlayAudio.prototype.error = function() {
  PlaybackManager.sendEvent('player::error', this.region_data, this.assignment_data);
};

/**
 * Is called from internal onEnded listener to stop timer and send 'real' event to playback manager
 */
PlayAudio.prototype.ended = function() {
  if(this.timer) {
    window.clearTimeout(this.timer);
  }
  PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlayAudio.prototype.toString = function() {
  return "PlayAudio";
};
