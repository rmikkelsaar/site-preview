/*global
  PlayFactory
  PlayVideo
  PlaybackManager
  */

/**
 * Blipless and fullscreen playback of video using the canvas tag along with video
 */
function PlayCanvasVideo() {
  this.canvas_elem;
  this.canvas_context;
  this.draw_timer;
  this.video_name = 'cv';
}
PlayCanvasVideo.prototype = new PlayVideo(); //Javascript inheritance
//Add this player to the list of available types
PlayFactory.playTypes.CanvasVideo = 'PlayCanvasVideo';
//Helper method that is used by the timeout to initiate the underlying call to the draw.
PlayCanvasVideo._draw = function(id) {
  PlayVideo.playerLookups[id].draw();
  return true;
};

/**
 * Instantiates the video player if none is available within the region otherwise just reuses it.
 * @param Node The node that points to the region's element.
 * @param AssignmentData assignment_data The assignment information to play
 */
PlayCanvasVideo.prototype.prepareRegion = function(region_node, assignment) {
  PlayVideo.prototype.prepareRegion.call(this, region_node, assignment);
  var canvas_id = 'vpc_' + this.region_data.getId();
  this.canvas_elem = document.getElementById(canvas_id);
  if(!this.canvas_elem) {
    //Build Canvas, fill out and init context
    region_node.append('<canvas style="width: 100%; height: 100%; border: none;" id="' + canvas_id + '"></canvas>');
    this.canvas_elem = document.getElementById(canvas_id);
    this.canvas_elem.width = region_node.get('clientWidth');
    this.canvas_elem.height = region_node.get('clientHeight');
  }
  this.canvas_context = this.canvas_elem.getContext('2d');
};

/**
 * Clear them timers
 */
PlayCanvasVideo.prototype.clearTimers = function() {
  if(this.draw_timer) {
    window.clearTimeout(this.draw_timer);
  }
  if(this.timer) {
    window.clearTimeout(this.timer);
  }
};

/**
 * Disables any elements currently on the screen for this player
 */
PlayCanvasVideo.prototype.hide = function() {
  this.clearTimers();
  this.canvas_context.clearRect(0, 0, this.canvas_elem.width, this.canvas_elem.height);
  this.canvas_elem.style.display = 'none';
};

/**
 * Plays the video specified by the assignment data
 */
PlayCanvasVideo.prototype.play = function() {
  this.clearTimers();
  var self = this;
  this.video_elem.play();
  this.state = 'play';
  this.canvas_elem.style.display = 'block';
  this.timer = window.setTimeout(function() { self.checkPlayback(); }, 250);
  this.draw(); //Should probably hook to event listener instead of calling directly
  PlaybackManager.sendEvent('player::play', this.region_data, this.assignment_data);
};

/**
 * Copies the current frame from the video to the canvas layer - ensure no blips between videos
 */
PlayCanvasVideo.prototype.draw = function() {
  if(!this.video_elem.paused && !this.video_elem.ended) {
    this.canvas_context.drawImage(this.video_elem, 0, 0, this.canvas_elem.width, this.canvas_elem.height);
  }
  this.draw_timer = window.setTimeout(PlayCanvasVideo._draw, 24, this.video_elem.id);
};

/**
 * Is called from internal onEnded listener to stop timer and send 'real' event to playback manager
 */
PlayCanvasVideo.prototype.ended = function() {
  this.clearTimers();
  if(this.state === 'play') {
    this.state = 'ended';
    PlaybackManager.sendEvent('player::complete', this.region_data, this.assignment_data);
  }
};

/**
 * Called when an error occurs in the video playback
 */
PlayCanvasVideo.prototype.error = function() {
  this.clearTimers();
  if(this.state === 'play') {
    this.state = 'ended';
    PlaybackManager.sendEvent('player::error', this.region_data, this.assignment_data);
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlayCanvasVideo.prototype.toString = function() {
  return "PlayCanvasVideo";
};
