/*global
  ConfigurationFactory
  */

/**
 * Manages the configuration from the locally distributed config.json file
 */
function ConfigurationLocal() {
  this.configuration = [];
}

/**
 * Loads the configuration locally, if the configuration is not available still returns without exception
 */
ConfigurationLocal.prototype.load = function() {
  //Syncronous call since data is local
  this.configuration = ConfigurationFactory.loadConfiguration('config.json');
};

/**
 * Retrieves the requested value from the configuration or the default if the value is not set
 * @param string name The name of the value to retrieve
 * @param mixed default_value The default value if the configuration is not set
 * @return mixed The configuration value
 */
ConfigurationLocal.prototype.getValue = function(name, default_value) {
  if(this.configuration[name]) {
    return this.configuration[name];
  }
  return default_value;
};

/**
 * Sets a local value for the configuration - this value is temporary in nature.
 * @param string name The name of the value to set
 * @param string value The value to set
 */
ConfigurationLocal.prototype.setValue = function(name, value) {
  this.configuration[name] = value;
};

/**
 * Sets multiple values for the configuration
 * @param array Hashmap of name => value
 */
ConfigurationLocal.prototype.setValues = function(values) {
  var key;
  for(key in values) {
    this.setValue(key, values[key]);
  }
};

/**
 * Returns the base URI for loading all media assets.
 * E.g. /pbm/media/{ID}
 *      http://localmediaservice/mediagetter.php?media={ID}
 */
ConfigurationLocal.prototype.getMediaURI = function() {
  return this.getValue('media_uri', 'media/');
};

/**
 * Returns the base URI for support services. E.g. localhost/ or 192.168.0.1/support_services
 */
ConfigurationLocal.prototype.getSupportBase = function() {
  return this.getValue('base_services', '/playback_support');
};
