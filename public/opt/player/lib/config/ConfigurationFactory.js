/*global
  PlaybackManager
  ConfigurationLocal
  */


/**
 * The configuration factory instantiates and manages the configuration for the playback manager.
 */
function ConfigurationFactory() {
  this.instance = null;
}

/**
 * Retrieves the current configuration instance object (Singleton)
 */
ConfigurationFactory.getInstance = function() {
  if(this.instance === null || this.instance === undefined) {
    PlaybackManager.log("ConfigurationFactory::getInstance - loading new instance", "info");
    this.instance = new ConfigurationLocal();
    this.instance.load();
  }
  return this.instance;
};

/**
 * Loads the configuration specified by the name
 * @param string The name of the configuration file to load (e.g. config.json)
 */
ConfigurationFactory.loadConfiguration = function(name) {
  var request = PlaybackManager.getYInstance().io(
    name + '?serial=' + Date.now(), { sync: true });
  try {
    return PlaybackManager.getYInstance().JSON.parse(request.responseText);
  } catch(e) {
    PlaybackManager.log("Error parsing " + name + " configuration", "warn");
    return [];
  }
};

/**
 * Loads and merges any output specific configuration that may exist
 * @param output_name The output name to check/load for
 */
ConfigurationFactory.initializeOutputConfiguration = function(output_name) {
  var config = ConfigurationFactory.getInstance();
  config.setValue('output_name', output_name);
  var output_config = ConfigurationFactory.loadConfiguration('config_' + output_name + '.json');
  if(!output_config) {
    config.setValue('screen_name', output_name);
  } else {
    config.setValues(output_config);
  }
};
