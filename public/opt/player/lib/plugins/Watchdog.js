/*global
  PlaybackManager
  ConfigurationFactory
  DataAccessFactory
  */

/**
 * Signals watchdog
 */
function Watchdog() {
}
PlaybackManager.pluginTypes.watchdog = 'Watchdog';

/**
*/
Watchdog.prototype.initialize = function() {
  if(!this.on) {
    PlaybackManager.getYInstance().augment(Watchdog, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  this.on('player::complete', function(e) {
    this.touch(e.details[0], e.details[1]);
  });
  this.on('region_player::new', function(e) {
    this.addRegion(e.details[0], e.details[1]);
  });
  this.on('region_player::remove', function(e) {
    this.remRegion(e.details[0], e.details[1]);
  });
  this.on('scheduler::new', function(e) {
    this.reset();
  });
};

Watchdog.prototype.touch = function(region, assignment) {
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  var url = ConfigurationFactory.getInstance().getSupportBase() + '/Watchdog.php?action=touch&region=' + region.getId() + "&output=" + output_name;
  PlaybackManager.getYInstance().io(url);
};

Watchdog.prototype.addRegion = function(region, playlist) {
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  var timeout = 60;
  // if slave (no playlist) will crap out here therefore don't do!
  if(playlist) {
    var medias = DataAccessFactory.getInstance().loadMediaByIds(playlist.getAssignmentIds());
    for(var i in medias) {
      if(medias[i].getLength() > timeout) {
        timeout = medias[i].getLength();
      }
    }
  }
  var url = ConfigurationFactory.getInstance().getSupportBase() + '/Watchdog.php?action=add&region=' + region.getId() + "&length=" + timeout + "&output=" + output_name;
  PlaybackManager.getYInstance().io(url);
};

Watchdog.prototype.remRegion = function(region, playlist) {
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  var url = ConfigurationFactory.getInstance().getSupportBase() + '/Watchdog.php?action=rem&region=' + region.getId() + "&output=" + output_name;
  PlaybackManager.getYInstance().io(url);
};

Watchdog.prototype.reset = function() {
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  var url = ConfigurationFactory.getInstance().getSupportBase() + '/Watchdog.php?action=reset&output=' + output_name;
  PlaybackManager.getYInstance().io(url);
};
/**
 * Name of this object - used for debugging.
 * @return
 */
Watchdog.prototype.toString = function() {
  return "Watchdog";
};
