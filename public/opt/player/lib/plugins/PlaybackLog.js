/*global
  PlaybackManager
  ConfigurationFactory
  */

/**
 * Logs successful media playbacks
 */
function PlaybackLog() {
  var playtracking = [];
}

PlaybackManager.pluginTypes.logging = 'PlaybackLog';


/**
*/
PlaybackLog.prototype.initialize = function() {
  if(!this.on) {
    PlaybackManager.getYInstance().augment(PlaybackLog, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  this.playtracking = [];
  this.on('player::play', function(e) {
    this.prepareLog(e.details[0], e.details[1]);
  });
  this.on('player::complete', function(e) {
    this.logPlayback(e.details[0], e.details[1]);
  });
  this.on('player::stop', function(e) {
    this.logPlayback(e.details[0], e.details[1]);
  });
};

PlaybackLog.prototype.prepareLog = function(region, assignment) {
  if(!this.playtracking[region.getId()]) {
    this.playtracking[region.getId()] = [];
  }
  this.playtracking[region.getId()][assignment.getId()] = new Date();
};

PlaybackLog.prototype.logPlayback = function(region, assignment) {
  var playlog = [];
  var playTime = 0;
  var regionId=region.getId();
  var assignmentId=assignment.getId();

  playlog.push('x=' + region.getLeft());
  playlog.push('y=' + region.getTop());
  playlog.push('width=' + region.getWidth());
  playlog.push('height=' + region.getHeight());
  playlog.push('report_name=' + assignment.getValue('report_name', assignment.getId()));
  
  var now = new Date();
  if(this.playtracking[regionId] && this.playtracking[regionId][assignmentId]) {
	  playTime = now - this.playtracking[regionId][assignmentId];
	  if(playTime < 1000) { return; }
	  playlog.push('length=' + playTime);
  } else {
	  playlog.push('length=' + (assignment.getLength() * 1000));
  }
  var url = ConfigurationFactory.getInstance().getSupportBase() + '/PlaybackLog.php?' + playlog.join('&');
  PlaybackManager.getYInstance().io(url);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
PlaybackLog.prototype.toString = function() {
  return "PlaybackLog";
};
