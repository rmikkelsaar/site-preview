/*global
  PlaybackManager
  ConfigurationFactory
  */

/**
 * Dynamically retrieves media assets in conjuction with MediaModule service
 */
function MediaRetrieve() {

}

PlaybackManager.pluginTypes.media_retrieve = 'MediaRetrieve';

/**
*/
MediaRetrieve.prototype.initialize = function() {
  if(!this.on) {
    PlaybackManager.getYInstance().augment(MediaRetrieve, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  this.on('player::prepare', function(e) {
    this.loadMedia(e.details[1]);
  });
};

MediaRetrieve.prototype.loadMedia = function(assignment) {
  var url = ConfigurationFactory.getInstance().getSupportBase() + '/MediaModule.php?media_id=' + assignment.getAssetId();
  PlaybackManager.getYInstance().io(url);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
MediaRetrieve.prototype.toString = function() {
  return "MediaRetrieve";
};
