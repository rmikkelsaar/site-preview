/*global
  PlaybackManager
  DataAccessFactory
  inTime
  */

/**
 * Manages the schedule data, the current playing schedule and the updating when required
 */
function SchedulerPreload() {
  this.schedules = [];
  this.current_schedule;
}

/**
 * Reloads the data for the scheduler
 * The loadCurrentSchedule will have to be called to execute on the change in data
 * Publishes
 * scheduler::new - Generated when a new current schedule is set
 * scheduler::remove - Generated when an existing schedule is removed
 * scheduler::no_scheduler - Generated when no current schedule can be found
 */
SchedulerPreload.prototype.initialize = function() {
  this.schedules = DataAccessFactory.getInstance().loadSchedules();
  PlaybackManager.log("SchedulerPreload::Initiailzed with " + this.schedules.length + " schedules", "info");
};

/**
 * Returns the schedule that should be playing at this type
 * This method simply retruns the schedule data - it does not change the running configuration
 * @return ScheduleData The current schedule information
 */
SchedulerPreload.prototype.findCurrentSchedule = function() {
  PlaybackManager.log("SchedulerPreload::findCurrentSchedule", "debug");
  var now = new Date();
  for(var i=0;i<this.schedules.length;i++) {
    //PlaybackManager.log("Schedule[" + i + "]:" + this.schedules[i].toString(), "debug");
    if(now >= this.schedules[i].getStartDate() &&
       now <= this.schedules[i].getEndDate() &&
         inTime(now, this.schedules[i].getTimeToPlays())) {
      return this.schedules[i];
    }
  }
};

/**
 * Reviews the current schedule configuration and loads the current schedule if required
 * This method is responsible for initializing the regions based on the current schedule and sending the
 * new schedule event
 */
SchedulerPreload.prototype.loadCurrentSchedule = function() {
  PlaybackManager.log("SchedulerPreload::loadCurrentSchedule", "debug");
  var schedule = this.findCurrentSchedule();
  if(PlaybackManager.playbackEvents.length === 0 && this.current_schedule && (!schedule|| schedule.getId() !== this.current_schedule.getId())) {
    //TEAR DOWN
    PlaybackManager.log("SchedulerPreload::loadCurrentSchedule: Removing current", "debug");
    PlaybackManager.sendEvent('scheduler::remove', this.current_schedule);
    this.current_schedule = null;
  }
  if(!this.current_schedule) {
    if(!schedule) {
      PlaybackManager.sendEvent('scheduler::no_schedule');
    } else {
      //PLAY
      this.current_schedule = schedule;
      PlaybackManager.log("SchedulerPreload::loadCurrentSchedule: " + this.current_schedule.toString(), "debug");
      PlaybackManager.sendEvent('scheduler::new', this.current_schedule);
    }
  }
};

/**
 * Returns the current schedule
 * @return ScheduleData The current schedule
 */
SchedulerPreload.prototype.getCurrentSchedule = function() {
  return this.current_schedule;
};

/**
 * Returns the default schedule if configured
 * @return ScheduleData The default schedule
 */
SchedulerPreload.prototype.getDefaultSchedule = function() {
  PlaybackManager.log("SchedulerPreload::getDefault", "debug");
  for(var i=0;i<this.schedules.length;i++) {
    if(this.schedules[i].getTimeToPlays().length === 0) {
      return this.schedules[i];
    }
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
SchedulerPreload.prototype.toString = function() {
  return "SchedulerPreload";
};
