/*global
  RegionPlayerBasic
  RegionPlayerFactory
  PlaybackManager
  ConfigurationFactory
  playlistsCompare
  DataAccessFactory
  inArray
  console
  */


/**
 * Manages the playlist and playback for a basic region
 * A basic region just plays media in order with no bells or whistles
 */
function RegionPlayerEvent() {
  this.counter=0;
}

RegionPlayerEvent.prototype = new RegionPlayerBasic(); //Javascript inheritance
//Add this player to the list of available types
RegionPlayerFactory.regionTypes.RegionPlayerEvent = 'RegionPlayerEvent';
RegionPlayerFactory.regionTypes.Event = 'RegionPlayerEvent';
//RegionPlayerFactory.regionTypes.Standard = 'RegionPlayerEvent';

RegionPlayerEvent.prototype.initialize = function(schedule_data, region_data) {
  RegionPlayerBasic.prototype.initialize.call(this, schedule_data, region_data);
  //Interrupt event
  this.on('message_service::broadcast', function(e) {
    if(e.details[0] === 'event') {
      if(PlaybackManager.playbackEvents.length === 0) {
        PlaybackManager.playbackEvents.push(e.details[1]);
        this.loadCurrentPlaylist();
      }
    }
  });
};

RegionPlayerEvent.prototype.loadCurrentPlaylist = function() {
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  var playlists = this.findCurrentPlaylists();
  var playlistsFuture;

  if(this.current_playlists[output_name] && !playlistsCompare(this.current_playlists, playlists)) {
    //Playlists have changed
    this.stop();
    PlaybackManager.log("in RegionPlayerEvent::loadCurrentPlaylist: Playlists have changed!", "debug");
  }

  // if there is no current_playlist set see if we've just found one
  if(!this.current_playlists[output_name]) {
    if(playlists[output_name]) {
      //We load for each output in case our name ever changes due to failure monitoring - we could be on a dual screen
      this.current_assignments = [];
      for(var i in playlists) {
        this.current_assignments[i] = DataAccessFactory.getInstance().loadMediaByIds(playlists[i].getAssignmentIds());
      }
      // if theres media to play (assigned to play) then set it up and send event
      if(this.current_assignments[output_name] && this.current_assignments[output_name].length) {
        playlistsFuture = this.findFuturePlaylists();
        if ( playlistsFuture ) {
          PlaybackManager.log("in RegionPlayerEvent::findFuturePlaylists:Found... ","debug");
          //look into the future
          for(i in playlistsFuture) {
            this.future_assignments[i] = DataAccessFactory.getInstance().loadMediaByIds(playlistsFuture[i].getAssignmentIds());
          }
          this.future_playlists = playlistsFuture;
        } else {
          PlaybackManager.log("in RegionPlayerEvent::findFuturePlaylists:None found...","debug");
        }
        this.current_playlists = playlists;
        this.setupTimers(output_name);
        PlaybackManager.log("in RegionPlayerEvent::loadCurrentPlaylist:Sending region_player::new>"+this.region_data.getId(),"debug");
        PlaybackManager.sendEvent('region_player::new', this.region_data, this.current_playlists[output_name], this.future_playlists[output_name]);
      }
    }
    if((!this.current_assignments[output_name] || this.current_assignments[output_name].length === 0) && PlaybackManager.playbackEvents.length > 0 && !inArray('default', PlaybackManager.playbackEvents)) {
      PlaybackManager.playbackEvents = [];
      this.loadCurrentPlaylist();
    } else if(!this.current_assignments[output_name] || this.current_assignments[output_name].length === 0) {
      PlaybackManager.log("in RegionPlayerEvent::loadCurrentPlaylist:Sending region_player::no_playlist","debug");
      PlaybackManager.sendEvent('region_player::no_playlist', this.region_data);
    }
  }
};


RegionPlayerEvent.prototype._incrementCurrentPlaylist = function(output_name) {
  //Inc and send events
  this.current_index++;

  if(this.current_index >= this.current_assignments[output_name].length) {
    this.current_index = 0;
    PlaybackManager.sendEvent('region_player::looped', this.region_data, this.current_playlists[output_name]);
    if(inArray('default', PlaybackManager.playbackEvents)) { return; }
    if(PlaybackManager.playbackEvents.length > 0) {
      PlaybackManager.playbackEvents = [];
      this.loadCurrentPlaylist();
    }
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
RegionPlayerEvent.prototype.toString = function() {
  return "RegionPlayerEvent";
};
