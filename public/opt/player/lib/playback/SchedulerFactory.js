/*global
  PlaybackManager
  ConfigurationFactory
  instance
  */

/**
 * Scheduler factory will load the configured scheduler instance
 */
function SchedulerFactory() {}

/**
 * Loads the scheduler instance based on the current configuration
 */
SchedulerFactory.loadScheduler = function() {
  var config = ConfigurationFactory.getInstance();
  var concrete = config.getValue('scheduler', 'SchedulerBasic');
  PlaybackManager.log("SchedulerFactory::loadScheduler: " + concrete, "info");
  eval("var instance = new " + concrete + "()");
  instance.initialize();
  return instance;
};
