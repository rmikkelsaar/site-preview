/*global
  wmsg
  PlaybackManager
  ConfigurationFactory
  */


/**
 * A simple lazy binding event service implementation
 * Allows for a more dynamic (and less reliable) event client using the node js.
 * @return
 */
function EventService() {
  this.client = null;
  this.connected = false;
  this.failure = false;
  this.name = '';
  this.connect_timer;
  this.fail_timer;
  this.timeout = 100;
}

EventService.prototype.initialize = function(name) {
  this.name = name;
  if(typeof wmsg === 'undefined') { return false; }
  this.client = new wmsg.SocketIoClient();

  var self = this;
  this.client.onWebOpened.subscribe(function() {
    self.clearTimers();
    self.connected = true;
  });
  this.client.onWebError.subscribe(function() {
    if(self.connected) {
      self.clearTimers();
      self.connected = false;
      self.failure = true;
      self.connect();
    }
  });
  this.client.onWebClosed.subscribe(function() {
    self.clearTimers();
    if(self.connected) { self.connected = false; }
    self.connect();
  });
  this.client.onWebMessage.subscribe(function(message) {
    if(message.data && message.data.source && message.data.type && message.data.data) {
      if(!message.data.target) {
        PlaybackManager.sendEvent(
          'message_service::broadcast', message.data.type, message.data.data);
      } else if(message.data.target === self.name) {
        PlaybackManager.sendEvent(
          'message_service::message', message.data.type, message.data.data);
      }
    }
  });
  this.connect();
};

EventService.prototype.clearTimers = function() {
  if(this.connect_timer) {
    window.clearTimeout(this.connect_timer);
  }
  if(this.fail_timer) {
    window.clearTimeout(this.fail_timer);
  }
};

EventService.prototype.connect = function () {
  this.clearTimers();
  var self = this;
  this.connect_timer = window.setTimeout(function() {
    //First attempt to connect is as quickly as possible - retries have to wait;
    self.timeout = 1000;
    self.client.connect(
      ConfigurationFactory.getInstance().getValue('message_url', 'localhost'));
  }, this.timeout);
 this.fail_timer = window.setTimeout(function() {
    if(!self.connected) {
      self.failure = true;
      self.connect();
    }
  }, 30000);
};

EventService.prototype.changeName = function(name) {
  this.name = name;
};

EventService.prototype.broadcast = function(type, data) {
  var payload = {target: "", source: this.name, type: type, data: data};
  this.client.sendBroadcast(payload);
};

EventService.prototype.sendTo = function(target, type, data) {
  var payload = {target: target, source: this.name, type: type, data: data};
  this.client.sendBroadcast(payload);
};
