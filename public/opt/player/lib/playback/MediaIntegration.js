/*global
  PlaybackManager
  */

/**
 * Methods used by dynamic media assets to interact with the playback platform
 */
function MediaIntegration() {
  this.subscriptions = [];
  this.daypart_events = [];
}

MediaIntegration.extend = function(region_id, asset_id, new_time) {
  PlaybackManager.log("MediaIntegration.extend" + new_time + ":" + region_id + ":" + asset_id, "debug");
  PlaybackManager.sendEvent('media::extend', region_id, asset_id, new_time);
};

MediaIntegration.sendEvent = function(region_id, asset_id, event_name) {
  PlaybackManager.log("MediaIntegration.sendEvent: " + region_id + ":" + asset_id + ":" + event_name + ":" + arguments[3] + ":" + arguments[4] + ":" + arguments[5], "debug");
  PlaybackManager.sendEvent('media::send', region_id, asset_id, event_name, arguments[3], arguments[4], arguments[5]);
};

MediaIntegration.broadcastEvent = function(region_id, asset_id, event_name) {
  PlaybackManager.log("MediaIntegration.broadcastEvent: " + region_id + ":" + asset_id + ":" + event_name + ":" + arguments[3] + ":" + arguments[4] + ":" + arguments[5], "debug");
  PlaybackManager.sendEvent('media::broadcast', region_id, asset_id, event_name, arguments[3], arguments[4], arguments[5]);
};

MediaIntegration.addDPNotify = function(region_id, notify_time) {
  PlaybackManager.log("MediaIntegration.addDPNotify" + notify_time + ":" + region_id, "debug");
  PlaybackManager.sendEvent('media::add_dp_notify', region_id, notify_time);
};

MediaIntegration.subscribeEvent = function(region_id, asset_id, event_name) {
  PlaybackManager.log("MediaIntegration.subscribeEvent" + event_name + ":" + region_id + ":" + asset_id, "debug");
  PlaybackManager.sendEvent('media::subscribe', region_id, asset_id, event_name);
};
