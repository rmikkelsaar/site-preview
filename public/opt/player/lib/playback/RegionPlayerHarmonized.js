/*global
RegionPlayerFactory
RegionPlayerBasic
ConfigurationFactory
PlaybackManager
RegionData
DataAccessFactory
eventCompare
inTime
inArray
MediaData
*/

/**
 * Manages the playlist and playback for a synchronized region
 */
function RegionPlayerHarmonized() {
  this.harmonized_playlists = [];
  this.harmonized_assignments = [];
  this.harmonized_index = 0;
  this.harmonized_state = "NONE";
  this.output;
}

RegionPlayerHarmonized.prototype = new RegionPlayerBasic(); //Javascript inheritance
//Add this player to the list of available types
RegionPlayerFactory.regionTypes.RegionPlayerHarmonized = 'RegionPlayerHarmonized';
RegionPlayerFactory.regionTypes.Harmonized = 'RegionPlayerHarmonized';

/**
 * Loads the data for this player but does not draw or begin playback
 * @param ScheduleData schedule_data The current schedule for this region
 * @param RegionData region_data The region information for this player
 */
RegionPlayerHarmonized.prototype.initialize = function(schedule_data, region_data) {
  //Interrupt event - only master reacts
  this.on('message_service::broadcast', function(e) {
    if(e.details[0] === 'event' && ConfigurationFactory.getInstance().getValue('is_master') === 'yes') {
      if(this.harmonized_state === "NONE") {
        this.harmonized_state = "RUNNING";
        PlaybackManager.blockSchedule = true;
        PlaybackManager.playbackEvents.push(e.details[1]);
        this.loadHarmonizedPlaylists();
      }
    }
  });
  //Interrupt playback
  this.on('message_service::message', function(e) {
    if(e.details[0] === 'playback instruction') {
      PlaybackManager.blockSchedule = true;

      var region = new RegionData(e.details[1].region);
      var assignment = new MediaData(e.details[1].assignment);
      //                        PlaybackManager.playbackEvents.push('Harmonized');
      if(region.getId() === this.region_data.getId()) {
        this._executePlayback(assignment);
      }
    }
  });
  //
  this.on('player::complete', function(e) {
    if(PlaybackManager.playbackEvents.length === 0) {
      this.harmonized_state = "NONE";
    }
    if(e.details[0].getId() === this.region_data.getId() && PlaybackManager.blockSchedule) {
      PlaybackManager.blockSchedule = false;
      this.loadCurrentPlaylist();
    }
  });

  RegionPlayerBasic.prototype.initialize.call(this, schedule_data, region_data);
  this.output = ConfigurationFactory.getInstance().getValue('output_name');
  ConfigurationFactory.getInstance().setValue('output_name', ConfigurationFactory.getInstance().getValue('screen_name'));

};

/**
 * Specifically searches for harmonized playlists which are then loaded into their own var so that the main playlist is not affected
 */
RegionPlayerHarmonized.prototype.loadHarmonizedPlaylists = function() {
  var output_name = ConfigurationFactory.getInstance().getValue('screen_name');
  var playlists = this.findCurrentPlaylists();
  if(playlists[output_name]) {
    this.harmonized_assignments = [];
    for(var i in playlists) {
      this.harmonized_assignments[i] = DataAccessFactory.getInstance().loadMediaByIds(playlists[i].getAssignmentIds());
    }
    if(this.harmonized_assignments[output_name] && this.harmonized_assignments[output_name].length) {
      this.harmonized_playlists = playlists;
      PlaybackManager.sendEvent('region_player::new', this.region_data, this.harmonized_playlists[output_name]);
    }
  }
  if(this.harmonized_playlists.length === 0) {
    PlaybackManager.playbackEvents = [];
  }
};

/**
 * Finds the playlists that match the current date, dayparts and events - synchronized matches against the screen name instead of output
 * @return PlaylistData[] The current matching playlists hashed to the screen name.
 */
RegionPlayerHarmonized.prototype.findCurrentPlaylists = function() {
  //We load the playlists for all outputs in case an event is active
  var playlists = [];
  var now = new Date();
  for(var i=0;i<this.playlists.length;i++) {
    if(eventCompare(PlaybackManager.playbackEvents, this.playlists[i].getEvents()) &&
       this.playlists[i].getStartDate() <= now &&
         this.playlists[i].getEndDate() >= now &&
           (this.playlists[i].getTimeToPlays().length === 0 || inTime(now, this.playlists[i].getTimeToPlays()))) {
      playlists[this.playlists[i].getValue('screen', 'VGA-0')] = this.playlists[i];
    }
  }
  return playlists;
};


/**
 * Plays the media at the current index in the playlist
 */
RegionPlayerHarmonized.prototype.play = function() {
  this.state = 'playing';
  var assignment;
  if(PlaybackManager.playbackEvents.length && !inArray('default', PlaybackManager.playbackEvents)) {
    //If we have any events then we are in harmonized and need to play back synchronized.
    PlaybackManager.log("RegionPlayerHarmonized::play harmonized");
    var output_name = ConfigurationFactory.getInstance().getValue('screen_name');
    for(var i in this.harmonized_assignments) {
      if(i === output_name) {
        PlaybackManager.log("RegionPlayerHarmonized::play local");
        //Play ours locally
        assignment = this.harmonized_assignments[i][this.harmonized_index];
      } else {
        //Send playback event for this region across the output's event
        PlaybackManager.sendMessage(i, "playback instruction", {'region': this.region_data, 'assignment': this.harmonized_assignments[i][this.harmonized_index] });
      }
    }
    //Play next
    this._executePlayback(assignment);

    //Inc and send events
    this.harmonized_index++;
    if(this.harmonized_index >= this.harmonized_assignments[output_name].length) {
      this.harmonized_index = 0;
      PlaybackManager.playbackEvents = [];
      PlaybackManager.sendEvent('region_player::looped', this.region_data, this.harmonized_playlists[output_name]);
    }
  } else {
    PlaybackManager.blockSchedule = false;
    RegionPlayerBasic.prototype.play.call(this);
  }
};

/**
 * Stops the playback and destoys the region html
 */
RegionPlayerHarmonized.prototype.destroy = function() {
  ConfigurationFactory.getInstance().setValue('output_name', this.output);
  RegionPlayerBasic.prototype.destroy.call(this);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
RegionPlayerHarmonized.prototype.toString = function() {
  return "RegionPlayerHarmonized";
};
