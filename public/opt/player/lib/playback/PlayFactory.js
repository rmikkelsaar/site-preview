/*global
  instance
  PlaybackManager
  */

/**
 * Loads the correct player intstance based on the provided data
 */
function PlayFactory() {
  this.instances = null;
}

PlayFactory.playTypes = [];

/**
 * Instantiats the player for the provided data
 * @param RegionData region_data The data for the region
 * @param AssignmentData assignment_data The assignment data for the player
 */
PlayFactory.getPlayer = function(region_data, assignment_data) {
  if(!this.instances) {
    this.instances = [];
  }
  if(!PlayFactory.playTypes[assignment_data.getPlayCommand()]) {
    throw "Unable to find player for: " + assignment_data.getPlayCommand();
  }

  var concrete = PlayFactory.playTypes[assignment_data.getPlayCommand()];
  var key = region_data.getId() + "-" + concrete;
  if(!this.instances[key]) {
    PlaybackManager.log("PlayFactory::getPlayer: " + concrete, "info");
    eval("var instance = new " + concrete + "()");
    instance.initialize(region_data);
    this.instances[key] = instance;
  }
  return this.instances[key];
};
