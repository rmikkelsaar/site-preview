/*global
  PlaybackManager
  RegionPlayerFactory
  DataAccessFactory
  inTime
  ConfigurationFactory
  PlayFactory
  eventCompare
  playlistsCompare
  inArray
  console
  inTimeFuture
  swfobject
  */

/**
 * Manages the playlist and playback for a basic region
 * A basic region just plays media in order with no bells or whistles
 */
function RegionPlayerBasic() {
  this.region_data;
  this.playlists;
  this.schedule_data;
  this.last_error_id = 0;

  this.current_playlists = [];
  this.future_playlists = [];
  this.current_assignments = [];
  this.future_assignments = [];
  this.current_index = -1;

  this.region_node;
  this.current_player;
  this.timers;
  this.registered_offsets;

  this.failure_assignment = 0;
  this.state = 'unitialized';

}


//Add this player to the list of available types
RegionPlayerFactory.regionTypes.Standard = 'RegionPlayerBasic';
RegionPlayerFactory.regionTypes.RegionPlayerBasic = 'RegionPlayerBasic';

RegionPlayerBasic.prototype.getId = function() {
  return this.region_data.getId();
};

function Wait(){
  this.value = false;
}

/**
 * Loads the data for this region and connects to the events  but does not draw or begin playback
 * @param ScheduleData schedule_data The current schedule for this region
 * @param RegionData region_data The region information for this player
 */
RegionPlayerBasic.prototype.initialize = function(schedule_data, region_data) {
  var self = this;
  this.region_data = region_data;
  this.schedule_data = schedule_data;
  this.timers = [];
  this.registered_offsets = [];
  this.playlists = DataAccessFactory.getInstance().loadPlaylistsByScheduleRegion(schedule_data.getId(), this.region_data.getId());
  //Subscribe to events
  if(!this.on) {
    PlaybackManager.getYInstance().augment(RegionPlayerBasic, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  //New playlist loaded event
  this.on('region_player::new', function(e) {
    PlaybackManager.log("EVENT>RegionPlayerBasic::region_player::new","debug");
    PlaybackManager.log(" this.state: '"+this.state+"' regionId:"+e.details[0].getId(),"debug");
    if(e.details[0].getId() === this.region_data.getId()) {
      PlaybackManager.log(" region_player::new>this.play()","debug");
      var WaitToPlay = new Wait();
      //start timeout that will window the time to play to provide
      //the option to make the build up to play the same length of
      //time (useful in transitional syncing)
      WaitToPlay.value = true;
      var timeOut = setTimeout(function() {
        WaitToPlay.value=false;
      }, 1550);
      this.play(WaitToPlay);
    }
  });
  //Playback completed
  this.on('player::complete', function(e) {
    PlaybackManager.log("EVENT>RegionPlayerBasic::player::complete","debug");
    PlaybackManager.log("this.state: '"+this.state+"' e.details[0].getId():"+e.details[0].getId()+" this.region_data.getId():"+this.region_data.getId(),"debug");
    if(e.details[0].getId() === this.region_data.getId() && this.state === 'playing') {
      this.last_error_id = 0;
      PlaybackManager.log(" player::complete>this.loadCurrentPlaylist()","debug");
      this.loadCurrentPlaylist();
      this.play();
    }
  });
  //Error in playback
  this.on('player::error', function(e) {
    if(e.details[0].getId() === this.region_data.getId() &&
       this.state === 'playing') {
      if(e.details[1].getId() === this.last_error_id) {
        this.stop();
      } else if(this.last_error_id === 0) {
        PlaybackManager.log("EVENT>RegionPlayerBasic::player::error1","debug");
        this.last_error_id = e.details[1].getId();
        this.play();
      } else {
        PlaybackManager.log("EVENT>RegionPlayerBasic::player::error2","debug");
        this.play();
      }
    }
  });
  //Add new daypart notification
  this.on('media::add_dp_notify', function(e) {
    PlaybackManager.log("EVENT>RegionPlayerBasic::media::add_dp_notify","debug");
    if(e.details[0] === this.region_data.getId()) {
      var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
      var offsets = [];
      offsets[e.details[1]] = e.details[1] * -1000;
      this.addTimers(output_name, offsets);
    }
  });
  //Generic timer to reload playlists periodically
  this.on('global::timer', function(e)  {
    //  PlaybackManager.log("this.state: '"+this.state,"debug");
    if(this.state === 'playing' && !PlaybackManager.blockSchedule) {
      this.loadCurrentPlaylist();
    } else if(this.state === 'initialized') {
      // PlaybackManager.playbackEvents = [];
      this.loadCurrentPlaylist();
    }
  });
};

/**
 * Prepares the region on the screen for playback - should only be called once
 */
RegionPlayerBasic.prototype.prepareRegion = function() {
  var width = (this.region_data.getWidth() / this.schedule_data.getLayoutWidth() * 100);
  var height = (this.region_data.getHeight() / this.schedule_data.getLayoutHeight() * 100);
  var left = (this.region_data.getLeft() / this.schedule_data.getLayoutWidth() * 100);
  var top = (this.region_data.getTop() / this.schedule_data.getLayoutHeight() * 100);

  PlaybackManager.getYInstance().one(document.body).append('<div id="region_' +
                                                           this.region_data.getId() + '"></div>');
  this.region_node = PlaybackManager.getYInstance().one('#region_' + this.region_data.getId());
  this.region_node.setStyle('position', 'absolute');
  this.region_node.setStyle('left', left + '%');
  this.region_node.setStyle('top', top + '%');
  this.region_node.setStyle('width', width + '%');
  this.region_node.setStyle('height', height + '%');
  PlaybackManager.log("RegionPlayerBasic.prepareRegion::width:"+width+" height:"+height+" left:"+left+" top:"+top);
  this.state = 'initialized';
  this.loadCurrentPlaylist();
};

/**
 * Finds the playlists that match the current date, dayparts and events
 * @return PlaylistData[] The current matching playlists hashed to the output name.
 */
RegionPlayerBasic.prototype.findCurrentPlaylists = function() {
  //We load the playlists for all outputs in case an event is active
  var playlists = [];
  var playlistsNext = [];
  var now = new Date();
  var playlistEvents;
  var startDate;
  var endDate;
  var timeToPlayData;
  var timeToPlayLength;

  for(var i=0;i<this.playlists.length;i++) {
    playlistEvents = this.playlists[i].getEvents();
    startDate = this.playlists[i].getStartDate();
    endDate = this.playlists[i].getEndDate();
    timeToPlayData = this.playlists[i].getTimeToPlays();
    timeToPlayLength = this.playlists[i].getTimeToPlays().length;

    if(eventCompare(PlaybackManager.playbackEvents, playlistEvents) &&
       startDate <= now && endDate >= now &&
         ( timeToPlayLength === 0 || inTime( now, timeToPlayData ))) {
      playlists[this.playlists[i].getValue('output', 'VGA-0')] = this.playlists[i];
    }
  }
  // add the ttp boundry to now and get the next playlist if any?
  return playlists;
};


/**
 * Finds the playlists that match the current date, dayparts and events
 * @return PlaylistData[] The current matching playlists hashed to the output name.
 */
RegionPlayerBasic.prototype.findFuturePlaylists = function() {
  //We load the playlists for all outputs in case an event is active
  var playlists = [];
  var now = new Date();
  var playlistEvents;
  var startDate;
  var endDate;
  var timeToPlayData;
  var timeToPlayLength;
  //closure to store the value of the next daypart to look for
  //needed to share amoung the calls to find the specific daypart
  var obPrediction = function () {
    var value = 0;
    return {
      set_value: function (x) {
        value = x;
      },
      get_value: function () {
        return value;
      }
    };
  };

  var prediction = obPrediction(0);
  //find future playlists
  for(var i=0;i<this.playlists.length;i++) {
    playlistEvents = this.playlists[i].getEvents();
    startDate = this.playlists[i].getStartDate();
    endDate = this.playlists[i].getEndDate();
    timeToPlayData = this.playlists[i].getTimeToPlays();
    timeToPlayLength = this.playlists[i].getTimeToPlays().length;

    if(eventCompare(PlaybackManager.playbackEvents, playlistEvents) &&
       startDate <= now && endDate >= now &&
         ( timeToPlayLength === 0 || inTimeFuture( now, timeToPlayData, prediction ))) {
      playlists[this.playlists[i].getValue('output', 'VGA-0')] = this.playlists[i];
    }
  }
  return playlists;
};



/**
 * Searches for the current playlist and if different then reloads and fires the event regarding the load. If no new playlists are found
 * then nothing will occur so this can safely be called periodically.
 */
RegionPlayerBasic.prototype.loadCurrentPlaylist = function() {
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  var playlists = this.findCurrentPlaylists();
  if(this.current_playlists[output_name] && !playlistsCompare(this.current_playlists, playlists)) {
    PlaybackManager.log("RegionPlayerBasic::switch_playlist", "debug");
    //Playlists have changed
    this.stop();
    PlaybackManager.log("RegionPlayerBasic::loadCurrentPlaylist:Sending region_player::remove");
    PlaybackManager.sendEvent('region_player::remove', this.region_data, this.current_playlists[output_name]);
  }

  PlaybackManager.log("RegionPlayerBasic::loadCurrentPlaylist>this.current_playlists:");

  if(!this.current_playlists[output_name]) {
    if(playlists[output_name]) {
      //We load for each output in case our name ever changes due to failure monitoring - we could be on a dual screen
      this.current_assignments = [];
      for(var i in playlists) {
        this.current_assignments[i] = DataAccessFactory.getInstance().loadMediaByIds(playlists[i].getAssignmentIds());
      }
      if(this.current_assignments[output_name] && this.current_assignments[output_name].length) {
        this.current_playlists = playlists;
        this.setupTimers(output_name);
        PlaybackManager.log("RegionPlayerBasic::loadCurrentPlaylist:Sending region_player::new");
        PlaybackManager.sendEvent('region_player::new', this.region_data, this.current_playlists[output_name]);
      }
    }
    if(!this.current_assignments[output_name] || this.current_assignments[output_name].length === 0) {
      PlaybackManager.log("RegionPlayerBasic::loadCurrentPlaylist:Sending region_player::no_playlist");
      PlaybackManager.sendEvent('region_player::no_playlist', this.region_data);
    }
  }
};


/**
 * Adds a timer to the list if does not already exist
 */
RegionPlayerBasic.prototype.addTimers = function(output_name, offsets) {
  //Filter out offsets already set - done here as one registration could be associated to many dayparts
  for(var o in offsets) {
    if(inArray(o, this.registered_offsets)) {
      offsets.splice(o, 1);
    } else {
      this.registered_offsets.push(o);
    }
  }
  var now = new Date();
  var self = this;
  var times = this.current_playlists[output_name].getTimeToPlays();
  for(var t in times) {
    var time_till_end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), times[t].getEndHour(), times[t].getEndMinute(), times[t].getEndSecond(), 0) - now;
    if(time_till_end > 0) {
      for(var offset in offsets) {
        if(time_till_end + offsets[offset] > 0) {
          //Uses closure to get value of variable
          this.timers.push(window.setTimeout(
            (function(time_value, r_instance) { return function () { r_instance.timeEvent(time_value); }; }(offset, self)),
            time_till_end + offsets[offset],
            output_name));
        }
      }
    }
  }
  PlaybackManager.log("RegionPlayerBasic::timers: " + this.timers.length, "debug");
};

/**
 * Configures the timers for the events and playlist switch
 * @param output_name
 */
RegionPlayerBasic.prototype.setupTimers = function(output_name) {
  this.addTimers(output_name, {0:1000, 15:-13000, 30:-29000, 60:-59000});
};

/**
 * Clears all remaining timers
 */
RegionPlayerBasic.prototype.clearTimers = function() {
  for(var t in this.timers) {
    if(this.timers[t]) {window.clearTimeout(this.timers[t]);}
  }
  this.registered_offsets = [];
  this.timers = [];
};

/**
 * Performs the actual execution of the player against the provided assignment
 * @param MediaData assignment The assignment to plyaback within the region
 */
RegionPlayerBasic.prototype._executePlayback = function(assignment,assignmentFuture,WaitToPlay) {
  // get the player we will use for playback
  var current_player = PlayFactory.getPlayer(this.region_data, assignment);
  PlaybackManager.log("RegionPlayerBasic::region " + this.region_data.getValue('name', this.region_data.getId()) + " is playing " + assignment.getValue('name'), "info");

  if(this.current_player && this.current_player.toString() !== current_player.toString()) {
    PlaybackManager.log("RegionPlayerBasic::play hiding current player", "debug");
    this.current_player.stop();
  }
  this.current_player = current_player;
  this.current_player.prepareRegion(this.region_node, assignment, assignmentFuture, this.current_playlists);
  PlaybackManager.log("RegionPlayerBasic::region:_executePlayback", "debug");
  this.current_player.play(WaitToPlay);
};

/**
 * Increments the current playlist index and loops back to beginning if necessary
 * @param string output_name The output name for the playlist to increment.
 */
RegionPlayerBasic.prototype._incrementCurrentPlaylist = function(output_name) {
  //Inc and send events
  this.current_index++;
  if(this.current_index >= this.current_assignments[output_name].length) {
    this.current_index = 0;
    PlaybackManager.sendEvent('region_player::looped', this.region_data, this.current_playlists[output_name]);
  }
};

/**
 * Plays the media at the current index in the playlist
 */
RegionPlayerBasic.prototype.play = function(WaitToPlay) {
  this.state = 'playing';
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  this._incrementCurrentPlaylist(output_name);
  //Load current
  var assignment = this.current_assignments[output_name][this.current_index];
  var assignmentFuture;
  if (this.future_assignments[output_name]){
    assignmentFuture = this.future_assignments[output_name][0];
  }
  this._executePlayback(assignment,assignmentFuture,WaitToPlay);
};

/**
 * Pauses playback keeping playlist and players initialized
 */
RegionPlayerBasic.prototype.pause = function() {
  if(this.current_player) {
    this.current_player.stop();
  }
  this.clearTimers();
  this.state = 'paused';
};

/**
 * Stops playback and reverts back to initialized state
 */
RegionPlayerBasic.prototype.stop = function() {
  if(this.current_player) {
    this.current_player.stop();
  }
  this.clearTimers();
  this.current_schedule = null;
  this.current_assignments = [];
  this.current_index = -1;
  this.current_playlists = [];
  this.future_playlists = [];
  this.state = 'initialized';

};

/**
 * Propagates daypart events
 * @param time_left
 */
RegionPlayerBasic.prototype.timeEvent = function(time_left, output_name) {
  PlaybackManager.sendEvent('region_player::time_event', this.region_data, this.current_playlists[output_name], time_left);
  if (time_left > 0){
    PlaybackManager.log("RegionPlayerBasic::timeEvent: " + time_left, "info");
  }
  if(time_left === 0) {
    PlaybackManager.sendEvent("global::timer");
    PlaybackManager.log("change state to 'initialized","debug");
  }
};

/**
 * Stops the playback and destroys the region html
 */
RegionPlayerBasic.prototype.destroy = function() {
  this.stop();
  PlaybackManager.sendEvent('region_player::destroy', this.region_data, this.current_playlist);
  this.region_node.remove(true);
  PlaybackManager.getYInstance().Global.removeTarget(this);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
RegionPlayerBasic.prototype.toString = function() {
  return "RegionPlayerBasic";
};
