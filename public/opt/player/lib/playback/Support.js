/*global
  console
  ConfigurationFactory
  */

/**
 * Is the provided value in the array
 * @param value
 * @param array
 * @returns {Boolean}
 */
function inArray(value, array) {
  for(var i=0;i<array.length;i++) {
    if(value === array[i])  { return true; }
  }
  return false;
}

/**
 * Returns the time to play object that we are currently in within the provided list or null if none can be found
 * @param Date current_date The date object to check against
 * @param TimeToPlayData[] time_to_plays The array of time to play objects to check the time against.
 * @return TimeToPlayData The found time to play object
 */
function getTimeData(current_date, time_to_plays) {
  for(var i=0;i<time_to_plays.length;i++) {
    var current_value = current_date.getHours() * 3600 + current_date.getMinutes() * 60 + current_date.getSeconds();
    if(current_value >= time_to_plays[i].getStartValue() &&
       current_value <= time_to_plays[i].getEndValue() &&
         inArray(current_date.getDay(), time_to_plays[i].getDayOfWeekValues())) {
      return time_to_plays[i];
    }
  }
  return;
}


/**
 * Determines if the provided date is within the the list of times to play (only checks the time)
 * @param Date current_date The date object to check against
 * @param TimeToPlayData[] time_to_plays The array of time to play objects to check the time against.
 * @return boolean True if the date is within one of the times and false otherwise.
 */
function inTime(current_date, time_to_plays) {
  var ttp = getTimeData(current_date, time_to_plays);
  if(ttp) {
   // console.log("inTime::"+ttp);
    return true;
  }
  else    { return false; }
}


/**
 * Returns the time to play object that we are currently in within the provided list or null if none can be found
 * @param Date current_date The date object to check against
 * @param TimeToPlayData[] time_to_plays The array of time to play objects to check the time against.
 * @return TimeToPlayData The found time to play object
 */
function getFutureTimeData(current_date, time_to_plays, prediction) {

  var current_value;
  var startValue;
  var endValue;
  // find where we are (time_to_play we are in) and add a 1 sec offset to the endValue to make a predicted value
  for(var i=0;i<time_to_plays.length;i++) {
    current_value = current_date.getHours() * 3600 + current_date.getMinutes() * 60 + current_date.getSeconds();
    startValue = time_to_plays[i].getStartValue();
    endValue = time_to_plays[i].getEndValue();
    if(current_value >= startValue && current_value <= endValue && inArray(current_date.getDay(), time_to_plays[i].getDayOfWeekValues())) {
      prediction.set_value(endValue + 1);
    }
  }
  // get the next daypart that falls within the predicted value
  // if none then default content will be selected next
  for(var j=0;j<time_to_plays.length;j++) {
    startValue = time_to_plays[j].getStartValue();
    endValue = time_to_plays[j].getEndValue();
    if(prediction.get_value() >= startValue && prediction.get_value() <= endValue && inArray(current_date.getDay(), time_to_plays[j].getDayOfWeekValues())) {
      // console.log("date:"+current_value);
      // console.log("predicted:"+prediction.get_value());
      // console.log("start:"+startValue);
      // console.log("end:"+endValue);
      return time_to_plays[j];
    }
  }
  return;
}

/**
 * Determines if the provided date is within the the list of times to play (only checks the time)
 * @param Date current_date The date object to check against
 * @param TimeToPlayData[] time_to_plays The array of time to play objects to check the time against.
 * @return boolean True if the date is within one of the times and false otherwise.
 */
function inTimeFuture(current_date, time_to_plays, prediction) {
  // find where we are and add a 1 sec offset to the endValue to make a predicted value
  var ttp = getFutureTimeData(current_date, time_to_plays, prediction);
  if(ttp) {
    //console.log("inTimeFuture::"+ttp);
    return true;
  }
  // default content selected next... between dayparts
  else    {return false;}
}

/**
 * Are all the current events within the playlist events
 * @param current_events
 * @param playlist_events
 * @returns {Boolean}
 */
function eventCompare(current_events, playlist_events) {
  if(current_events.length < playlist_events.length) { return false; }
  if(current_events.length === playlist_events.length && playlist_events.length === 0) { return true; }
  for(var i=0;i<playlist_events.length;i++) {
    if(!inArray(playlist_events[i], current_events)) { return false; }
  }
  return true;
}

/**
 * Load the media URL based on the configuration
 * @param media
 * @returns
 */
function getMediaPath(media) {
  return ConfigurationFactory.getInstance().getMediaURI() + media.getAssetId();
}

/**
 * Are the playlists equal
 * @param playlists_one
 * @param playlists_two
 * @returns {Boolean}
 */
function playlistsCompare(playlists_one, playlists_two) {
  for(var i in playlists_one) {
    if(playlists_one[i] !== playlists_two[i]) { return false; }
  }
  return true;
}
