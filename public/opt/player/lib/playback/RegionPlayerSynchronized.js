/*global
  RegionPlayerBasic
  RegionPlayerFactory
  RegionData
  MediaData
  ConfigurationFactory
  PlaybackManager
  inTime
  eventCompare
  */

/**
 * Manages the playlist and playback for a synchronized region
 */
function RegionPlayerSynchronized() {
  this.first_run = true;
}
RegionPlayerSynchronized.prototype = new RegionPlayerBasic(); //Javascript inheritance
//Add this player to the list of available types
RegionPlayerFactory.regionTypes.RegionPlayerSynchronized = 'RegionPlayerSynchronized';
RegionPlayerFactory.regionTypes.Synchronized = 'RegionPlayerSynchronized';

/**
 * Loads the data for this player but does not draw or begin playback
 * @param ScheduleData schedule_data The current schedule for this region
 * @param RegionData region_data The region information for this player
 */
RegionPlayerSynchronized.prototype.initialize = function(schedule_data, region_data) {
  RegionPlayerBasic.prototype.initialize.call(this, schedule_data, region_data);
  ConfigurationFactory.getInstance().setValue('output_name', ConfigurationFactory.getInstance().getValue('screen_name'));
  this.on('message_service::message', function(e) {
    if(e.details[0] === 'playback instruction') {
      var region = new RegionData(e.details[1].region);
      var assignment = new MediaData(e.details[1].assignment);
      if(region.getId() === this.region_data.getId()) {
    	  //Ensure previous media is ended
    	  if(this.current_player) {
    		  this.current_player.ended();
    	  }
    	  this._executePlayback(assignment);
      }
    }
  });
};

/**
 * Searches for the current playlist and if different then loads and
 * fires the event regarding the load
 */
RegionPlayerSynchronized.prototype.loadCurrentPlaylist = function() {
  if(ConfigurationFactory.getInstance().getValue('is_master') !== 'yes') {
    //Slaves wait for instruction
    if(this.first_run) {
      PlaybackManager.sendEvent('region_player::new', this.region_data, null);
      this.first_run = false;
    }
    this.state = 'initialized';
    return;
  }
  //	ConfigurationFactory.getInstance().setValue('output_name', ConfigurationFactory.getInstance().getValue('screen_name'));
  RegionPlayerBasic.prototype.loadCurrentPlaylist.call(this);

};

/**
 * Finds the playlists that match the current date, dayparts and events - synchronized matches against the screen name instead of output
 * @return PlaylistData[] The current matching playlists hashed to the screen name.
 */
RegionPlayerSynchronized.prototype.findCurrentPlaylists = function() {
  //We load the playlists for all outputs in case an event is active
  var playlists = [];
  var now = new Date();
  for(var i=0;i<this.playlists.length;i++) {
    if(eventCompare(PlaybackManager.playbackEvents, this.playlists[i].getEvents()) &&
       this.playlists[i].getStartDate() <= now &&
         this.playlists[i].getEndDate() >= now &&
           (this.playlists[i].getTimeToPlays().length === 0 || inTime(now, this.playlists[i].getTimeToPlays()))) {
      playlists[this.playlists[i].getValue('screen', 'VGA-0')] = this.playlists[i];
    }
  }
  return playlists;
};

/**
 * Plays the media at the current index in the playlist
 */
RegionPlayerSynchronized.prototype.play = function() {
  //  PlaybackManager.log("RegionPlayerSynchronized::play");
  this.state = 'playing';
  if(ConfigurationFactory.getInstance().getValue('is_master') !== 'yes') {
    return;
  }
  if(this.first_run) {
    //Hold for 500 milliseconds to give event service chance to connect;
    //    PlaybackManager.log("RegionPlayerSynchronized::first run sleep");
    this.first_run = false;
    var self = this;
    window.setTimeout(function() {
      self.play();
    }, 500);
    return;
  }
  var output_name = ConfigurationFactory.getInstance().getValue('screen_name', 'VGA-0');
  var assignment = 0;
  this._incrementCurrentPlaylist(output_name);
  for(var i in this.current_assignments) {
    if(i === output_name) {
      //Play ours locally
      assignment = this.current_assignments[i][this.current_index];
    } else {
      //Send playback event for this region across the output's event
      PlaybackManager.sendMessage(i, "playback instruction", {'region': this.region_data, 'assignment': this.current_assignments[i][this.current_index] });
    }
  }
  if(assignment) {
    //Play next
    this._executePlayback(assignment);
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
RegionPlayerSynchronized.prototype.toString = function() {
  return "RegionPlayerSynchronized";
};
