/*global
  RegionPlayerFactory
  ConfigurationFactory
  PlaybackManager
  DataAccessFactory
  inTime
  inArray
  getTimeData
  */

/**
 * Manages the schedule data, the current playing schedule and the updating when required
 */
function SchedulerBasic() {
  this.schedules = [];
  this.active_regions = [];
  this.current_schedule;
  this.in_default = false;
  this.last_ttp_id = 0;
  this.interval;
}

/**
 * Reloads the data for the scheduler
 * The loadCurrentSchedule will have to be called to execute on the change in data
 * Publishes
 * scheduler::new - Generated when a new current schedule is set
 * scheduler::remove - Generated when an existing schedule is removed
 * scheduler::no_scheduler - Generated when no current schedule can be found
 */
SchedulerBasic.prototype.initialize = function() {
  this.schedules = DataAccessFactory.getInstance().loadSchedules();
  //Subscribe to events
  if(!this.on) {
    PlaybackManager.getYInstance().augment(
      SchedulerBasic, PlaybackManager.getYInstance().EventTarget);
  }
  PlaybackManager.getYInstance().Global.addTarget(this);
  this.on('scheduler::new', function(e) {
    PlaybackManager.log("EVENT>SchedulerBasic::scheduler::new>Found new schedule #"+e.details[0].getId(), "debug");
    this.inititalizeScheduleRegions(e.details[0]);
  });
  this.on('region_player::new', function(e) {
    PlaybackManager.log("EVENT>SchedulerBasic::region_player::new","debug");

    if(this.in_default && !inArray("default", PlaybackManager.playbackEvents)) {
      this.in_default = false;
      if(this.default_timer) {
        window.clearInterval(this.default_timer);
      }
      var region_node = PlaybackManager.getYInstance().one('#default');
      if(region_node) {
        region_node.remove(true);
      }
    }
  });
  this.on('global::timer', function(e)  {
    if(!PlaybackManager.blockSchedule) {
      this.loadCurrentSchedule();
    }
  });
  this.on('region_player::no_playlist', function(e)  {
    var self = this;
    window.setTimeout(function() {
      self.processDefault();
    }, 1000);
  });
  this.on('scheduler::no_schedule', function(e)  {
    this.destroyScheduleRegions();
    this.processDefault();
  });
  // start now
  // keep checking if the schedule has changed by reloading with the global::timer
  this.interval = window.setInterval(function() { PlaybackManager.sendEvent("global::timer"); }, 1000);
};

/**
 * Returns the schedule that should be playing at this type
 * This method simply retruns the schedule data - it does not change the running configuration
 * @return ScheduleData The current schedule information
 */
SchedulerBasic.prototype.findCurrentSchedule = function() {
  var now = new Date();
  // NOTE: the default schedule is never looked at and is always last
  for(var i=0;i<this.schedules.length;i++) {
    if(now >= this.schedules[i].getStartDate() &&
       now <= this.schedules[i].getEndDate() &&
         inTime(now, this.schedules[i].getTimeToPlays())) {
      return this.schedules[i];
    }
  }
};

/**
 * Reviews the current schedule configuration and loads the current schedule if required
 * This method is responsible for initializing the regions based on the current schedule and sending the
 * new schedule event
 */
SchedulerBasic.prototype.loadCurrentSchedule = function() {
  // seed the logic wih posibly a change in the schedule
  var schedule = this.findCurrentSchedule();
  // if ( ( schedule && this.current_schedule ) && this.current_schedule.getId() !== schedule.getId()){
  //     PlaybackManager.log("Schedule change","debug");
  //     PlaybackManager.log("this.current_schedule:"+this.current_schedule.getId(),"debug");
  //     PlaybackManager.log("schedule:"+schedule.getId(),"debug");
  // }

  if( this.current_schedule && ( schedule && schedule.getId() !== this.current_schedule.getId() ) ) {
    var ttp = getTimeData(new Date(), schedule.getTimeToPlays());
    //Only switch if the ttp has changed
    //- protects default content playback when missing regions
    if(ttp.getId() !== this.last_ttp_id) {
      //TEAR DOWN
      //NOT USED no scheduler::remove subcribers....
      // PlaybackManager.sendEvent('scheduler::remove', this.current_schedule);
      this.current_schedule = null;
    }
  }
  // if there is no currently playing schedule setup yet
  if(!this.current_schedule) {
    if(!schedule) {  // no schedules at all right now!
      // send event that will destroy the schedules regions and start default content
      PlaybackManager.sendEvent('scheduler::no_schedule');
    } else {
      //PLAY the new schedule found
      // initialize the regions for the new current schedule found
      if ( this.in_default !== true ) {
        PlaybackManager.sendEvent('scheduler::new', schedule);
      }
      this.last_ttp_id = getTimeData(new Date(), schedule.getTimeToPlays()).getId();
      this.current_schedule = schedule;
    }
  }
};

/**
 * Configures the timers for the events and playlist switch
 * @param output_name
 */
SchedulerBasic.prototype.setupTimers = function() {
  var now = new Date();
  var self = this;
  var times = this.current_schedule.getTimeToPlays();
  var getTimeEvent = function() { self.timeEvent(0); };
  for(var t in times) {
    var time_till_end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), times[t].getEndHour(), times[t].getEndMinute(), times[t].getEndSecond(), 0) - now;
    if(time_till_end > 0) {
      this.timers.push(window.setTimeout( getTimeEvent(), time_till_end + 1000));
    }
  }
};

/**
 * Propagates daypart events
 * @param time_left
 */
SchedulerBasic.prototype.timeEvent = function(time_left, output_name) {
  PlaybackManager.sendEvent('scheduler::time_event', this.current_schedule, time_left);
  if(time_left === 0) {
    PlaybackManager.sendEvent("global::timer");
  }
};

/**
 * Clears all remaining timers
 */
SchedulerBasic.prototype.clearTimers = function() {
  for(var timer in this.timers) {
    if(timer) { window.clearTimeout(timer); }
  }
};

/**
 * Deterimine if all regions are empty an dif so go into default
 */
SchedulerBasic.prototype.processDefault = function() {
  //Only go to default if all regions are active and none are playing content
  for(var i=0;i<this.active_regions.length;i++) {
    if(this.active_regions[i].state === 'playing') {
      PlaybackManager.log("state === 'playing'","debug");
      return;
    }
  }
  var default_schedule = this.getDefaultSchedule();
  // make the choice between scheduled default and system default content
  if(default_schedule && !this.in_default) {
    if(this.current_schedule) {
      var ttp = getTimeData(new Date(), this.current_schedule.getTimeToPlays());
      if(ttp) {
        this.last_ttp_id = ttp.getId();
      }
    }
    this.current_schedule = default_schedule;
    this.in_default = true; //Make sure we don't loop
    this.inititalizeScheduleRegions(this.getDefaultSchedule(), new Array("default"));
  } else {
    this.in_default = true;
    this.playSystemDefault();
  }
};


/**
 * Initializes the playback of regions for a schedule
 * This method is usually executed from the 'scheduler::new' event
 */
SchedulerBasic.prototype.inititalizeScheduleRegions = function(schedule, events) {
  this.destroyScheduleRegions();
  // looks like this helps decide on whether on not this Region is being built for default content
  // therefore is default content thought of as a type of event?
  // regionplayer::new will look at this to see if default is in the array and respond accordingly
  if(!events) {
    PlaybackManager.playbackEvents = [];
  } else {
    PlaybackManager.playbackEvents = events;
  }

  var regions = schedule.getRegions();
  var configScreenName = ConfigurationFactory.getInstance().getValue('screen_name');

  var region_player = null;
  var default_match = null;
  var outputs = [];
  var screens = [];
  //init every region
  for(var i=0;i<regions.length;i++) {
    outputs = regions[i].getValue('outputs', new Array(regions[i].getValue('output')));
    screens = regions[i].getValue('screens', new Array(regions[i].getValue('screen')));
    PlaybackManager.log("SchBasic::initSchRegions: found-> screen_name:"+
                        configScreenName + "  output:" + outputs + "  screen:" + screens, "debug");
    if(inArray(ConfigurationFactory.getInstance().getValue('output_name'), outputs)) {
      default_match = regions[i];
      // ex. check if configScreenName is defined configScreenName is not in the region hash tag 'screen:'
      if(configScreenName && !inArray(configScreenName, screens)) {
        PlaybackManager.log("SchBasic::initSchRegions: configScreenName:"+configScreenName+" doesnt match screen:"+screens);
        continue;
      }
      PlaybackManager.log("SchBasic::initSchRegions: building region_player for "+configScreenName+" "+(i+1));
      PlaybackManager.log("SchBasic::initSchRegions: region_player.prepareRegion#" + (i+1));
      region_player = RegionPlayerFactory.getInstance(regions[i], schedule);
      this.active_regions.push(region_player);
      region_player.prepareRegion();
    }
  }
  //If no regions are found use the default match
  if(this.active_regions.length === 0 && default_match) {
    region_player = RegionPlayerFactory.getInstance(default_match, schedule);
    this.active_regions.push(region_player);
    region_player.prepareRegion();

    PlaybackManager.log("SchedulerBasic::initSchRegions: building region_player for default_match and pushing into active_regions:" + (i+1));
    PlaybackManager.log(" default_match-> " + (i+1) + "  screen_name:"+ configScreenName + " output:" + outputs + " screen:" + screens);
    PlaybackManager.log("SchedulerBasic::initSchRegions: region_player.prepareRegion #" + (i+1));
  }
  if (!this.active_regions) { PlaybackManager.log("SchedulerBasic::initSchRegions:NO ACTIVE REGIONS>"+this.active_regions); }
};


/**
 * Removes all the active schedule regions and destroys the objects
 * This method is usually executed from the 'scheduler::remove' event
 */
SchedulerBasic.prototype.destroyScheduleRegions = function () {
  for(var i=0;i<this.active_regions.length;i++) {
    this.active_regions[i].destroy();
  }
  this.active_regions = [];
};

/**
 * Initializes a simple video tag and plays default content
 */
SchedulerBasic.prototype.playSystemDefault = function() {
  var region_node = PlaybackManager.getYInstance().one('#default');
  if(!region_node) {
    document.body.innerHTML = '';
    PlaybackManager.getYInstance().one(document.body).append('<div id="default"></div>');
    region_node = PlaybackManager.getYInstance().one('#default');
    region_node.setStyle('position', 'absolute');
    region_node.setStyle('left', '0px');
    region_node.setStyle('top', '0px');
    region_node.setStyle('width', '100%');
    region_node.setStyle('height', '100%');
    region_node.append('<video loop="loop" style="border: none;" id="default_video"></video>');
    var video_elem = document.getElementById('default_video');
    video_elem.style.width = region_node.get('clientWidth') + 'px';
    video_elem.style.height = region_node.get('clientHeight') + 'px';
    video_elem.src = ConfigurationFactory.getInstance().getMediaURI() + '/default/default_content.mp4';
    video_elem.play();
    this.default_timer = window.setInterval(function() {
      var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
      var url = ConfigurationFactory.getInstance().getSupportBase() + '/Watchdog.php?action=reset&output=' + output_name;
      PlaybackManager.getYInstance().io(url);
    }, 25000);
  }
};

/**
 * Returns the current schedule
 * @return ScheduleData The current schedule
 */
SchedulerBasic.prototype.getCurrentSchedule = function() {
  return this.current_schedule;
};

SchedulerBasic.prototype.getActiveRegion = function(region_id) {

  for(var r in this.active_regions) {
    if(this.active_regions[r].getId() === region_id) {
      return this.active_regions[r];
    }
  }
};

/**
 * Returns the default schedule if configured
 * @return ScheduleData The default schedule
 */
SchedulerBasic.prototype.getDefaultSchedule = function() {
  for(var i=0;i<this.schedules.length;i++) {
    if(this.schedules[i].getTimeToPlays().length === 0) {
      return this.schedules[i];
    }
  }
};

/**
 * Name of this object - used for debugging.
 * @return
 */
SchedulerBasic.prototype.toString = function() {
  return "SchedulerBasic";
};
