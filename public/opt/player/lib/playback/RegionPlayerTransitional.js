/*global
  RegionPlayerFactory
  ConfigurationFactory
  RegionPlayerEvent
  RegionPlayerBasic
  */

/**
 * Manages the playlist and playback for a basic region
 * A basic region just plays media in order with no bells or whistles
 */
function RegionPlayerTransitional() {
  this.is_full = false;
  this.first = true;
  this.original_left;
  this.original_right;
  this.original_width;
  this.original_height;
}
RegionPlayerTransitional.prototype = new RegionPlayerEvent(); //Javascript inheritance
//Add this player to the list of available types
RegionPlayerFactory.regionTypes.RegionPlayerTransitional = 'RegionPlayerTransitional';
RegionPlayerFactory.regionTypes.Transitional = 'RegionPlayerTransitional';

RegionPlayerTransitional.prototype.prepareRegion = function() {
  RegionPlayerBasic.prototype.prepareRegion.call(this);
  this.original_left = this.region_node.getStyle('left');
  this.original_right = this.region_node.getStyle('right');
  this.original_width = this.region_node.getStyle('width');
  this.original_height = this.region_node.getStyle('height');
};

RegionPlayerTransitional.prototype.play = function() {
  if(this.first) {
    this.first = false;
    var self = this;
    window.setTimeout(function() { self._realPlay(); }, 120);
  } else {
    this._realPlay();
  }
};

RegionPlayerTransitional.prototype._realPlay = function() {
  var is_full, i;
  this.state = 'playing';
  var output_name = ConfigurationFactory.getInstance().getValue('output_name', 'VGA-0');
  this._incrementCurrentPlaylist(output_name);
  //Load current
  var assignment = this.current_assignments[output_name][this.current_index];
  if(assignment.getValue('is_full', 'no') === "yes") {
    is_full = true;
  } else {
    is_full = false;
  }
  if(is_full && !this.is_full) {
    for( i in window.playback.active_regions) {
      if(window.playback.active_regions[i] !== this) {
        window.playback.active_regions[i].pause();
      }
    }
    this.region_node.setStyle('left', '0px');
    this.region_node.setStyle('top', '0px');
    this.region_node.setStyle('width', '100%');
    this.region_node.setStyle('height', '100%');
  } else if(!is_full && this.is_full) {
    this.region_node.setStyle('left', this.original_left);
    this.region_node.setStyle('right', this.original_right);
    this.region_node.setStyle('width', this.original_width);
    this.region_node.setStyle('height', this.original_height);
    for( i in window.playback.active_regions) {
      if(window.playback.active_regions[i] !== this) {
        window.playback.active_regions[i].play();
      }
    }
  }
  this.is_full = is_full;

  this._executePlayback(assignment);
  //      this._incrementCurrentPlaylist(output_name);
};

/**
 * Name of this object - used for debugging.
 * @return
 */
RegionPlayerTransitional.prototype.toString = function() {
  return "RegionPlayerBasic";
};
