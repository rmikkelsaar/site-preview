/*global
  PlaybackManager
  instance
*/

/**
 * Loads the correct player intstance based on the provided region and instantiates that instance
 */
function RegionPlayerFactory() {}
RegionPlayerFactory.regionTypes = [];

/**
 * Instantiats the region player for the provided region
 * @param RegionData region_data The data for the region - actually drives the factory creation
 * @param ScheduleData schedule_data Support data that can be used by the region player to find playscripts
 */
RegionPlayerFactory.getInstance = function(region_data, schedule) {
  if(!RegionPlayerFactory.regionTypes[region_data.getPlayerType()]) {
    throw "Unable to find player for: " + region_data.getPlayerType();
  }
  var concrete = RegionPlayerFactory.regionTypes[region_data.getPlayerType()];
  PlaybackManager.log("RegionPlayerFactory::getInstance: " + concrete, "info");
  eval("var instance = new " + concrete + "()");
  instance.initialize(schedule, region_data);
  return instance;
};
