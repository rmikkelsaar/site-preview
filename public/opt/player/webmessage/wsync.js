/**
  WSYNC peer (leader & follower) implementation.
  @package SynchronizedPlayback
  @subpackage peer
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */

                                                           /* namespace wsync */

var wsync = { };

/**
  Short-hand for getting the current time with millisecond accuracy.
  @return The current time with millisecond accuracy.
 */
wsync.getTime = function () {
  return Date.now() || (new Date()).getTime();
};

/**
  Calculates the average of a set of numbers.
  @param values An array of numbers.
  @return The average.
 */
wsync.avg = function (values) {
  return values.reduce(function (x, y) { return x + y; }, 0) / values.length;
};

/**
  Constructs a new instance of wsync.PeerError.
 */
wsync.PeerError = function (code) {
  this.code = code;
};

wsync.PeerError.MIN_FOLLOWERS_TIMEOUT = 1;
wsync.PeerError.LEADER_TIMEOUT = 2;
wsync.PeerError.SYNC_TIMEOUT = 3;
wsync.PeerError.MULTIPLE_LEADERS = 4;
wsync.PeerError.ILLEGAL_WEB_MESSAGE = 5;
wsync.PeerError.ILLEGAL_COMMAND = 6;


/**
  Constructs a new instance of wsync.Synchronizer (abstract).
  @emit onSyncFaster Synchronized playback slowed down.
  @emit onSyncSlower Synchronized playback sped up.
  @emit onSyncSeek Synchronized playback seeked.
  @emit onSynchronized Playback is synchronized.
 */
wsync.Synchronizer = function () {

  var self = this;

  /**
    Gets/sets the sync tolerance (in seconds).
   */
  var syncToleranceSec = wsync.Synchronizer.SYNC_TOLERANCE_SEC;
  Object.defineProperty(this, "syncToleranceSec", {
      "get": function () {
        return syncToleranceSec;
      },
      "set": function (value) {
        wmsg.assertNumber(value);
        syncToleranceSec = value;
      }
    });

  /**
    Gets/sets the sync rate (in percent).
   */
  var syncRatePercent = wsync.Synchronizer.SYNC_RATE_PERCENT;
  Object.defineProperty(this, "syncRatePercent", {
      "get": function () {
        return syncRatePercent;
      },
      "set": function (value) {
        wmsg.assertNumber(value);
        syncRatePercent = value;
      }
    });
  
  /**
    Gets/sets the skip tolerance (in seconds).
   */
  var skipToleranceSec = wsync.Synchronizer.SKIP_TOLERANCE_SEC;
  Object.defineProperty(this, "skipToleranceSec", {
      "get": function () {
        return skipToleranceSec;
      },
      "set": function (value) {
        wmsg.assertNumber(value);
        skipToleranceSec = value;
      }
    });

  this.haveStartCommand = wmsg.NewEvent("haveStartCommand", this);
  this.haveSyncCommand = wmsg.NewEvent("haveSyncCommand", this);
  this.haveStopCommand = wmsg.NewEvent("haveStopCommand", this);

  this.onSyncFaster = wmsg.NewEvent("onSyncFaster", this);
  this.onSyncSlower = wmsg.NewEvent("onSyncSlower", this);
  this.onSyncSeek = wmsg.NewEvent("onSyncSeek", this);
  this.onSynchronized = wmsg.NewEvent("onSynchronized", this);

  var rttMs = 0;
  Object.defineProperty(this, "rttMs", {
      "get": function () {
        return rttMs;
      },
      "set": function (value) {
        wmsg.assertNumber(value);
        rttMs = value;
      }
    });
  
  this.fireStartCommand = function (ct) {
    var cmd = new wsync.Command(wsync.Command.START);
    cmd.rttMs = rttMs;
    cmd.ct = ct;
    self.haveStartCommand.fire(cmd);
  };

  this.fireSyncCommand = function (ct) {
    var cmd = new wsync.Command(wsync.Command.SYNC);
    cmd.rttMs = rttMs;
    cmd.ct = ct;
    self.haveSyncCommand.fire(cmd);
  };

  this.fireStopCommand = function (ct) {
    var cmd = new wsync.Command(wsync.Command.STOP);
    cmd.rttMs = rttMs;
    cmd.ct = ct;
    self.haveStopCommand.fire(cmd);
  };

  this.executeCommand = function (cmd) {
    switch (cmd.id) {
      case wsync.Command.START:
        self.startSync();
        break;
      case wsync.Command.SYNC:
        self.syncTo(cmd.rttMs, cmd.ct);
        break;
      case wsync.Command.STOP:
        self.stopSync();
        break;
      default:
        break;
    }
  };

  /**
    Start synchronization. To be implemented by derived.
   */
  this.startSync = function () { };

  /**
    Synchronize playback based on the leader's time and RTT as well as the
    peer's RTT. To be implemented by derived.
    @param leaderRttMs The leader's round-trip time.
    @param leaderCurrentTime The leader's current playback time.
   */
  this.syncTo = function (leaderRttMs, leaderCurrentTime) { };

  /**
    Stop synchronization. To be implemented by derived.
   */
  this.stopSync = function () { };

};

/**
  Allowed frame difference, assuming that 60 (NTSC) will be the highest number
  of frames for any media type.
 */
wsync.Synchronizer.SYNC_TOLERANCE_SEC = 0.1; /* 100 ms */

/**
  The relative speed up or slow down of playback if a follower is out of sync.
 */
wsync.Synchronizer.SYNC_RATE_PERCENT = 0.02;

/**
  Maximum frame difference, assuming that 60 (NTSC) will be the highest number
  of frames for any media type, until playback will seek to the playback
  position.
 */
wsync.Synchronizer.SKIP_TOLERANCE_SEC = 0.5; /* 500 ms */

/**
  Constructs a new instance of wsync.SynchronizedHtmlMedia.
  Complete support for harmonized and synchronized playback of HTML audio and
  video.
  @param media The HTML media object that will be synchronized.
 */
wsync.SynchronizedHtmlMedia = function (media) {

  wsync.Synchronizer.call(this);
  var self = this;

  YAHOO.util.Event.addListener(media, "playing", handlePlaying);
  YAHOO.util.Event.addListener(media, "timeupdate", handleTimeUpdate);
  YAHOO.util.Event.addListener(media, "ended", handleEnded);

  function handlePlaying() {
    self.fireStartCommand(media.currentTime);
  };

  function handleTimeUpdate() {
    self.fireSyncCommand(media.currentTime);
  };

  function handleEnded() {
    self.fireStopCommand(media.currentTime);
  };

  this.startSync = function () {
    media.play();
  };

  this.syncTo = function (leaderRttMs, leaderCurrentTime) {
    /*
       If the browser hasn't selected a media source element yet, don't try to
       seek in the media.
     */
    switch (media.networkState) {
      case media.NETWORK_EMPTY:
      case media.NETWORK_NO_SOURCE:
        return;
      default:
        break;
    }

    if (media.paused) {
      self.startSync();
    }

    /*
       Adjust for various transmission and processing delays.
     */
    var epsilon = leaderCurrentTime + 0.001 * 0.5 * (leaderRttMs + self.rttMs);
    var drift = epsilon - media.currentTime;
    var drift_abs = Math.abs(drift);

    if (drift_abs <= self.syncToleranceSec) {
      /*
         The drift rate is in the interval that we consider synchronized.
       */
      media.playbackRate = media.defaultPlaybackRate;
      self.onSynchronized.fire();
    }
    else if (drift_abs >= self.skipToleranceSec) {
      /*
         The drift rate is too big to be nudged back into place fast enough.
       */
      try {
        media.currentTime = epsilon;
        self.onSyncSeek.fire();
      }
      catch (e) {
        /*
           Sometimes exceptions are thrown when setting currentTime. Maybe a
           buffering issue? (TODO)
         */
      }
    }
    else {
      if (drift > 0) {
        /*
           The drift rate causes us to speed up.
         */
        media.playbackRate = media.defaultPlaybackRate + self.syncRatePercent;
        self.onSyncFaster.fire();
      }
      else if (drift < 0) {
        /*
           The drift rate causes us to slow down.
         */
        media.playbackRate = media.defaultPlaybackRate - self.syncRatePercent;
        self.onSyncSlower.fire();
      }
      else {
        /* This branch should be impossible to take. */
      }
    }
  };

  this.stopSync = function () {
    media.pause();
    YAHOO.util.Event.removeListener(media, "ended", handleEnded);
    YAHOO.util.Event.removeListener(media, "timeupdate", handleTimeUpdate);
    YAHOO.util.Event.removeListener(media, "playing", handlePlaying);
  };
};
wsync.SynchronizedHtmlMedia.prototype = new wsync.Synchronizer();
wsync.SynchronizedHtmlMedia.prototype.constructor = wsync.SynchronizedHtmlMedia;

/**
  Constructs a new instance of wsync.SynchronizedFlash.
  Support only for harmonized playback.
  @param flash The Flash object that will be synchronized.
 */

wsync.SynchronizedFlash = function (flash) {

  wsync.Synchronizer.call(this);
  var self = this;

  function getTotalFrames() {
    return flash.TGetPropertyAsNumber("/", 5);
  };

  function getCurrentFrame() {
    return flash.TGetPropertyAsNumber("/", 4);
  };

  function getCurrentTime() {
    return getCurrentFrame() / getTotalFrames();
  };

  /**
    Since Flash player doesn't signal end of playback (or anything for that
    matter), we need to store the last sampled frame number to notice when we
    have looped.
   */
  var prevFrame = 0;
  /**
    Since Flash player doesn't fire _any_ events with regards to playback
    position, playback start, or playback stop and doesn't support querying the
    frames-per-second property of the stage, it makes synchronized playback nigh
    impossible without adding some kind of code support to every SWF out there.
    For now we use a timer to continuously poll the main timeline's current
    frame number. That still won't allow successful synchronized playback, but
    at least we will emit _some_ events.
   */
  var samplingTimer = new wmsg.Timer();
  samplingTimer.callback = sample;
  samplingTimer.timeoutMs = wsync.SynchronizedFlash.DEFAULT_SAMPLING_TIME_MS;
  /**
    Gets and sets the sampling rate of Flash player's playback position (ie.,
    current frame).
   */
  Object.defineProperty(this, "samplingTimeMs", {
      "get": function () {
        return samplingTimer.timeoutMs;
      },
      "set": function (value) {
        samplingTimer.timeoutMs = value;
      }
    });

  function stop() {
    samplingTimer.clearInterval();
    flash.StopPlay();
    self.fireStopCommand(1);
  };

  function start() {
    flash.Play();
    self.fireStartCommand(0);
    samplingTimer.setInterval();
  };

  function sample() {
    var cur_frame = getCurrentFrame();
    /*
       Potential issue: When the duration of a frame is longer than the sampling
       time, then playback will stop immediately. If we had access to the stage's
       frameRate attribute, we could adapt the rate to it.
     */
    if ((cur_frame === getTotalFrames()) || (cur_frame < self.prevFrame)) {
      stop();
    }
    else {
      self.prevFrame = cur_frame;
      self.fireSyncCommand(getCurrentTime());
    }
  };

  this.startSync = function () {
    start();
  };

  this.syncTo = function (leaderRttMs, leaderCurrentTime) {
    if (!flash.IsPlaying()) {
      self.startSync();
    }
    /*
       We're always behind.
     */
    self.onSyncFaster.fire();
  };

  this.stopSync = function () {
    stop();
  };
};
wsync.SynchronizedFlash.prototype = new wsync.Synchronizer();
wsync.SynchronizedFlash.prototype.constructor = wsync.SynchronizedFlash;

/**
  The default sampling rate of the frame sample timer. Based on the rate of
  events fired by the HTML media elements in Google Chrome for Linux.
 */
wsync.SynchronizedFlash.DEFAULT_SAMPLING_TIME_MS = 250;

/**
  Constructs a new instance of wsync.Command.
 */
wsync.Command = function (id) {
  /** The command identifier. */
  this.id = id;

  /** The timestamp. */
  this.ts = wsync.getTime();

  /** The round-trip time. */
  this.rttMs = null;

  /** The current time. */
  this.ct = null;
};

/**
  Start playback command identifier.
 */
wsync.Command.START = 0;

/**
  Synchronization command identifier.
 */
wsync.Command.SYNC = 1;

/**
  Stop playback command identifier.
 */
wsync.Command.STOP = 2;

wsync.RttCalibrator = function () {

  var self = this;

  /** The number of RTT measurements. */
  this.numRttTrials = 5;

  /** A collection of round-trip time measurements. */
  var rttMeasurements = [];

  /** The calculated round-trip time. */
  var rttMs = 0;
  Object.defineProperty(this, "rttMs", {
        "get": function () {
          return rttMs;
        }
      });

  this.onMeasuringStarted = wmsg.NewEvent("onMeasuringStarted", this);
  function fireStarted() {
    self.onMeasuringStarted.fire();
  };

  this.onMeasuringFinished = wmsg.NewEvent("onMeasuringFinished", this);
  function fireFinished() {
    self.onMeasuringFinished.fire();
  };

  this.haveEchoMessage = wmsg.NewEvent("haveEchoMessage", this);
  function fireHaveEchoMessage() {
    self.haveEchoMessage.fire({
        "cmd": "rtt",
        "ts": wsync.getTime()
      });
  };

  this.doCalibrate = function () {
    fireStarted();
    rttMeasurements = [];
    rttMs = 0;
    fireHaveEchoMessage();
  };

  this.processWebMessage = function (msg) {
    if (   ( msg.type !== wmsg.WebSocketClient.ECHO_MESSAGE)
        || (!msg.data)
        || ( msg.data.cmd !== "rtt")) {
      return;
    }
    rttMeasurements.push(wsync.getTime() - msg.data.ts);
    rttMs = wsync.avg(rttMeasurements);
    if (rttMeasurements.length < self.numRttTrials) {
      fireHaveEchoMessage();
    }
    else {
      fireFinished();
    }
  };

};

/**
  Constructs a new instance of wsync.Leader.
 */
wsync.Leader = function () {

  var self = this;

  this.forcePlayback = wmsg.NewEvent("forcePlayback", this);
  this.onPeerError = wmsg.NewEvent("onPeerError", this);

  /** The minimum number of followers needed to start synchronized playback. */
  var minFollowers = wsync.Leader.DEFAULT_MIN_FOLLOWERS;
  Object.defineProperty(this, "minFollowers", {
        "get": function () { return minFollowers; },
        "set": function (value) { minFollowers = value; }
      });

  /**
    The timeout after synchronized playback starts regardless of missing
    followers.
   */
  var minFollowersTimer = new wmsg.Timer();
  minFollowersTimer.callback = handleMinFollowersTimeout;
  minFollowersTimer.timeoutMs = wsync.Leader.DEFAULT_MIN_FOLLOWERS_TIMEOUT_MS;
  Object.defineProperty(this, "minFollowersTimeoutMs", {
      "get": function () {
        return minFollowersTimer.timeoutMs;
      },
      "set": function (value) {
        wmsg.assertNumber(value);
        minFollowersTimer.timeoutMs = value;
      }
    });
  /** Callback when minFollowersTimeout fires. */
  function handleMinFollowersTimeout() {
    minFollowersTimer.clearTimeout();
    self.onPeerError.fire(new wsync.PeerError(wsync.PeerError.MIN_FOLLOWERS_TIMEOUT));
    self.forcePlayback.fire();
  };

  /** The number of subscribed followers. */
  var numFollowers = 0;

  /** Callback when a follower has subscribed. */
  this.followerSubscribed = function () {
    numFollowers++;
    if (numFollowers >= self.minFollowers) {
      minFollowersTimer.clearTimeout();
      self.forcePlayback.fire();
    }
  };

  this.followerUnsubscribed = function () {
    numFollowers--;
  };

  /** Callback when RTT has been measured. */
  this.startFollowersTimeout = function () {
    minFollowersTimer.setTimeout();
  };
};

wsync.Leader.DEFAULT_MIN_FOLLOWERS = 1;
wsync.Leader.DEFAULT_MIN_FOLLOWERS_TIMEOUT_MS = 250;

/**
  Constructs a new instance of wsync.Follower.
 */
wsync.Follower = function () {

  var self = this;

  this.forcePlayback = wmsg.NewEvent("forcePlayback", this);
  this.onPeerError = wmsg.NewEvent("onPeerError", this);

  /**
    The timeout after synchronized playback starts regardless of missing leader.
   */
  var leaderTimeout = new wmsg.Timer();
  leaderTimeout.callback - handleLeaderTimeout();
  leaderTimeout.timeoutMs = wsync.Follower.DEFAULT_LEADER_TIMEOUT_MS;
  Object.defineProperty(this, "leaderTimeoutMs", {
      "get": function () {
        return leaderTimeout.timeoutMs;
      },
      "set": function (value) {
        leaderTimeout.timeoutMs = value;
      }
    });
  /** Callback when leaderTimeout fires. */
  function handleLeaderTimeout() {
    leaderTimeout.clearTimeout();
    self.onPeerError.fire(new wsync.PeerError(wsync.PeerError.LEADER_TIMEOUT));
    self.forcePlayback.fire();
  };

  this.startLeaderTimeout = function () {
    leaderTimeout.setTimeout();
  };
  this.stopLeaderTimeout = function () {
    leaderTimeout.clearTimeout();
  };
};

wsync.Follower.DEFAULT_LEADER_TIMEOUT_MS = 1000;

/**
  @param role The leader/follower to bind.
  @param synchronizer The synchronizer to bind.
  @param bridge The event channel to bind.
  @param session The session to join.
 */
wsync.SynchronizedPeer = function (role, synchronizer, bridge, session) {

  var self = this;

  var calibrator = new wsync.RttCalibrator();
  var source = null;

  Object.defineProperty(this, "role", {
      "get": function () { return role; }
    });

  Object.defineProperty(this, "synchronizer", {
      "get": function () { return synchronizer; }
    });

  Object.defineProperty(this, "bridge", {
      "get": function () { return bridge; }
    });

  Object.defineProperty(this, "session", {
      "get": function () { return session; }
    });

  function bind_calibrator() {

    function on_calibrator_have_echo_message(msg) {
      bridge.client.sendEcho(msg);
    };
    function on_client_web_message(msg) {
      calibrator.processWebMessage(msg);
    };
    function on_calibrator_started() {
      calibrator.onMeasuringFinished.subscribe(on_calibrator_finished);
      calibrator.haveEchoMessage.subscribe(on_calibrator_have_echo_message);
      bridge.client.onWebMessage.subscribe(on_client_web_message);
    };
    function on_calibrator_finished() {
      synchronizer.rttMs = calibrator.rttMs;
      bridge.client.onWebMessage.unsubscribe(on_client_web_message);
      calibrator.haveEchoMessage.unsubscribe(on_calibrator_have_echo_message);
      calibrator.onMeasuringFinished.unsubscribe(on_calibrator_finished);
    };
    function on_force_playback() {
      synchronizer.startSync();
    };

    calibrator.onMeasuringStarted.subscribe(on_calibrator_started);
    role.forcePlayback.subscribe(on_force_playback);
  };

  function bind_leader() {

    function on_synchronizer_have_start_command(cmd) {
      bridge.channel.emit(session, cmd);
    };
    function on_synchronizer_have_sync_command(cmd) {
      bridge.channel.emit(session, cmd);
    };
    function on_synchronizer_have_stop_command(cmd) {
      bridge.channel.emit(session, cmd);
    };
    function on_client_subscribed(subscription) {
      if (subscription.event === session) {
        role.followerSubscribed();
      }
    };
    function on_client_unsubscribed(unsubscription) {
      if (unsubscription.event === session) {
        role.followerUnsubscribed();
      }
    };
    function on_calibrator_finished() {
      synchronizer.haveStartCommand.subscribe(on_synchronizer_have_start_command);
      synchronizer.haveSyncCommand.subscribe(on_synchronizer_have_sync_command);
      synchronizer.haveStopCommand.subscribe(on_synchronizer_have_stop_command);
      bridge.channel.publish(session);
      bridge.channel.onClientSubscribed.subscribe(on_client_subscribed);
      bridge.channel.onClientUnsubscribed.subscribe(on_client_unsubscribed);
      role.startFollowersTimeout();
    };
    
    calibrator.onMeasuringFinished.subscribe(on_calibrator_finished);
  };

  function bind_follower () {

    function on_channel_event(cmd) {
      synchronizer.executeCommand(cmd);
    };
    function on_event_published(publication) {
      if (publication.event === session) {
        source = publication.source;
        bridge.channel.subscribe(source, session, on_channel_event);
        role.stopLeaderTimeout();
      }
    };
    function on_event_cancelled(cancellation) {
      if (cancellation.event === session) {
        bridge.channel.unsubscribe(source, session, on_channel_event);
      }
    };
    function on_calibrator_finished() {
      role.startLeaderTimeout();
      bridge.channel.onEventPublished.subscribe(on_event_published);
      bridge.channel.onEventCancelled.subscribe(on_event_cancelled);

      /* Check the yellow pages for the session. */
      bridge.channel.discover(function (yp) {
          if (yp === null) {
            return;
          }
          yp.events.forEach(function (event) {
            if (event === session) {
              source = yp.source;
              bridge.channel.subscribe(source, session, on_channel_event);
              role.stopLeaderTimeout();
            }
          });
        });
    };
    
    calibrator.onMeasuringFinished.subscribe(on_calibrator_finished);
  };

  this.bind = function () {
    bind_calibrator();
    if (role instanceof wsync.Leader) {
      bind_leader();
    }
    else if (role instanceof wsync.Follower) {
      bind_follower();
    }
    else {
      throw new Error("Unhandled peer role.");
    }
    return self;
  };

  this.unbind = function () {
    if (role instanceof wsync.Leader) {
      unbind_leader();
    }
    else if (role instanceof wsync.Follower) {
      unbind_follower();
    }
    else {
      throw new Error("Unhandled peer role.");
    }
    unbind_calibrator();
    return self;
  };

  this.join = function () {
    calibrator.doCalibrate();
  };

};
