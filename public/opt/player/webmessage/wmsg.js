/**
	WMSG client implementation.
	@package WebMessageClient
	@version 1.0.0
	@author Chris Eineke
	@copyright 2011 EK3 Technologies Inc.
 */

                                                            /* namespace wmsg */

var wmsg = { };

wmsg.NewEvent = function (name, context) {
  return new YAHOO.util.CustomEvent(name, context, false, YAHOO.util.CustomEvent.FLAT);
};

/**
  @see http://stackoverflow.com/questions/18082/validate-numbers-in-javascript-isnumeric
 */
wmsg.isNumber = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

wmsg.assertNumber = function (value) {
  if (!wmsg.isNumber(value)) {
    throw new TypeError("value must be numeric.");
  }
};

wmsg.isFunction = function (value) {
  return typeof value === 'function';
};

wmsg.assertFunction = function (value) {
  if (!wmsg.isFunction(value)) {
    throw new TypeError("value must be a Function.");
  }
};

wmsg.isNull = function (value) {
  return (value === null) || (value === undefined);
};

wmsg.assertNotNull = function (value) {
  if (wmsg.isNull(value)) {
    throw new TypeError("value must be non-null.");
  }
};

wmsg.isString = function (value) {
  return (typeof value === 'string') || (value instanceof String);
};

wmsg.assertString = function (value) {
  if (!wmsg.isString(value)) {
    throw new TypeError("value must be a string/String.");
  }
};

wmsg.Timer = function () {
  
  var self = this;
  
  var callback = null;
  this.__defineGetter__("callback", function () {
      return callback;
    });
  this.__defineSetter__("callback", function (value) {
      wmsg.assertFunction(value);
      callback = value;
    });

  var timeoutMs = null;
  this.__defineGetter__("timeoutMs", function () {
      return timeoutMs;
    });
  this.__defineSetter__("timeoutMs", function (value) {
      wmsg.assertNumber(value);
      timeoutMs = value;
    });

  var handle = null;
  
  this.setTimeout = function () {
    self.clearTimeout();
    handle = setTimeout(callback, timeoutMs);
  };

  this.setInterval = function () {
    self.clearInterval();
    handle = setInterval(callback, timeoutMs);
  };
  
  this.clearTimeout = function () {
    clearTimeout(handle);
    handle = null;
  };

  this.clearInterval = function () {
    clearInterval(handle);
    handle = null;
  };

  this.running = function () {
    return Boolean(handle);
  };

};

/**
  Constructs a new instance of wmsg.Message.
 */
wmsg.Message = function (type, data) {

  /** The message type. */
  this.type = type;

  /** The message payload. */
  this.data = data;

  /** The source of the message. */
  this.source = null;

  /** The target peer(s). */
  this.target = null;
};

/**
  Constructs a new instance of wmsg.WebSocketClient.

  @emit onWebOpening void The connection is about to be opened.
  @emit onWebOpened void The connection is open.
  @emit onWebMessage Object The connection received a web message.
  @emit onWebMessageError Object An unparseable or unstringifiable message was
  attempted to be send or received.
  @emit onWebError void The connection encountered an error.
  @emit onWebClosing void The connection is about to close.
  @emit onWebClosed CloseEvent The connection closed.
 */
wmsg.WebSocketClient = function () {

  var self = this;

  this.onWebOpening = wmsg.NewEvent("onWebOpening", this);
  this.onWebOpened = wmsg.NewEvent("onWebOpened", this);
  this.onWebMessage = wmsg.NewEvent("onWebMessage", this);
  this.onWebMessageError = wmsg.NewEvent("onWebMessageError", this);
  this.onWebError = wmsg.NewEvent("onWebError", this);
  /**
    The onWebClosing event gives a client the last chance to send some data
    before the connection is closed. There's no guarantee that any more incoming
    messages will be delivered. Ie., don't rely onWebMessage to fire a
    message after onWebClosing has fired.
   */
  this.onWebClosing = wmsg.NewEvent("onWebClosing", this);
  this.onWebClosed = wmsg.NewEvent("onWebClosed", this);

  /**
    The WebSocket used for the connection. You may use the socket's readyState
    attribute to check the state of the connection.
    @see http://dev.w3.org/html5/websockets/#dom-websocket-readystate
   */
  var socket = null;

  /**
    To get at the WebSocket used by this client.
   */
  this.__defineGetter__("socket", function () {
      return socket;
    });

  /**
    A shortcut to the readyState.
   */
  this.__defineGetter__("readyState", function () {
      return socket ? socket.readyState : null;
    });

  /**
    When the socket is established.
   */
  function handleSocketOpen() {
    self.onWebOpened.fire();
  };

  /**
    When the socket received a frame.
    @param e The event.
   */
  function handleSocketMessage(e) {
    try {
      var message = JSON.parse(e.data);
      self.onWebMessage.fire(message);
    }
    catch (ex) {
      self.onWebMessageError.fire(ex);
    }
  };

  /**
    When the socket raised an error.
   */
  function handleSocketError() {
    self.onWebError.fire();
  };

  /**
    When the socket was closed.
    @param e The event.
   */
  function handleSocketClose(e) {
    self.onWebClosed.fire(e);
  };

  /**
    Connects to the WebSocket server URL.
    @param url The server URL.
   */
  this.connect = function (url) {
    if (socket) {
      self.disconnect();
      socket.onopen = null;
      socket.onmessage = null;
      socket.onerror = null;
      socket.onclose = null;
      socket = null;
    }
    self.onWebOpening.fire();
    socket = new WebSocket(url);
    socket.onopen = handleSocketOpen;
    socket.onmessage = handleSocketMessage;
    socket.onerror = handleSocketError;
    socket.onclose = handleSocketClose;
  };

  /**
    Disconnects from the server.
   */
  this.disconnect = function () {
    self.onWebClosing.fire();
    socket.close();
  };

  /**
    Sends a message to the server. This method is here for your convenience
    only. You should use one of the send<type> methods below.
    @param message The unencoded message to send.
   */
  this.sendMessage = function (message) {
    try {
      var message_json = JSON.stringify(message);
      socket.send(message_json);
    }
    catch (ex) {
      self.onWebMessageError.fire(ex);
    }
  };

  /**
    Sends an echo message with the payload.
    @param data The payload.
   */
  this.sendEcho = function (data) {
    var message = new wmsg.Message(wmsg.WebSocketClient.ECHO_MESSAGE, data);
    self.sendMessage(message);
  };

  /**
    Sends a broadcast message with the payload.
    @param data The payload.
   */
  this.sendBroadcast = function (data) {
    var message = new wmsg.Message(wmsg.WebSocketClient.BROADCAST_MESSAGE, data);
    self.sendMessage(message);
  };

  /**
    Sends an echo-broadcast message with the payload.
    @param data The payload.
   */
  this.sendEchoBroadcast = function (data) {
    var message = new wmsg.Message(wmsg.WebSocketClient.ECHOBROADCAST_MESSAGE, data);
    self.sendMessage(message);
  };

  /**
    Sends a relay message with the payload to the target(s).
    @param data The payload.
    @param target The target(s).
   */
  this.sendRelay = function (data, target) {
    var message = new wmsg.Message(wmsg.WebSocketClient.RELAY_MESSAGE, data);
    message.target = target;
    self.sendMessage(message);
  };
};

wmsg.WebSocketClient.ECHO_MESSAGE = 0;
wmsg.WebSocketClient.BROADCAST_MESSAGE = 1;
wmsg.WebSocketClient.ECHOBROADCAST_MESSAGE = 2;
wmsg.WebSocketClient.RELAY_MESSAGE = 3;

/**
  Constructs a new instance of wmsg.SocketIoClient.

  @emit onWebOpening void The connection is about to be opened.
  @emit onWebOpened void The connection is open.
  @emit onWebMessage Object The connection received a web message.
  @emit onWebMessageError Object An unparseable or unstringifiable message was
  attempted to be send or received.
  @emit onWebError void The connection encountered an error.
  @emit onWebClosing void The connection is about to close.
  @emit onWebClosed CloseEvent The connection closed.
 */
wmsg.SocketIoClient = function () {

  var self = this;

  this.onWebOpening = wmsg.NewEvent("onWebOpening", this);
  this.onWebOpened = wmsg.NewEvent("onWebOpened", this);
  this.onWebMessage = wmsg.NewEvent("onWebMessage", this);
  this.onWebMessageError = wmsg.NewEvent("onWebMessageError", this);
  this.onWebError = wmsg.NewEvent("onWebError", this);
  /**
    The onWebClosing event gives a client the last chance to send some data
    before the connection is closed. There's no guarantee that any more incoming
    messages will be delivered. Ie., don't rely onWebMessage to fire a
    message after onWebClosing has fired.
   */
  this.onWebClosing = wmsg.NewEvent("onWebClosing", this);
  this.onWebClosed = wmsg.NewEvent("onWebClosed", this);

  /**
    The SocketIO socket used for the connection.
   */
  var socket = null;

  /**
    To get at the socket used by this client.
   */
  this.__defineGetter__("socket", function () {
      return socket;
    });

  /**
    When the socket is established.
   */
  function handleSocketOpen() {
    self.onWebOpened.fire();
  };

  /**
    When the socket received data.
    @param data The data.
   */
  function handleSocketMessage(data) {
    try {
      var message = JSON.parse(data);
      self.onWebMessage.fire(message);
    }
    catch (ex) {
      self.onWebMessageError.fire(ex);
    }
  };

  /**
    When the socket raised an error.
   */
  function handleSocketError() {
    self.onWebError.fire();
  };

  /**
    When the socket was closed.
    @param e The event.
   */
  function handleSocketClose(e) {
    self.onWebClosed.fire(e);
  };

  /**
    Connects to the SocketIo server URL.
    @param url The server URL.
   */
  this.connect = function (url) {
    if (socket) {
      self.disconnect();
      socket.removeListener('connect', handleSocketOpen);
      socket.removeListener('message', handleSocketMessage);
      socket.removeListener('error', handleSocketError);
      socket.removeListener('disconnect', handleSocketClose);
      socket = null;
    }
    self.onWebOpening.fire();
    socket = io.connect(url, { "force new connection": true });
    socket.addListener('connect', handleSocketOpen);
    socket.addListener('message', handleSocketMessage);
    socket.addListener('error', handleSocketError);
    socket.addListener('disconnect', handleSocketClose);
  };

  /**
    Disconnects from the server.
   */
  this.disconnect = function () {
    self.onWebClosing.fire();
    socket.disconnect();
  };

  /**
    Sends a message to the server. This method is here for your convenience
    only. You should use one of the send<type> methods below.
    @param message The unencoded message to send.
   */
  this.sendMessage = function (message) {
    try {
      var message_json = JSON.stringify(message);
      socket.send(message_json);
    }
    catch (ex) {
      self.onWebMessageError.fire(ex);
    }
  };

  /**
    Sends an echo message with the payload.
    @param data The payload.
   */
  this.sendEcho = function (data) {
    var message = new wmsg.Message(wmsg.SocketIoClient.ECHO_MESSAGE, data);
    self.sendMessage(message);
  };

  /**
    Sends a broadcast message with the payload.
    @param data The payload.
   */
  this.sendBroadcast = function (data) {
    var message = new wmsg.Message(wmsg.SocketIoClient.BROADCAST_MESSAGE, data);
    self.sendMessage(message);
  };

  /**
    Sends an echo-broadcast message with the payload.
    @param data The payload.
   */
  this.sendEchoBroadcast = function (data) {
    var message = new wmsg.Message(wmsg.SocketIoClient.ECHOBROADCAST_MESSAGE, data);
    self.sendMessage(message);
  };

  /**
    Sends a relay message with the payload to the target(s).
    @param data The payload.
    @param target The target(s).
   */
  this.sendRelay = function (data, target) {
    var message = new wmsg.Message(wmsg.SocketIoClient.RELAY_MESSAGE, data);
    message.target = target;
    self.sendMessage(message);
  };
};

wmsg.SocketIoClient.ECHO_MESSAGE = 0;
wmsg.SocketIoClient.BROADCAST_MESSAGE = 1;
wmsg.SocketIoClient.ECHOBROADCAST_MESSAGE = 2;
wmsg.SocketIoClient.RELAY_MESSAGE = 3;

/**
  Constructs a new instance of wmsg.EventRegistry.
 */
wmsg.EventRegistry = function () {
  
  var self = this;
  var events = {};

  /**
    @param event The event to check.
    @return Whether or not the event is registered.
   */
  this.hasEvent = function (event) {
    return events[event] !== undefined;
  };

  /**
    Adds the event to the registry.
    @param event The event to add.
    @return Whether or not the event was added.
   */
  this.addEvent = function (event) {
    if (events[event] === undefined) {
      events[event] = [];
      return true;
    }
    else {
      return false;
    }
  };

  /**
    Removes the event from the registry.
    @param event The event to remove.
    @return Whether or not the event was removed.
   */
  this.removeEvent = function (event) {
    if (events[event] !== undefined) {
      delete events[event];
      return true;
    }
    else {
      return false;
    }
  };

  /**
    @return All registered events.
   */
  this.getEvents = function () {
    return Object.keys(events);
  };

  /**
    @param event The event to query.
    @return All event listeners for the event.
   */
  this.getListeners = function (event) {
    if (events[event] === undefined) {
      return [];
    }
    else {
      return events[event];
    }
  };

  /**
    @param event The event to query.
    @param listener The listener to check for.
    @return Whether or not the listener is attached to the event.
   */
  this.hasListener = function (event, listener) {
    if (events[event] === undefined) {
      return false;
    }
    else {
      return events[event].indexOf(listener) !== -1;
    }
  };

  /**
    @param event The event to query.
    @return Whether or not the event has any listeners.
   */
  this.hasListeners = function (event) {
    if (events[event] === undefined) {
      return false;
    }
    else {
      return events[event].length > 0;
    }
  };

  /**
    @param event The event to query.
    @return How many listeners are attched to the events.
   */
  this.numListeners = function (event) {
    if (events[event] === undefined) {
      return undefined;
    }
    else {
      return events[event].length;
    }
  };

  /**
    @param event The event to attach to.
    @param listener The listener to attach to the event.
    @return Whether or not the listener was attached to the event.
   */
  this.addListener = function (event, listener) {
    if (events[event] === undefined) {
      return false;
    }

    var index = events[event].indexOf(listener);
    if (index !== -1) {
      return false;
    }
    else {
      events[event].push(listener);
      return true;
    }
  };

  /**
    @param event The event to detach from.
    @param listener The listener to detach from the event.
    @return Whether or not the listener was removed from the event.
   */
  this.removeListener = function (event, listener) {
    if (events[event] === undefined) {
      return false;
    }

    var index = events[event].indexOf(listener);
    if (index === -1) {
      return false;
    }
    else {
      events[event].splice(index, 1);
      return true;
    }
  };

};

/**
  Constructs a new instance of wmsg.Activation. Activation records store future
  invocations of functions.
  @param resultFunction The result function. Called when everything went well.
  @param errorFunction The error function. Called when something went awry.
 */
wmsg.Activation = function (resultFunction, errorFunction) {

  if (resultFunction != null) {
    wmsg.assertFunction(resultFunction);
  }
  if (errorFunction != null) {
    wmsg.assertFunction(errorFunction);
  }

  var self = this;

  var activated = false;
  var activationCnt = 0;
  this.__defineGetter__("activationCnt", function () {
      return activationCnt;
    });
  
  var aid = null;
  this.__defineGetter__("aid", function () {
      return aid;
    });
  this.__defineSetter__("aid", function (value) {
      aid = value;
    });

  var resultTimer = null;
  var errorTimer = null;
  
  /**
    Sets this activation to activated. Duh.
   */
  function activate() {
    activated = true;
    activationCnt++;
    if (resultTimer !== null) {
      resultTimer.clearTimeout();
    }
    if (errorTimer !== null) {
      errorTimer.clearTimeout();
    }
  };

  /**
    Calls the result function with the supplied parameters. If the stored
    function is null or undefined or any of the two functions have been called,
    it will not be invoked.
    @param ... The arguments forwarded to the result function.
   */
  this.callResultFunction = function (/* ... */) {
    if (activated) {
      return;
    }
    activate();
    if (wmsg.isFunction(resultFunction)) {
      resultFunction.apply(null, arguments);
    }
    return self;
  };

  /**
    Calls the error function with the supplied parameters. If the stored
    function is null or undefined or any of the two functions have been called,
    it will not be invoked.
    @param ... The arguments forwarded to the result function.
   */
  this.callErrorFunction = function (/* ... */) {
    if (activated) {
      return;
    }
    activate();
    if (wmsg.isFunction(errorFunction)) {
      errorFunction.apply(null, arguments);
    }
    return self;
  };

  /**
    Sets up a timer to call the result function.
    @param timeoutMs The timeout (in milliseconds).
    @param value The value passed to the result function.
   */
  this.setResultTimeout = function (timeoutMs, value) {
    resultTimer = new wmsg.Timer();
    resultTimer.timeoutMs = timeoutMs;
    resultTimer.callback = function () {
      self.callResultFunction(value);
    };
    resultTimer.setTimeout();
  };

  /**
    Sets up a timer to call the result function.
    @param timeoutMs The timeout (in milliseconds).
    @param value The value passed to the error function.
   */
  this.setErrorTimeout = function (timeoutMs, value) {
    errorTimer = new wmsg.Timer();
    errorTimer.timeoutMs = timeoutMs;
    errorTimer.callback = function () {
      self.callErrorFunction(value);
    };
    errorTimer.setTimeout();
  };

  /**
    Restarts this activation for later re-use.
   */
  this.restart = function () {
    activated = false;
    if (resultTimer !== null) {
      resultTimer.setTimeout();
    }
    if (errorTimer !== null) {
      errorTimer.setTimeout();
    }
  };

};

/**
  Constructs a new instance of wmsg.ActivationList.
 */
wmsg.ActivationList = function () {

  var self = this;

  var records = new Array();
  var nextActivationId = 0;

  /**
    Generates the next activation ID.
    @return The next activation ID.
   */
  function generateNextActivationId() {
    return nextActivationId++;
  };

  /**
    Adds an activation to the activation list.
    @param activation The activation to store.
    @return The created activation.
   */
  this.insert = function (activation) {
    activation.aid = generateNextActivationId();
    records[activation.aid] = activation;
    return activation;
  };

  /**
    @param aid The activation to get.
    @return An activation from this list.
   */
  this.get = function (aid) {
    return records[aid];
  };

  /**
    Overwrites a stored activation.
    @param aid The activation id to overwrite.
    @param activation The activation to store.
   */
  this.set = function (aid, activation) {
    activation.aid = aid;
    records[aid] = activation;
    return activation;
  };

  /**
    Removes an activation from the activation list.
    @param aid The activation to remove.
   */
  this.remove = function (aid) {
    var activation = records[aid];
    delete records[aid];
    return activation;
  };

};

/**
  Constructs a new instance of wmsg.EventChannel.
  @emit onEventPublished Object A remote channel published an event.
  @emit onEventCancelled Object A remote channel cancelled an event.
  @emit onEventsRevealed Object A remote channel revealed its events.
  @emit onClientSubscribed Object A remote channel subscribed to an event.
  @emit onClientUnsubscribed Object A remote channel unsubscribed from an event.
  @emit onClientDropped Object A remote channel was dropped from an event.
  @emit relayMessageReady A relay message is ready to be sent.
  @emit broadcastMessageReady A broadcast message is ready to be sent.
 */
wmsg.EventChannel = function () {

  var self = this;

  this.onEventPublished = wmsg.NewEvent("onEventPublished", this);
  this.onEventCancelled = wmsg.NewEvent("onEventCancelled", this);
  this.onEventsRevealed = wmsg.NewEvent("onEventsRevealed", this);
  this.onClientSubscribed = wmsg.NewEvent("onClientSubscribed", this);
  this.onClientUnsubscribed = wmsg.NewEvent("onClientUnsubscribed", this);
  this.onClientDropped = wmsg.NewEvent("onClientDropped", this);
  this.relayMessageReady = wmsg.NewEvent("relayMessageReady", this);
  this.broadcastMessageReady = wmsg.NewEvent("broadcastMessageReady", this);

  var registry = new wmsg.EventRegistry();
  var activations = new wmsg.ActivationList();

  /**
    Gets/sets the discovery timeout period (in milliseconds).
   */
  var discoveryTimeoutMs = wmsg.EventChannel.DISCOVERY_TIMEOUT_MS;
  this.__defineGetter__("discoveryTimeoutMs", function () {
      return discoveryTimeoutMs;
    });
  this.__defineSetter__("discoveryTimeoutMs", function (value) {
      wmsg.assertNumber(value);
      discoveryTimeoutMs = value;
    });

  /**
    Gets/sets the subscription timeout period (in milliseconds).
   */
  var subscriptionTimeoutMs = wmsg.EventChannel.SUBSCRIPTION_TIMEOUT_MS;
  this.__defineGetter__("subscriptionTimeoutMs", function () {
      return subscriptionTimeoutMs;
    });
  this.__defineSetter__("subscriptionTimeoutMs", function (value) {
      wmsg.assertNumber(value);
      subscriptionTimeoutMs = value;
    });

  /**
    Gets/sets the unsubscription timeout period (in milliseconds).
   */
  var unsubscriptionTimeoutMs = wmsg.EventChannel.UNSUBSCRIPTION_TIMEOUT_MS;
  this.__defineGetter__("unsubscriptionTimeoutMs", function () {
      return unsubscriptionTimeoutMs;
    });
  this.__defineSetter__("unsubscriptionTimeoutMs", function (value) {
      wmsg.assertNumber(value);
      unsubscriptionTimeoutMs = value;
    });

  /**
    Gets/sets the emit timeout period (in milliseconds).
   */
  var emitTimeoutMs = wmsg.EventChannel.EMIT_TIMEOUT_MS;
  this.__defineGetter__("emitTimeoutMs", function () {
      return emitTimeoutMs;
    });
  this.__defineSetter__("emitTimeoutMs", function (value) {
      wmsg.assertNumber(value);
      emitTimeoutMs = value;
    });

  function fireRelayMessageReady(data, target) {
    self.relayMessageReady.fire([data, target]);
  };

  function fireGenericErrorResponse(aid, event, reason, target) {
    fireRelayMessageReady({
        "aid": aid,
        "cmd": "nack",
        "event": event,
        "reason": reason
      },
      target);
  };

  function fireGenericResultResponse(aid, event, target) {
    fireRelayMessageReady({
        "aid": aid,
        "cmd": "ack",
        "event": event
      },
      target);
  };

  function fireBroadcastMessageReady(data) {
    self.broadcastMessageReady.fire(data);
  };

  function formatRemoteEventName(id, event) {
    return "<" + id + "::" + event + ">";
  };

  /**
    Signals that a remote channel has revealed events.
  */
  function handleReveal(msg) {
    self.onEventsRevealed.fire({
      "source": msg.source,
      "events": msg.data.events
    });
  };

  /**
    Responds to a yellow pages request with a directed message containing a
    collection of published events.
   */
  function handleYellowPagesRequest(msg) {
    fireRelayMessageReady({
        "aid": msg.data.aid,
        "cmd": "yp!",
        "events": registry.getEvents()
      },
      msg.source);
  };

  /**
    Forwards the result of the response to our yellow pages request to the user.
   */
  function handleYellowPagesResponse(msg) {
    var activation = activations.get(msg.data.aid);
    if (activation === undefined) {
      return;
    }
    activation.callResultFunction({
        "source": msg.source,
        "events": msg.data.events
      });
  };

  /**
    Subscribes the remote listener to the requested event and responds with a
    directed positive or negative acknowledgement.
   */
  function handleSubscribe(msg) {
    var source = msg.source;
    var event = msg.data.event;
    var aid = msg.data.aid;

    if (!registry.hasEvent(event)) {
      fireGenericErrorResponse(aid, event, "not_published", source);
      return;
    }
    if (registry.hasListener(event, source)) {
      fireGenericErrorResponse(aid, event, "already_subscribed", source);
      return;
    }
    registry.addListener(event, source);
    fireGenericResultResponse(aid, event, source);
    self.onClientSubscribed.fire({
        "event": msg.data.event,
        "source": msg.source
      });
  };

  /**
    Unsubscribes the remote listener from the requested event and responds with
    a positive or negative acknowledgement.
   */
  function handleUnsubscribe(msg) {
    var source = msg.source;
    var event = msg.data.event;
    var aid = msg.data.aid;

    if (!registry.hasEvent(event)) {
      fireGenericErrorResponse(aid, event, "not_published", source);
      return;
    }
    if (!registry.hasListener(event, source)) {
      fireGenericErrorResponse(aid, event, "not_subscribed", source);
      return;
    }
    registry.removeListener(event, source);
    fireGenericResultResponse(aid, event, source);
    self.onClientUnsubscribed.fire({
        "event": msg.data.event,
        "source": msg.source
      });
  };

  /**
    Call the positive result function in the activation to generically handle a
    positive result from a remote channel.
   */
  function handleResult(msg) {
    var activation = activations.remove(msg.data.aid);
    if (activation === undefined) {
      return;
    }
    activation.callResultFunction();
  };

  /**
    Call the negative error function in the activation to generically handle a
    negative result from a remote channel.
   */
  function handleError(msg) {
    var activation = activations.remove(msg.data.aid);
    if (activation === undefined) {
      return;
    }
    activation.callErrorFunction(msg.data.reason);
  };

  /**
    Signals that a remote channel has published an event.
   */
  function handlePublish(msg) {
    self.onEventPublished.fire({
        "source": msg.source,
        "event": msg.data.event
      });
  };

  /**
    Signals that a remote channel has cancelled an event.
   */
  function handleCancel(msg) {
    var fqn = formatRemoteEventName(msg.source, msg.data.event);
    if (!registry.hasEvent(fqn)) {
      return;
    }
    registry.removeEvent(fqn);
    self.onEventCancelled.fire({
        "source": msg.source,
        "event": msg.data.event
      });
  };

  /**
    Forwards the event that a remote channel emitted to the local listener(s).
   */
  function handleEmit(msg) {
    var fqn = formatRemoteEventName(msg.source, msg.data.event);
    if (!registry.hasEvent(fqn)) {
      fireRelayMessageReady({ "aid": msg.data.aid, "cmd": "nack" }, msg.source);
      return;
    }
    /*
      If the event exists locally, then there's at least one listener attached
      to it.
     */
    fireRelayMessageReady({ "aid": msg.data.aid, "cmd": "ack" }, msg.source);
    registry.getListeners(fqn).forEach(function (listener) {
      listener.call(null, msg.data.data);
    });
  };

  /**
    Dispatches a message to the respective handler.
   */
  this.processWebMessage = function (msg) {
    switch (msg.data.cmd) {
      case "reveal":
        handleReveal(msg);
        break;
      case "yp?":
        handleYellowPagesRequest(msg);
        break;
      case "yp!":
        handleYellowPagesResponse(msg);
        break;
      case "subscribe":
        handleSubscribe(msg);
        break;
      case "unsubscribe":
        handleUnsubscribe(msg);
        break;
      case "ack":
        handleResult(msg);
        break;
      case "nack":
        handleError(msg);
        break;
      case "publish":
        handlePublish(msg);
        break;
      case "cancel":
        handleCancel(msg);
        break;
      case "emit":
        handleEmit(msg);
        break;
      default:
        break;
    }
  };

  /**
    Broadcasts all published events.
   */
  this.reveal = function () {
    fireBroadcastMessageReady({
      "cmd": "reveal",
      "events": registry.getEvents()
    });
    return true;
  };

  /**
    Broadcasts a request for published events. The result function will be
    called with null if the timeout was triggered.
    @param result Function receiving the result.
    @param error Function receiving the timeout.
   */
  this.discover = function (result, error) {
    var activation = activations.insert(
        new wmsg.Activation(
          function (yp) {
            if (wmsg.isFunction(result)) {
              result.call(null, yp);
            }
            if (yp === null) {
              activations.remove(activation.aid);
            }
            else {
              activation.restart();
            }
          },
          function () {
            if (wmsg.isFunction(error)) {
              error.call(null, error);
            }
            activations.remove(activation.aid);
          }));
    activation.setResultTimeout(discoveryTimeoutMs, null);
    fireBroadcastMessageReady({
        "aid": activation.aid,
        "cmd": "yp?"
      });
    return true;
  };

  /**
    Broadcasts that this channel will emit an event.
    @param event The event.
   */
  this.publish = function (event) {
    wmsg.assertString(event);
    if (registry.hasEvent(event)) {
      return false;
    }
    registry.addEvent(event);
    fireBroadcastMessageReady({
        "cmd": "publish",
        "event": event
      });
    return true;
  };

  /**
    Broadcasts that this channel will no longer emit an event.
    @param event The event.
   */
  this.cancel = function (event) {
    wmsg.assertString(event);
    if (!registry.hasEvent(event)) {
      return false;
    }
    registry.removeEvent(event);
    fireBroadcastMessageReady({
        "cmd": "cancel",
        "event": event
      });
    return true;
  };

  /**
    Emits an event.
    @param event The event name.
    @param data The event data.
   */
  this.emit = function (event, data) {
    wmsg.assertString(event);
    if (!registry.hasEvent(event)) {
      return false;
    }
    var listeners = registry.getListeners(event);
    listeners.forEach(function (listener) {
        var activation = activations.insert(
          new wmsg.Activation(
            function () {
              activations.remove(activation.aid);
            },
            function (error) {
              registry.removeListener(event, listener);
              self.onClientDropped.fire({
                "event": event,
                "listener": listener
              });
              activations.remove(activation.aid);
            }));
        activation.setErrorTimeout(emitTimeoutMs);
        fireRelayMessageReady({
            "aid": activation.aid,
            "cmd": "emit",
            "event": event,
            "data": data
          },
          listener);
      });
    return true;
  };

  /**
    Subscribes the listener to the event emitted by the remote channel. The
    listener will be attached to the remote channel when the remote channel has
    acknowledged the subscription or it will be attached right away if there's
    a different listener for the event emitted by that event channel.
    @param id The remote channel's client id.
    @param event The event.
    @param listener The listener.
    @param result Callback function that signals much success.
    @param error Callback function that signals much fail.
   */
  this.subscribe = function (id, event, listener, result, error) {
    wmsg.assertNotNull(id);
    wmsg.assertString(event);
    wmsg.assertFunction(listener);

    var fqn = formatRemoteEventName(id, event);
    if (!registry.hasEvent(fqn)) {
      var activation = activations.insert(
          new wmsg.Activation(
            function () {
              registry.addEvent(fqn);
              registry.addListener(fqn, listener);
              if (wmsg.isFunction(result)) {
                result.call(null);
              }
              activations.remove(activation.aid);
            },
            function (reason) {
              if (wmsg.isFunction(error)) {
                error.call(null, reason);
              }
              activations.remove(activation.aid);
            }));
      activation.setErrorTimeout(subscriptionTimeoutMs);
      fireRelayMessageReady({
          "aid": activation.aid,
          "cmd": "subscribe",
          "event": event
        },
        id);
      return;
    }
    else if (!registry.hasListener(fqn, listener)) {
      registry.addListener(fqn, listener);
      if (wmsg.isFunction(result)) {
        result.call(null);
      }
    }
    else {
      if (wmsg.isFunction(error)) {
        error.call(null, "already_subscribed");
      }
    }
  };

  /**
    Unsubscribes the listener from the event emitted by the remote channel. The
    listener will be attached to the remote channel when the remote channel has
    acknowledged the subscription.
    @param id The remote channel's id or null.
    @param event The event.
    @param listener The listener.
    @param result Callback function that signals much success.
    @param error Callback function that signals much fail.
   */
  this.unsubscribe = function (id, event, listener, result, error) {
    wmsg.assertNotNull(id);
    wmsg.assertString(event);
    wmsg.assertFunction(listener);

    var fqn = formatRemoteEventName(id, event);
    if (!registry.hasEvent(fqn)) {
      if (wmsg.isFunction(error)) {
        error.call(null, "not_published");
      }
    }
    else if (!registry.hasListener(fqn, listener)) {
      if (wmsg.isFunction(error)) {
        error.call(null, "not_subscribed");
      }
    }
    else if (registry.numListeners(fqn) > 1) {
      registry.removeListener(fqn, listener);
      if (wmsg.isFunction(result)) {
        result.call(null);
      }
    }
    else {
      var activation = activations.insert(
          new wmsg.Activation(
            function () {
              registry.removeListener(fqn, listener);
              registry.removeEvent(fqn);
              if (wmsg.isFunction(result)) {
                result.call(null);
              }
              activations.remove(activation.aid);
            },
            function (reason) {
              registry.removeListener(fqn, listener);
              registry.removeEvent(fqn);
              if (wmsg.isFunction(error)) {
                error.call(null, reason);
              }
              activations.remove(activation.aid);
            }));
      activation.setErrorTimeout(unsubscriptionTimeoutMs);
      fireRelayMessageReady({
          "aid": activation.aid,
          "cmd": "unsubscribe",
          "event": event
        },
        id);
    }
  };

};

/**
  The default discovery timeout. If there were no other EventChannelS on the
  channel, then calling .discover() would never return any results and the
  activation record would be kept indefinitely. This timer prevents that.
 */
wmsg.EventChannel.DISCOVERY_TIMEOUT_MS = 5000;

/**
  The default subscription timeout. If the remote party became unavailable while
  the local party was waiting for a response to .subscribe(), then the local
  party would never get any results and the activation record would be kept
  indefinitely. This timer prevents that.
 */
wmsg.EventChannel.SUBSCRIPTION_TIMEOUT_MS = 5000;

/**
  The default unsubscription timeout. If the remote party became unavailable
  while the local party was waiting for a response to .unsubscribe(), then the
  local party would never get any results and the activation would be kept
  indefinitely. This timer prevents that.
 */
wmsg.EventChannel.UNSUBSCRIPTION_TIMEOUT_MS = 5000;

/**
  The default emission timeout. If the remote party became unavailable while the
  local party was waiting for the acknowledgement of .emit(), then the local
  party would never notice that the party became unavailable. This timer
  prevents that.
 */
wmsg.EventChannel.EMIT_TIMEOUT_MS = 1000;

/**
  @param client The client to use.
  @param channel The channel to use.
 */
wmsg.WebSocketEventChannel = function (client, channel) {

  var self = this;

  /**
    Gets the client.
   */
  this.__defineGetter__("client", function () {
      return client;
    });

  /**
    Gets the channel.
   */
  this.__defineGetter__("channel", function (){
      return channel;
    });

  function on_web_message(data) {
    channel.processWebMessage(data);
  };

  function on_relay_message_ready(data) {
    client.sendRelay(data[0], data[1]);
  };

  function on_broadcast_message_ready(data) {
    client.sendBroadcast(data);
  };

  this.bind = function () {
    client.onWebMessage.subscribe(on_web_message);
    channel.relayMessageReady.subscribe(on_relay_message_ready);
    channel.broadcastMessageReady.subscribe(on_broadcast_message_ready);
    return self;
  };

  this.unbind = function () {
    channel.broadcastMessageReady.unsubscribe(on_broadcast_message_ready);
    channel.relayMessageReady.unsubscribe(on_relay_message_ready);
    client.onWebMessage.unsubscribe(on_web_message);
    return self;
  };
};

wmsg.SocketIoEventChannel = wmsg.WebSocketEventChannel;

/**
  @param wsec The web socket event channel to probe.
 */
wmsg.WsecProbe = function (wsec) {

  var self = this;
 
  /**
    Gets the web socket event channel.
   */
  this.__defineGetter__("wsec", function (){
      return wsec;
    });

  function print(str) {
    console.debug("WsecProbe: "+str);
  };

  function on_web_opening() {
    print("Connection is opening.");
  };

  function on_web_opened() {
    print("Connection is opened.");
  };

  function on_web_message(msg) {
    print("Connection received message: "+msg.toString());
  };

  function on_web_message_error(ex) {
    print("Connection encountered message error: "+ex.toString());
  };

  function on_web_error(ex) {
    print("Connection encountered message error: "+ex.toString());
  };

  function on_web_closing() {
    print("Connection is closing.");
  };

  function on_web_closed() {
    print("Connection is closed.");
  };
    
  function on_event_published(e) {
    print("'"+e.source+"' published event '"+e.event+"'");
  };
  
  function on_event_cancelled(e) {
    print("'"+e.source+"' cancelled event '"+e.event+"'");
  };

  function on_client_subscribed(e) {
    print("'"+e.source+"' subscribed to '"+e.event+"'");
  };

  function on_client_unsubscribed(e) {
    print("'"+e.source+"' unsubscribed from '"+e.event+"'");
  };

  function on_client_dropped(e) {
    print("'"+e.listener+"' was dropped from '"+e.event+"'");
  };

  function on_relay_message_ready(e) {
    print("Relay message ready: "+e.toString());
  };

  function on_broadcast_message_ready(e) {
    print("Broadcast message ready: "+e.toString());
  };

  this.bind = function () {
    wsec.client.onWebOpening.subscribe(on_web_opening);
    wsec.client.onWebOpened.subscribe(on_web_opened);
    wsec.client.onWebMessage.subscribe(on_web_message);
    wsec.client.onWebMessageError.subscribe(on_web_message_error);
    wsec.client.onWebError.subscribe(on_web_error);
    wsec.client.onWebClosing.subscribe(on_web_closing);
    wsec.client.onWebClosed.subscribe(on_web_closed);
    wsec.channel.onEventPublished.subscribe(on_event_published);
    wsec.channel.onEventCancelled.subscribe(on_event_cancelled);
    wsec.channel.onClientSubscribed.subscribe(on_client_subscribed);
    wsec.channel.onClientUnsubscribed.subscribe(on_client_unsubscribed);
    wsec.channel.onClientDropped.subscribe(on_client_dropped);
    wsec.channel.relayMessageReady.subscribe(on_relay_message_ready);
    wsec.channel.broadcastMessageReady.subscribe(on_broadcast_message_ready);
  };

  this.unbind = function () {
    wsec.channel.broadcastMessageReady.unsubscribe(on_broadcast_message_ready);
    wsec.channel.relayMessageReady.unsubscribe(on_relay_message_ready);
    wsec.channel.onClientDropped.unsubscribe(on_client_dropped);
    wsec.channel.onClientUnsubscribed.unsubscribe(on_client_unsubscribed);
    wsec.channel.onClientSubscribed.unsubscribe(on_client_subscribed);
    wsec.channel.onEventCancelled.unsubscribe(on_event_cancelled);
    wsec.channel.onEventPublished.unsubscribe(on_event_published);
    wsec.client.onWebClosed.unsubscribe(on_web_closed);
    wsec.client.onWebClosing.unsubscribe(on_web_closing);
    wsec.client.onWebError.unsubscribe(on_web_error);
    wsec.client.onWebMessageError.unsubscribe(on_web_message_error);
    wsec.client.onWebMessage.unsubscribe(on_web_message);
    wsec.client.onWebOpened.unsubscribe(on_web_opened);
    wsec.client.onWebOpening.unsubscribe(on_web_opening);
  };

};
