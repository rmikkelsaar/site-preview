<?php

class WebSocketClient {
    protected $socket;
    protected $host;
    
    public function connect($url) {
        $errno = 0;
        $errstr = '';
        $matches = array();
        if(!preg_match('/^ws:\/\/(.+):(\d+)$/', $url, $matches))
            throw new Exception('Unable to parse the local web socket server url');
        $this->socket = @fsockopen($matches[1], $matches[2], $errno, $errstr, 2);
        if(empty($this->socket))
            throw new Exception(sprintf("%d - %s", $errno, $errstr));
        $this->host = $matches[1];
    }
    
    public function sendBroadcast($type, $data) {
        
        $message = json_encode(array('type' => 1,
                                     'source' => '',
                                     'target' => '',
                                     'data' => array('target' => '',
                                                     'source' => 'system',
                                                     'type' => $type,
                                                     'data' => $data)));
        $errno = 0;
        $errstr = '';
        $head = sprintf("GET / HTTP/1.1\r\n".
                        "Upgrade: WebSocket\r\n".
                        "Connection: Upgrade\r\n".
                        "Origin: %s\r\n".
                        "Host: %s\r\n".
                        "Content-Length: %d\r\n\r\n",
                        $_SERVER['SERVER_NAME'], $this->host, $message);
        $return = @fwrite($this->socket, $head);
        if(empty($return))
            throw new Exception(sprintf("%d - %s", $errno, $errstr));
        $headers = fread($this->socket, 2000);
        
        $return = @fwrite($this->socket, sprintf("\x00%s\xff", $message));
        if(empty($return))
            throw new Exception(sprintf("%d - %s", $errno, $errstr));
        ////WebSocket handshake
        fclose($this->socket); 
    }
    
    public function sendMessage($target, $type, $data) {
        
    }
}