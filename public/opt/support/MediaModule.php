<?php
require_once('config.php');
require_once('ConfigManager/CM_ConfigFactory.php');
require_once('CommonAdapter/grid/COM_ConsumerContext.php');
require_once('MediaService/service/MSP_MediaSession.php');
require_once('MediaService/service/asset/MSP_MediaAssetLoader.php');

$media_id = $_REQUEST['media_id'];
$media_path = sprintf('%s/%d', LOCAL_MEDIA, $media_id);
if(file_exists($media_path)) exit;    

$session = COM_GridSecurity::login(USER, PASS);
$consumer = COM_ConsumerContext::bootstrap(REG_SERVICE);
CM_ConfigFactory::setInstance($consumer);
MSP_MediaSession::initFromConsumer($consumer);

$asset = MSP_MediaAssetLoader::loadByid($media_id);
if($asset->getMediaType()->getMediaClassName() == 'VIDEOS' && $asset->getMediaType()->getServerName() != 'SWF') {
    if(file_exists('/tmp/media_module.lock')) return;
    touch('/tmp/media_module.lock');
    $extension = $asset->getMediaType()->getExtensions();
    $source_path = sprintf('/tmp/%d.%s', $asset->getId(), array_pop($extension));
    $target_name = sprintf('%d.mp4', $asset->getId());
    $asset->getCurrentConfiguration()->getFile($source_path);
    exec(sprintf('cd /tmp && /usr/local/bin/ffmpeg -i %s -an -y -vcodec libx264 -b 8000000 -vpre slow -threads 0 -pass 1 /tmp/raw_%s', $source_path, $target_name));
    exec(sprintf('cd /tmp && /usr/local/bin/ffmpeg -i %s -acodec libfaac -ab 128k -y -vcodec libx264 -b 8000000 -vpre slow -threads 0 -pass 2 /tmp/raw_%s', $source_path, $target_name));
    exec(sprintf('cd /tmp && /usr/local/bin/qt-faststart /tmp/raw_%s %s', $target_name, $media_path));
    unlink('/tmp/media_module.lock');
} else {
    $asset->getCurrentConfiguration()->getFile($media_path);
}
