<?php
require_once('config.php');

foreach(array('x', 'y', 'width', 'height', 'report_name', 'length') as $variable) {
    if(!isset($_REQUEST[$variable])) return;
}
$log_entry = sprintf("%s %s %d %d %d %d %d\n", strftime('%H:%M:%S'),
                                               $_REQUEST['report_name'],
                                               $_REQUEST['x'],
                                               $_REQUEST['y'],
                                               $_REQUEST['width'],
                                               $_REQUEST['height'],
                                               $_REQUEST['length'] / 1000);
$file = sprintf("%s/datalog_%s",LOG_PATH, strftime('%d-%m-%Y'));
file_put_contents($file, $log_entry, FILE_APPEND);
