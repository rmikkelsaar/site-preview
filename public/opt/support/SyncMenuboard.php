<?php

/**
 * At startup the data synchronization service must retrieve the menuboard data from each neighbour device
 * and determine the most recent to use based on the generated timestamp.
 *
 * @author Bing You
 * @copyright EK3 Technologies Inc.
 * @version 1.0
 */

require_once('LpiConfig.php');
require_once('LpiFunctions.php');

$xmlTmpFile = '/tmp/menuboard.xml';
$config = new LPIConfig();

$menuboardFileName = preg_replace("/\\.[^.\\s]{3,4}$/", "", substr(strrchr($config->xmlFinalData, "/"), 1));

$max_retries = 10;
$retry_count = 0;

$neighbours = getNeighboursAddress();
$noResponsedNeighbours = array();

while (!empty($neighbours) && $max_retries > $retry_count ) { 	
 	
	$noResponsedNeighbours = array();
	
	foreach($neighbours as $nbAddress) {

		$fp = fopen($xmlTmpFile, 'w');
		$url  = 'http://'.$nbAddress.'/data/'.$menuboardFileName;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		$data = curl_exec($ch);
			
		// Check if any error occured
		if( curl_errno($ch) ) {
			array_push($noResponsedNeighbours, $nbAddress );

			curl_close($ch);
			fclose($fp);
		} else {
			curl_close($ch);
			fclose($fp);

    		if (mime_content_type($xmlTmpFile) == 'application/xml') {
				OverWriteMenuboard($xmlTmpFile, $config->schemaFinalData, $config->xmlFinalData);
			} else {
				error_log('Sorry. The neighbour device ['.$entry.'] has not valid menuboard.xml.', 0);
			}
		}
	}

	$neighbours = $noResponsedNeighbours;	
	++$retry_count;
 	
 	switch ( count($neighbours) ) {
 		case 1:
 			sleep(9);
 			break;
 		case 2:
 		    sleep(6);
 		    break;
 		case 3:
 			sleep(3);	    	 		
 	} 
}

foreach($neighbours as $nbAddress) {
	error_log('Sorry. The neighbour device ['.$nbAddress.'] has not responded to your activation request.', 0);
}
?>
