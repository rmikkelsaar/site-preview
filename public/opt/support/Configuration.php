<?php
require_once('config.php');

if(empty($_REQUEST['request'])) exit;
$config = json_decode(file_get_contents('config.json'));
if($_REQUEST['request'] == 'debug_dump') {
    echo '<pre>';
    print_r($config);
} elseif($_REQUEST['request'] == 'dump') {
    echo json_encode($config);
}
if(empty($_REQUEST['name'])) exit;
if($_REQUEST['request'] == 'get') {
    if(isset($config->{$_REQUEST['name']})) {
        echo json_encode($config->{$_REQUEST['name']});
    } else {
        json_encode(null);
    }
} elseif($_REQUEST['request'] == 'set') {
    if(empty($_REQUEST['value']) && isset($config->{$_REQUEST['name']}))
        unset($config->{$_REQUEST['name']});
    else
        $config->{$_REQUEST['name']} = $_REQUEST['value'];
    file_put_contents('config.json', json_encode($config));
    echo 1;
}