<?php

/**
* The service will provide a method to retrieve the menuboard data for consumers that are originating from
* one of the neighbour devices at the location.
* 
* @author Bing You
* @copyright EK3 Technologies Inc.
* @version 1.0
*/

require_once('config.php');

if(isset($_REQUEST['xmlname'])){
	if(preg_match('/^([A-Za-z0-9_\-]+)$/', $_REQUEST['xmlname'])) {
		$xmlDownloadFile = sprintf('/opt/data/%s.xml', $_REQUEST['xmlname']);
		 
		if(file_exists($xmlDownloadFile)){

			header('Pragma: public');
			header('Expires: 0');
			header('Content-type: application/xml');
			header('Content-Disposition: attachment; filename='.basename($xmlDownloadFile));
			header('Content-Transfer-Encoding: binary');
			header('Cache-Control: must-revalidate');
			header('Content-Length: '.filesize($xmlDownloadFile));
			ob_clean();
			flush();
			readfile($xmlDownloadFile);
		} else {
			header('HTTP/1.0 404 Not Found');
			echo "<h1>404 Not Found</h1>";
			echo "The file that have requested cound not be found.";
		}
	}
}
exit();
