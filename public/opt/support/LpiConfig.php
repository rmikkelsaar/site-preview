<?php

/**
* LPI service config file located in /etc/lpi_conf.xml
* 
* @author Bing You
* @copyright EK3 Technologies Inc.
* @version 1.0
*/

class LPIConfig
{
	public $schemaCustomers;
	public $schemaStandInput;

	public $xmlFinalData;
	public $schemaFinalData;

	public $serverUrl;

	public function __construct() {
		$configDom = new DOMDocument;
		if ($configDom->load('/etc/lpi_conf.xml')) {
			$customNodes = $configDom->getElementsByTagName('custom_input');

			$this->schemaCustomers = array();
			for ($i = 0; $i < $customNodes->length; $i ++)
			{
				$customNode = $customNodes->item($i);
				array_push($this->schemaCustomers, $customNode->getElementsByTagName('schema')->item(0)->nodeValue);
			}

			$standardNodes = $configDom->getElementsByTagName('standard_input');
			$this->schemaStandardInput = $standardNodes->item(0)->getElementsByTagName('schema')->item(0)->nodeValue;

			$menuboardNodes = $configDom->getElementsByTagName('menuboard');
			$this->xmlFinalData = $menuboardNodes->item(0)->getElementsByTagName('xml')->item(0)->nodeValue;
			$this->schemaFinalData = $menuboardNodes->item(0)->getElementsByTagName('schema')->item(0)->nodeValue;
				
			$serverUrlNodes = $configDom->getElementsByTagName('server_url');
			$this->serverUrl = $serverUrlNodes->item(0)->nodeValue;

		} else {
			die('Sorry. The Location POS Integration config file is invalid.');
		}
	}
}