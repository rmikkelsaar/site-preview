<?php 
require_once('config.php');
require_once('lib/WebSocketClient.class.php');

if(empty($_REQUEST['button'])) {
    echo 0;
    exit;    
} elseif(!file_exists('/var/state/local/is_master')) {
    echo 0;
    exit;
}

$ws_url = 'http://localhost:15526'; 
if(file_exists(LOCAL_CONFIG)) {
    $config = json_decode(file_get_contents(LOCAL_CONFIG));
    if(isset($config->message_url))
        $ws_url = $config->message_url;
}
#URL Mangling for old web socket - will upgrade to rest interface for 2.1
$ws_url = str_replace('http://', 'ws://', $ws_url);
$ws_url = str_replace(':15526', ':15525', $ws_url);
$client = new WebSocketClient;
$client->connect($ws_url);
$client->sendBroadcast('event', sprintf('button %d', $_REQUEST['button']));
echo 1;
