<?php

/**
 * A standard POST interface will be provided but customers may request periodic polling or other services
 * to send information to our local devices.
 * In all cases the receipt and validation of the data structure must be separated from the transformation
 * and local synchronization to ensure that long processes or failures are not spread to the customer service.
 *
 * @author Bing You
 * @copyright EK3 Technologies Inc.
 * @version 1.0.1
 */

require_once('LpiConfig.php');
require_once('LpiFunctions.php');

//Grab the first file provided - we don't care what the variable name is
$fileData = array_shift($_FILES);

if(!empty($fileData)) {
	if(mime_content_type($fileData['tmp_name']) == 'application/xml') {
		$xmlUploadFile = $fileData['tmp_name'];
		//Make sure the XML is well formed.
		$dom = new DOMDocument;
		if (!$dom->load($xmlUploadFile)) {
			$error = libxml_get_last_error();
			header("HTTP/1.0 400 Bad Request");
			die('Sorry, but the XML file "' . $fileData['name'] . '" is not well formed. The error occured on line ' . $error->line . ' and the exact message was "' . $error->message . '". Please correct the error and try again.');
		}

		$config = new LPIConfig();
		$isCustomSchemaValidated = false;
		foreach($config->schemaCustomers as $schemaCustomer) {
			if (@$dom->schemaValidate($schemaCustomer)) {
				$isCustomSchemaValidated = true;
				break;
			}
		}

		//Validate the XML document against its Schema.
		if ( !$isCustomSchemaValidated ) {
			//Take actions if invalid.
			if (@!$dom->schemaValidate($config->schemaStandardInput)) {

				if (@!$dom->schemaValidate($config->schemaFinalData)) {
					header("HTTP/1.0 400 Bad Request");
					die('Sorry. The XML "' . $fileData['name'] . '" is invalid. Please try to fix the errors in it yourself and try again. ');

				} else {
					$generated = $dom->getElementsByTagName('generated');
        	        $timestamp = $generated->item(0)->firstChild->data; 

					if (compareTimeStamp($timestamp, $config->xmlFinalData)) {
						move_uploaded_file($fileData['tmp_name'], $config->xmlFinalData);
						//$dom->save( $config->xmlFinalData );
					}
					echo "Broadcast menuboard.xml successfully.\n";
					exit;
				}
			}
		}

		move_uploaded_file($fileData['tmp_name'],"/tmp/customer.xml");
		echo "File ". $fileData['name'] ." uploaded successfully.\n";
		$port = 80;
		if(isset($_SERVER['SERVER_PORT']))
		    $port = $_SERVER['SERVER_PORT'];
		shell_exec(sprintf("php LpiBackground.php %d > /dev/null &", $port));
	} else {
		header("HTTP/1.0 400 Bad Request");
		echo "Please upload XML file.";
	}
} else {
	header("HTTP/1.0 400 Bad Request");
	echo "No file uploaded. ";
}