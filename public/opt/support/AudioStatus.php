<?php
/*
 * Copyright 2014 EK3 Technologies Inc.
 */
 
/*
 * Maintains an audio status file, which is a rotating record of
 * songs the audioplayer has scheduled/played
 */
require_once('/etc/ek3/audioconf.php');

if (empty($_REQUEST["song"])) {
    if (openlog( "AudioStatus.php" , 0 , LOG_LOCAL0 )) {
        syslog(LOG_INFO, "Required parameter song not set");
        closelog();
    }
    return;
}
if (empty($_REQUEST["date"])) {
    if (openlog( "AudioStatus.php" , 0 , LOG_LOCAL0 )) {
        syslog(LOG_INFO, "Required parameter date not set");
        closelog();
    }
    return;
}

$songs = file(STATUS_FILE);
if ($songs) {
    array_push($songs, $_REQUEST["date"] . " " . $_REQUEST["song"] . "\n");
} else {
    $songs = array( $_REQUEST["date"] . " " . $_REQUEST["song"] . "\n");
}
if (count($songs) > MAX_RECORDS) {
    $songs = array_slice($songs, MAX_RECORDS * -1);
}
file_put_contents(STATUS_FILE, $songs);
