<?php 
require_once('config.php');

if(isset($_REQUEST['dataname'])) {
    if(preg_match('/^([A-Za-z0-9_\-]+)$/', $_REQUEST['dataname'])) {
        $path = sprintf('/opt/data/%s.xml', $_REQUEST['dataname']);
        if(file_exists($path))
            $data = file_get_contents($path);
    }
}
if(isset($_REQUEST['format']) && $_REQUEST['format'] == 'json') {
        $xml = simplexml_load_string($data);
        $data = json_encode($xml);
}
echo $data;
