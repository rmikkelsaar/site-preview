<?php
require_once('config.php');
if(empty($_REQUEST['action']) || empty($_REQUEST['output']) || empty($_REQUEST['region'])) return 1;
if(!file_exists('/dev/shm/xwatcher')) {
    mkdir('/dev/shm/xwatcher');
    @chmod('/dev/shm/xwatcher', 0777);
}

$action = $_REQUEST['action'];
$output = preg_replace('/[^a-zA-Z0-9]/', '', $_REQUEST['output']);
$config_path = sprintf('/dev/shm/xwatcher/config_%d-%s', $_REQUEST['region'], $output);
$touch_path = sprintf('/dev/shm/xwatcher/touch_%d-%s', $_REQUEST['region'], $output);

if($action == 'reset') {
    exec(sprintf('rm -f -- /dev/shm/xwatcher/*-%s', $output));
} elseif($action == 'add') {
    if(empty($_REQUEST['region']) || empty($_REQUEST['length'])) return 1;
    file_put_contents($config_path, ceil($_REQUEST['length'] / 60) * 60);
    touch($touch_path);
    @chmod($config_path, 0666);
    @chmod($touch_path, 0666);
} elseif($action == 'rem') {
    if(empty($_REQUEST['region'])) return 1;
    exec(sprintf('rm -f -- /dev/shm/xwatcher/config_%d-%s', $_REQUEST['region'], $output));
} elseif($action == 'touch') {
    if(empty($_REQUEST['region'])) return 1;
    touch($touch_path);
    @chmod($touch_path, 0666);
} else {
    return 1;
}
return 0;
