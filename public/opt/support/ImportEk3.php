<?php

require_once('LpiConfig.php');
require_once('LpiFunctions.php');

$configFile='/etc/lpi_conf.xml';
if (file_exists($configFile)) {
	$config = new LPIConfig();
	
	$importfile = '/opt/data/queue/import.xml';
	if (file_exists($importfile)) {
		if (ImportToEK3Server($config->serverUrl, $importfile)) {
			unlink($importfile);
		}	
	}
}
