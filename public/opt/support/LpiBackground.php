<?php
require_once('LpiConfig.php');
require_once('LpiFunctions.php');

$config = new LPIConfig();
$output = shell_exec('java -jar /opt/data/lpi.jar /tmp/customer.xml');
if (!empty($output)) {
	die('Sorry. Transformation XML failed. Error:'. $output . '\n');
}

$importfile = '/opt/data/queue/import.xml';
copy($config->xmlFinalData, $importfile);
if (ImportToEK3Server($config->serverUrl, $config->xmlFinalData)){
	unlink($importfile);
}
$port = 80;
if(isset($argv[1]) && (int)$argv[1] > 0)
    $port = (int)$argv[1];
BroadcastToNeighbours($config->xmlFinalData, $port);