<?php
/*
 * Copyright 2014 EK3 Technologies Inc.
 */

/*
 * Logs to syslog
 */
 
if (empty($_REQUEST["msg"])) {
    if (openlog( "Syslog.php" , 0 , LOG_LOCAL0 )) {
        syslog(LOG_INFO, "Required parameter msg not set");
        closelog();
    }
    return;
}
if (empty($_REQUEST["ident"])) {
    if (openlog( "Syslog.php" , 0 , LOG_LOCAL0 )) {
        syslog(LOG_INFO, "Required parameter ident not set");
        closelog();
    }
    return;
}
if (openlog( $_REQUEST["ident"] , 0 , LOG_LOCAL0 )) {
    syslog(LOG_INFO, $_REQUEST["msg"]);
    closelog();
}
