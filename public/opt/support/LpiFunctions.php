<?php

/**
 * The location synchronization is triggered externally to distribute the current pricing information from
 * the data folder across all neighbour devices at the locations. The neighbour devices are determined from
 * the existing state kept on the device driven from the location configuration in inPulse Xpress.
 *
 * @author Bing You
 * @copyright EK3 Technologies Inc.
 * @version 1.0.1
 */

function BroadcastToNeighbours($xmlFinalData, $port) {
	$ch = curl_init();
	$data['file'] = '@'.$xmlFinalData;
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	$max_retries = 10;
	$retry_count = 0;

	$neighbours = getNeighboursAddress();
	$noResponsedNeighbours = array();

	while (!empty($neighbours) && $max_retries > $retry_count ) {

		$noResponsedNeighbours = array();

		foreach($neighbours as $nbAddress) {
		    curl_setopt($ch, CURLOPT_PORT, $port);
			curl_setopt($ch, CURLOPT_URL, 'http://'.$nbAddress.'/support/Upload.php');
			curl_exec($ch);

			// Check if any error occured
			if( curl_errno($ch) ) {
				array_push($noResponsedNeighbours, $nbAddress );
			}
		}

		$neighbours = $noResponsedNeighbours;
		++$retry_count;

		switch ( count($neighbours) ) {
			case 1:
				sleep(9);
				break;
			case 2:
				sleep(6);
				break;
			case 3:
				sleep(3);
		}
	}

	foreach($neighbours as $nbAddress) {
		error_log('Could not connect to host '.$nbAddress.' after 10 times attempts', 0);
	}

	curl_close($ch);
}

/**
 * The import is triggered from on a node resulting in the current product and price information to be
 * distributed to the central services using rsync. The service will continually retry until successful
 * delivery is made and will always attempt delivery with the latest data available from the device.
 * Once the data has been successfully delivered an asynchronous process will import the data into the
 * business model for the location that the information was received from.
 */

function ImportToEK3Server($serverUrl, $xmlFinalData) {
	$fp = fopen('/etc/msgsys.conf','r');
	while (($line = fgets($fp)) !== false) {
		if (strpos($line,'MSGSYS_USER=') !== false) {
			$User = str_replace("\n", "", substr($line, 12));
		}

		if (strpos($line,'MSGSYS_PWD=') !== false) {
			$Password = str_replace("\n", "", substr($line, 11));
		}
	}

	//Sample: $url = "https://test:test@172.19.2.200/pos_import/data";
	$url = str_replace("://", "://".$User.":".$Password."@", $serverUrl);

	$ch = curl_init();
	$data['serial_num'] = $User;
	$data['xml'] = '@'.$xmlFinalData;
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	//function CallCurl($ch, $max_retries);
	$result = CallCurl($ch, 1);
	curl_close($ch);

	if (! $result) {
	  error_log('Import to EK3 Service:['.$url.'] failed.', 0);
	}
	return $result;
}

function CallCurl($ch, $max_retries) {
	$result = false;
	$retry_count = 0;

	while(! $result && $retry_count < $max_retries)
	{
		curl_exec($ch);
		// Check if any error occured
		if( curl_errno($ch) || curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 ) {
			$retry_count ++;
		} else {
			$result = true;
		}

		if(! $result &&  $retry_count < $max_retries){			
			sleep(15);
		}
	}
	
	return $result;
}

function compareTimeStamp($incomeTimestamp, $xmlFinalData) {
	$localDom = new DOMDocument;
	if ($localDom->load( $xmlFinalData )) {
		$localGenerated = $localDom->getElementsByTagName('generated');
		$localTimestamp = $localGenerated->item(0)->firstChild->data;
		if ( $incomeTimestamp > $localTimestamp ) {
			return true;
		} else {
			return false;
		}
	}
	return true;
}

function OverWriteMenuboard($xmlMenuboardInput, $schemaFinalData, $xmlFinalData) {
	//Make sure the XML is well formed.
	$dom = new DOMDocument;
	if (!$dom->load($xmlMenuboardInput)) {
		$error = libxml_get_last_error();
		error_log('Sorry, but the menuboard.xml is not well formed. The error occured on line ' . $error->line . ' and the exact message was "' . $error->message . '". Please correct the error and try again.', 0);

	} else {

		//Validate the XML document against its Schema.
		if (!$dom->schemaValidate($schemaFinalData)) {
			error_log('Sorry. The menuboard.xml is invalid. Please try to fix the errors in it yourself and try again. ', 0);
		} else {
			$generated = $dom->getElementsByTagName('generated');
			$timestamp = $generated->item(0)->firstChild->data;
			if (compareTimeStamp($timestamp, $xmlFinalData)) {
				$dom->save($xmlFinalData);
			}
		}
	}
}

function getNeighboursAddress() {
	$arrayNeighboursAddress = array();

	if ($handle = opendir('/var/state/location_neighbours')) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				array_push($arrayNeighboursAddress, $entry);
			}
		}
		closedir($handle);
	}
	return $arrayNeighboursAddress;
}
