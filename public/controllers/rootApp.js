'use strict'

// Main rootApp for all projects, subsequent controllers defined elsewhere
var rootApp = angular.module('rootApp', ['ui.bootstrap', 'ui.router', 'ngCookies']);

/**
 * Route Configuration for sub partials
 */
rootApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
 	$urlRouterProvider.otherwise("/search");
	$stateProvider
	.state('search', {
		url: "/search",
		controller: 'searchCtrl',
		templateUrl: 'search.html'
	});
}])
.run(function ($rootScope, $window) { // Assign global scope variable
	$rootScope.endPoint = $window.location.origin;
});


/**
 * Socket factory
 * returns the socket object used for communication to the server
 */
rootApp.factory('socket', function() {
	var socket = io.connect();
	return socket;
});