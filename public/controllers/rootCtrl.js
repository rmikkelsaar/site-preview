/**
 * Copyright 2015 EK3 Technologies Inc.
 */
 
rootApp.controller('rootCtrl', ['$scope', '$log', 'task', 'socket', function($scope, $log, task, socket) {


	$scope.socket = socket;

	$scope.socket.on('message', function(msg) {
		$log.info('Socket::message ', msg);
	});

	
	/**
	 * searchLoaded is called from a child controller to tell the 
	 * main ctrl that we are all loaded and ready
	 */
	$scope.searchLoaded = function() { // Our main page has loaded
		// TODO Send cookie to backend to start user session
		$log.info('Search Loaded');
		socket.emit('fully connected');
	};

	/**
	 * initRoot
	 * once all subsequent data is loaded, assign a session ID to the backend
	 */
	$scope.initRoot = function() {
		$log.info('Root Loaded');
	};
}]);