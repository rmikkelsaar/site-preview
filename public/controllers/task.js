/**
 * Copyright 2015 EK3 Technologies Inc.
 */

/**
 * task Service
 * Responsible for all server communication using defered promises
 */
rootApp.service('task', function task($http, $q, $rootScope) {
	var task = this;
	/**
	 * @param {string} Tenant Name
	 * @param {int} Location ID
	 * @return {promise} the callback to this http request
	 */
	task.getLocation = function(tenant, id, sockID){
		var defer = $q.defer();
        var data = { 'tenant': tenant, 'location': id, 'sock_id': sockID };

        $http.get( $rootScope.endPoint + '/location', { params: data })
        .success(function(res) {
        	defer.resolve(res);
        })
        .error(function(err, status) {
        	defer.reject(err);
        });
		return defer.promise;
	};

	/**
	 * previewConnection
	 * @param {int} ConnectionID
	 * Sets up the backend start mini-webservers from ports 3001+ for specific iframes to point
	 */
	task.previewConnection = function(connectionID, sockID) {
		var defer = $q.defer();
		var data = { 'connection': connectionID, 'sock_id': sockID };

		$http.get( $rootScope.endPoint + '/preview', { params: data })
		.success(function(res) {
			defer.resolve(res);
		})
		.error(function(err, status) {
			defer.reject(err);
		});

		return defer.promise;
	};

	return task;
});