/**
 * Copyright 2015 EK3 Technologies Inc.
 */

rootApp.controller('searchCtrl', ['$scope', '$log', '$http', '$interval', '$timeout', '$window', '$q', 'task', function($scope, $log, $http, $interval, $timeout, $window, $q, task ) {
    $scope.searchQuery = "";
    $scope.locationDocument = "";
    $scope.locationSearchErr = "";
    $scope.locationObject = null;
    $scope.connectionIDs = [];
    $scope.currentConnection = null;
    $scope.screens = [];
    // TODO Pull tenants from available tenants on server
    $scope.tenants = [
        {'name': 'McDonalds', 'id': 'mcd'},
        {'name': 'McDonalds US', 'id': 'mcu'},
        {'name': 'RBC', 'id': 'rbc'},
        {'name': 'Tim Hortons', 'id': 'tdl'}
    ];
    $scope.tenant = $scope.tenants[0];

    /**
     * Tenant dropdown changed event
     */
    $scope.tenantChange = function() {
        $log.info('Tenant Changed:', $scope.tenant);
    };

    /**
     *  Clear the search query and all information
     */
    $scope.clearSearch = function() {
        $scope.searchQuery = "";
        $scope.locationDocument = "";
        $scope.locationObject = null;
        $scope.hasLocation = false;
        $scope.screens = [];
    };

    /**
     * Search for this location and retrieve relavent information
     */
    $scope.searchLocation = function() {
        if( $scope.searchQuery != "" ) {
            task.getLocation($scope.tenant.id, $scope.searchQuery, $scope.$parent.socket.id)
            .then(function(res) {
                if( res.statusCode === 200 ) {
                    $log.info('Found Location:', $scope.searchQuery);
                    $scope.screens = [];
                    $scope.hasLocation = true;
                    $scope.locationObject = angular.fromJson(res.obj);
                    $scope.connectionIDs = $scope.locationObject.connection_ids;
                    $scope.locationSearchErr = "";
                } else {
                    $log.warn('Error searching for location:', $scope.searchQuery, res);
                    var errorcode = typeof(res.error) !== 'undefined' ? res.error.code : '';
                    $scope.locationSearchErr = res.statusCode + ' ' + errorcode + ' ' + res.message;
                    $scope.clearSearch();
                    $scope.hasLocation = false;
                }            
            }, function(err) { // Error
                $log.error(err);
            });
        }        
    };

    /**
     * selectConnection
     * will fetch all other relavent information for this connection
     * and set up the backend for all iframes
     */
    $scope.selectConnection = function(conn) {
        task.previewConnection(conn, $scope.$parent.socket.id)
        .then(function(res) {
            if( res.length > 0 ) {
                var obj = null;
                $scope.screens = [];
                for( i in res ) {
                    $scope.screens.push(res[i]);
                }    
            } else {
                $log.error('Error occuded while selecing a connection', res.message);
                $scope.screens = [];
            }
        }, function(err) { // Error
            console.log(err);
        });
    };

    /**
     * Initailize Page Data onload
     */
    $scope.initPage = function() {
        $scope.$parent.searchLoaded();
    };
}]);