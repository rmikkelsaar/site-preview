/**
 * Copyright 2015 EK3 Technologies Inc.
 */

var cors = require('cors'),
	Downloader = require('./Downloader');
	winston = require('winston'),
	when = require('when')
	fs = require('fs');

var util = require('util');

/**
 * @param {string} Environment that this app will be attached to
 * Manager manages the downloading of all documents
 * getLocation() Must be called with a valid locationID before calling any other methods
 */
function Manager(environment) {
	this.environment = environment;
	this.base_url = 'http://' + this.environment;
	this.users = [];
	this.downloaded_assets = [];
	this.compressed_files = [];
};


/**
 * @param {string} Tenant ID
 * @param {int} Location ID
 * @param {int} Socket ID
 * @returns {promise} A deferred promise for asynchronous downloads
 * Fetches the location Document
 */
Manager.prototype.getLocation = function(tenant, locationID, sock) {
	var self = this,
		d = when.defer(),
		user = self.getUser(sock);
	winston.debug('Manager::getLocation():', tenant, locationID);
	user.tenant_short = tenant;

	var downloader = new Downloader(self.base_url + tenant +  '.tech.ek3.com:8001/documents/');

	// Realm Document
	downloader.getDocument( '0', 'realm' ).then(function(response) {
		var realmDoc = JSON.parse(response.obj);
		user.media_uri = realmDoc.media_origin_uri;
		user.document_uri = realmDoc.configuration_service_uri;
		user.document_uri = user.document_uri.replace('https', 'http');
		user.tenant_id = realmDoc.tenant_id;
	}).then(function() { // Connection Document
		return downloader.getDocument(locationID, 'location');
	}).then(function(response) {
		user.documents.location = response.obj;
		d.resolve(response);
	}).catch(function(error) {
		winston.warn('Manager::getLocation()', error);
		self.getUser(sock).documents.location = null;
		d.reject(error);
	})

	return d.promise;
};

/**
 * @param {int} Connection ID
 * @returns {promise} A deferred promise for asynchronous calls
 * TODO: Fetches all relavent documents for this specific connection
 */
Manager.prototype.getDocuments = function(connectionID, sock) {
	var self = this,
		d = when.defer(),
		user = self.getUser(sock);

	var downloader = new Downloader(user.document_uri);

	// Promise Chain used for dependant asynchronous fetch calls 
	downloader.getDocument(connectionID, 'connection').then(function(result) {
		var connectionObj = JSON.parse(result.obj);
		return getMz( downloader, connectionObj.mz_ids );
	}).then(function(marketing_zones) {
		winston.debug('Manager::getDocuments() downloaded ', marketing_zones.length, 'marking zones');
		user.documents.mzs = []; // Init / clear
		var marketingZoneArray = [];
		for( i in marketing_zones ) {
			marketingZoneArray.push( JSON.parse(marketing_zones[i].obj) );
		}
		marketingZoneArray.sort(compareName);
		user.documents.mzs = marketingZoneArray;
		return getPlayscripts( downloader, user.documents.mzs );
	}).then(function(playscripts) {
		winston.debug('Successfully downloaded ', playscripts.length, 'playscripts');
		if( !self.splitPlayscripts( playscripts, sock )) {
			d.reject('Manager::getDocuments() Error Splitting Playscripts');
		}
		if( !self.generateConfigFiles( sock )) {
			d.reject('Manager::getDocuments() Error Generating Configuration Files');
		}
		if( !self.scanMedia( sock )) {
			d.reject('Manager::getDocuments() Error Scanning Media');
		}
		d.resolve(playscripts);
	}).catch(function(error) {
		winston.error('Manager::getDocuments()', error);
		d.reject(error);
	});
	return d.promise;
};

function prototyping(data) {
	var d = when.defer();
	setTimeout(function() {
		d.reject('REJECTING A PROMISE' + data);
	}, 3000);

	return d.promise;
}

/**
 * splitPlayscripts
 * @param {array<object>} Contains all the playscript documents for each screen
 * @param {object} SockID is the UID for the user requesting the documents
 * @return {bool} Success of the playscript splitting
 */
Manager.prototype.splitPlayscripts = function(data, sockID) {
	var self = this,
		user = self.getUser(sockID),
		playscript = null;

	if( data.length < 1 ) { winston.error('Manager::splitPlayscripts() Data is empty'); return false; }
	for( i in data ) {
		playscript = JSON.parse(data[i].obj);
		for( x in user.documents.mzs ) {
			if( user.documents.mzs[x].id == playscript.resources[0] ) { // Match playscript.resources with mz.id
				user.documents.mzs[x].media = JSON.stringify(playscript.media);
				user.documents.mzs[x].index = playscript['index-0']
				user.documents.mzs[x].playlist = JSON.stringify(playscript.playlist);
				user.documents.mzs[x].schedule = JSON.stringify(playscript.schedule);
				user.documents.mzs[x].region = JSON.stringify(playscript.region);
				user.documents.mzs[x].datafeed = JSON.stringify(playscript.datafeed);
				user.documents.mzs[x].time_to_play = JSON.stringify(playscript.time_to_play);
				continue;
			}
		}
		// Assign screen path for iframe
		data[i].screenLocation = "http://" + user.referer + "/screen" + (parseInt(i) + 1);
	}
	return true;
};

/**
 * @param {object} Media Object
 * scanMedia will search through the media object, find all occurrences of any zip file
 * download, extract and map its asset id it to sub directory /public/compressed to be statically served
 */
Manager.prototype.scanMedia = function(sockID) {
	var self = this,
		zipFiles = [],
		fileUID = "";
	try {
		var user = self.getUser(sockID);

		if( !user ) { winston.error('Manager::scanMedia() No User with sockID', sockID); return false; }
		var mzs = user.documents.mzs;
		for( i in mzs ) {
			var media = JSON.parse(mzs[i].media);
			for( x in media ) {
				if( media[x].meta_values.name.toLowerCase().indexOf('zip') >= 0 ) { // Found a zip file
					user.is_orchestration = true;
					fileUID = media[x].media_asset_id;
					// media[x].media_asset_id = media[x].media_asset_id + '_' + (parseInt(i) + 1);
					if( zipFiles.indexOf(fileUID) < 0 ) { // Do not queue redundant files
						zipFiles.push(fileUID);
					}
				}
			}
			// Reserialize media.json for the link reference
			// user.documents.mzs[i].media = JSON.stringify(media);
		}

		winston.debug('Manager::scanMedia() Zip Files Discovered', util.inspect(zipFiles));
		// Assign media
		if( zipFiles.length > 0 ) {
			for( i in zipFiles ) {
				if( self.compressed_files.indexOf(zipFiles[i]) < 0 ) // Do not store redundant files
				{
					self.compressed_files.push(zipFiles[i]);
				}
			}
			winston.debug('Manger::scanMedia() Total Compressed Files Directory', util.inspect(self.compressed_files) );
		}
	} catch (e) {
		winston.error('Manager::scanMedia() Error', e);
		return false;
	}
	winston.debug('Manager::scanMedia() Successfull');
	return true;
};

/**
 * @return {promise} A deffered promise indicating the completion of filesystem manipulation,
 * 					 downloading and extracting
 * getCompressedFiles looks at the currently stored list of zip files that have been extracted
 * and compares that list to what needs to be downloaded and extracted
 * 
 */
Manager.prototype.getCompressedFiles = function(sockID) {
	var self = this,
		downloadList = [],
		d = when.defer();
	var user = self.getUser(sockID);
	var downloader = new Downloader(user.media_uri);
	for( i in self.compressed_files ) {
		if( self.downloaded_assets.indexOf(self.compressed_files[i].toString()) < 0 ) { // Check if asset it downloaded already
			downloadList.push(downloader.getMedia( self.compressed_files[i], user.documents.mzs.length) );
		}
	}
	if( downloadList.length == 0 ) { d.resolve(); } // Nothing to download

	// Wait for all downlaods to occur
	when.all(downloadList).then(function(assets) {
		winston.debug('Manager()::getCompressedFiles() All media assets synced.');
		d.resolve();
	}).catch(function(error) {
		d.reject(error);
	});

	return d.promise;
};

/**
 * @param {object} Left comparator
 * @param {object} Right comparator
 * @return {int} Comparitor result
 * Sorts the objects based on property
 */
function compareName(a, b) {
	if( a.name < b.name ) {
		return -1;
	} else if ( a.name > b.name ) {
		return 1;
	}
	return 0;
}

/**
 * getMz
 * @param {object} Downloader class already instantiated with the proper url
 * @param {array} Array of id's for mz to download
 * @return {Array<promises>} to an All for completion
 */
function getMz( downloader, mzs ) {
	var deferreds = [];
	winston.debug("Downloading ", mzs.length, " Marketing zone documents: ");
	for( i in mzs ) {
		deferreds.push(downloader.getDocument(mzs[i], 'mz'));
	}
	return when.all(deferreds); // Returns only when all mz's are downloaded
}

/**
 * getPlayscripts
 * @param {object} Downloader class already instantiated with the proper url
 * @param {array} Array of id's for playscripts to download
 * @return {Array<promises>} to an All for completion
 */
function getPlayscripts( downloader, playscripts ) {
	var deferreds = [];
	winston.debug("Downloading ", playscripts.length, " Playscript documents: ");
	for( i in playscripts ) {
		deferreds.push(downloader.getDocument(playscripts[i].playscript_id, 'playscript'));
	}
	return when.all(deferreds); // Returns only when all mz's are downloaded
}


/**
 * @param {int} SocketID
 * Generates the configuration documents for the specific dataset
 */
Manager.prototype.generateConfigFiles = function(sockID) {
	var self = this,
		user = this.getUser(sockID);
	if( !user ) { 
		winston.error('Manager::generateConfigFiles() No User with sockID', sockID); 
		return false;
	}
	var config = null,
		config_VGA = null;

	for( i in user.documents.mzs ) {
		config_VGA = new Object();
		config_VGA.is_master = (i == 0 ? 'yes' : 'no');
		config_VGA.screen_name = 'screen #' + (parseInt(i) + 1); // This needs to reflect the posibility of region_name being present in mz

		config = new Object();
		config.base_services = '/support/';
		// config.media_uri = 'http:' + user.document_uri.split(':')[1] + '/cdn/'; // Strip off port and everything to the right of it		
		config.media_uri = '/media/screen' + (parseInt(i) + 1) + '/' + sockID + '/'; // Intercept all media requests and scan for zip files to redict already extracted contents
		config.message_url = 'http://' + user.referer.split(':')[0] + ':15530';
		config.output_name = 'VGA';
		config.support_modules = [ 'logging', 'watchdog' ];

		user.documents.mzs[i].config = JSON.stringify(config);
		user.documents.mzs[i].config_vga = JSON.stringify(config_VGA);
	}	

	if( user.documents.mzs.length > 0 ) {
		winston.debug('Configuration Files Generated Successfully');
		return true;
	} else {
		winston.error('Manager::generateConfigFiles() No Configuration Files were generated');
		return false;
	}
};



/**
 * getUser takes a socket object or socket id and returns the user object. 
 * @param {string || object} Socket object OR socketID to allow this to be more extensible
 */
Manager.prototype.getUser = function(sock) {
	var self = this;
	for( i in self.users ) {
		if( typeof(sock) === "object" && self.users[i].sockID == sock.id ) {
			return self.users[i];
		} else if ( self.users[i].sockID == sock ) {
			return self.users[i];
		}
	}
	winston.warn('Returning NULL user');
	return null;
};

/**
 * removes the user object from our users array
 * @param {string} UID of User via SocketID
 */
Manager.prototype.removeUser = function(sock) {
	var self = this;
	self.users.splice(self.getUser(sock), 1);
}


/**
 * getUser returns the user object for a specific socket ID
 * @param {string} UID of User via SocketID
 */
Manager.prototype.printUsers = function() {
	var self = this;
	for( i in self.users ) {
		self.users[i].toString();
	}
}


module.exports = Manager;