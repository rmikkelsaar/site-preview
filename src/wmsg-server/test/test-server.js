/**
	WebMessage server test suite.
	@package SynchronizedPlayback
	@subpackage server
	@version 1.0.0
	@author Chris Eineke
	@copyright 2011 EK3 Technologies Inc.
 */
var WebMessageServer = require("../src/server");
var WebSocket = require("websocket-client").WebSocket;
var sys = require("sys");
var log = require("../src/log");
var instance = null;
var port = 15525;
var url = "ws://localhost:"+port;
var surl = "wss://localhost:"+port;

//log.verbose = true;
//log.debug = true;

exports["initialize component"] = function (test) {
  var enableDebugMessageTypes = true;
	instance = new WebMessageServer(enableDebugMessageTypes);
	test.notEqual(instance, null);
  instance.spawnWebSocketJsonFrontEnd(port);
	test.done();
};

exports["test web socket connection"] = function (test) {
	var client = new WebSocket(url);
	client.on("open", function () {
      client.close(1);
    });
	client.on("close", function () {
      test.done();
    });
};

exports["test secure web socket connection not supported"] = function (test) {
	test.expect(1);
	test.throws(function () {
      new WebSocket(surl)
    },
    ReferenceError);
	test.done();
};

exports["send null"] = function (test) {
	var client = new WebSocket(url);
	client.on("open", function () {
      client.send(JSON.stringify(null));
		});
	client.on("close", function () {
      test.done();
		});
};

exports["send empty object"] = function (test) {
	var client = new WebSocket(url);
	client.on("open", function () {
      client.send(JSON.stringify({ }));
		});
	client.on("close", function () {
      test.done();
		});
};

exports["send invalid content"] = function (test) {
	var client = new WebSocket(url);
	client.on("open", function () {
      client.send(JSON.stringify("abcd"));
		});
	client.on("close", function () {
      test.done();
		});
};

exports["send echo message"] = function (test) {
	var client = new WebSocket(url);
	client.on("open", function () {
      client.send(JSON.stringify({
          "type": 0,
          "data": "test"
        }));
		});
	client.on("message", function (msg) {
      msg = JSON.parse(msg);
      test.equal(msg.type, 0);
      test.notEqual(msg.source, null);
      test.equal(msg.data, "test");
      client.close(1);
		});
	client.on("close", function (msg) {
      test.done();
		});
};

exports["send broadcast message"] = function (test) {
	var client1 = new WebSocket(url);
	client1.on("open", function () {
      client1.send(JSON.stringify({
          "type": 1,
          "data": "test"
        }));
			});
	client1.on("message", function (msg) {
      test.fail();
		});
	var client2 = new WebSocket(url);
	client2.on("message", function (msg) {
      msg = JSON.parse(msg);
      test.equal(msg.type, 1);
      test.notEqual(msg.source, null);
      test.equal(msg.data, "test");
      client1.close(1);
      client2.close(1);
		});
	client2.on("close", function () {
      test.done();
		});
};

exports["send echo-broadcast message"] = function (test) {
	var i=0;
	var client1 = new WebSocket(url);
	client1.on("open", function () {
      client1.send(JSON.stringify({ type: 2, data: "test" }));
		});
	client1.on("message", function (msg) {
      msg = JSON.parse(msg);
      test.equal(msg.type, 2);
      test.notEqual(msg.source, null);
      test.equal(msg.data, "test");
      client1.close(1);
      i++;
      if (i==2) {
        test.done();
      }
    });
	var client2 = new WebSocket(url);
  client2.on("message", function (msg) {
      msg = JSON.parse(msg);
      test.equal(msg.type, 2);
      test.notEqual(msg.source, null);
      test.equal(msg.data, "test");
      client2.close(1);
      i++;
      if (i==2) {
        test.done();
      }
    });
};

exports["send relay message to one client"] = function (test) {
	var client1 = new WebSocket(url);
	client1.on("open", function () {
      client1.send(JSON.stringify({
          "type": 1
        }));
			});
  client1.on("message", function (msg) {
      msg = JSON.parse(msg);
      test.equal(msg.type, 3);
      test.notEqual(msg.source, null);
      test.done();
      client1.close(1);
    });
	var client2 = new WebSocket(url);
  client2.on("message", function (msg) {
      msg = JSON.parse(msg);
      test.equal(msg.type, 1);
      test.notEqual(msg.source, null);
      var next_msg = JSON.stringify({
        "type": 3,
        "target": msg.source
      });
      client2.send(next_msg);
      client2.close();
    });
};

exports["send relay message to two clients"] = function (test) {
	var ids = [];
	var i = 0;
	var client1 = new WebSocket(url);
	client1.on("open", function () {
				client1.send(JSON.stringify({
            "type": 0
          }));
      });
  client1.on("message", function (msg) {
      msg = JSON.parse(msg);
      if (msg.type == 0) {
        ids.push(msg.source);
      }
      else {
        i++;
        client1.close(1);
        if (i == 2) {
          test.done();
        }
      }
    });
	var client2 = new WebSocket(url);
	client2.on("open", function () {
				client2.send(JSON.stringify({
            "type": 0
          }));
			});
  client2.on("message", function (msg) {
      msg = JSON.parse(msg);
      if (msg.type == 0) {
        ids.push(msg.source);
      }
      else {
        i++;
        client2.close(1);
        if (i == 2) {
          test.done();
        }
      }
    });
	var client3 = new WebSocket(url);
  client3.on("open", function () {
      var intervalId = setInterval(function () {
        if (ids.length == 2) {
          clearInterval(intervalId);
          client3.send(JSON.stringify({
              "type": 3,
              "target": ids
            }));
          client3.close();
        }
      },
      100);
    });
};
