/**
  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var wmsg = require("./wmsg");
var wmsg_frontends = require("./frontends");
var log = require("./log");
var util = require("util");
var WriteStream = require("fs").WriteStream;
var EventEmitter = require("events").EventEmitter;

/**
  Constructs a new instance of WebMessageServer.
  @param supportDebugTypes Whether or not to support clear-text message types.
 */
function WebMessageServer(supportDebugTypes) {

  this.constructor.super_.call(this);
  var self = this;

  /** The message hub. */
  var dispatcher = new wmsg.Dispatcher();
 
  /** The currently running front-ends. */
  var frontends = {};

  /** A message probe. */
  var probe = null;

  /**
    Sets up the message hub with the message types that the system can support.
   */
  function initializeDispatcher() {
    dispatcher.addType(wmsg.MessageTypes.Echo);
    dispatcher.addType(wmsg.MessageTypes.Broadcast);
    dispatcher.addType(wmsg.MessageTypes.EchoBroadcast);
    dispatcher.addType(wmsg.MessageTypes.Relay);
    if (supportDebugTypes) {
      dispatcher.addType(wmsg.MessageTypes.EchoDebug);
      dispatcher.addType(wmsg.MessageTypes.BroadcastDebug);
      dispatcher.addType(wmsg.MessageTypes.EchoBroadcastDebug);
      dispatcher.addType(wmsg.MessageTypes.RelayDebug);
    }
    log.print_debug("Initialized dispatcher.");
  };

  function validatePort(port) {
    return port >= 0 && port <= 65535;
  };

  function assertNotRunning(port) {
    if (!validatePort(port)) {
      throw new Error("port must be a number in the range 0-65535.");
    }
    if (frontends[port] !== undefined) {
      throw new Error("Another front-end is already running on port "+port+".");
    }
  };

  function assertRunning(port) {
    if (!validatePort(port)) {
      throw new Error("port must be a number in the range 0-65535.");
    }
    if (frontends[port] === undefined) {
      throw new Error("No front-end running on port "+port+".");
    }
  };

  function spawnFrontEnd(type, frontend, port) {
    assertNotRunning(port);
    frontend.start(port);
    frontends[port] = frontend;
    self.emit("spawnedFrontEnd", type, frontend, port);
  };

  function killFrontEnd(type, port) {
    assertRunning(port);
    var frontend = frontends[port];
    delete frontends[port];
    frontend.stop(port);
    self.emit("killedFrontEnd", type, frontend, port);
  };

  function handleSpawnFrontEnd(type, port) {
    switch (type) {
      case "rest":
        self.spawnRestInterface(port);
      break;
      case "websocket-json":
        self.spawnWebSocketJsonFrontEnd(port);
      break;
      case "socketio":
        self.spawnSocketIoFrontEnd(port);
      break;
      default:
      break;
    }
  };

  function handleKillFrontEnd(type, port) {
    switch (type) {
      case "rest":
        self.killRestInterface(port);
      break;
      case "websocket-json":
        self.killWebSocketJsonFrontEnd(port);
      break;
      case "socketio":
        self.killSocketIoFrontEnd(port);
      break;
      default:
      break;
    }
  };

  /**
    Spawns a new WebSocket front-end with JSON encoding on the specified port.
    @param port The specified port.
   */
  this.spawnWebSocketJsonFrontEnd = function (port) {
    var frontend = new wmsg_frontends.WebSocketFrontEnd(new wmsg.JsonMessageCodec());
    dispatcher.on(wmsg.MessageTypes.Echo.event, frontend.handleEchoMessage);
    dispatcher.on(wmsg.MessageTypes.Broadcast.event, frontend.handleBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.EchoBroadcast.event, frontend.handleEchoBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.Relay.event, frontend.handleRelayMessage);
    dispatcher.on("error", frontend.handleDispatcherError);
    frontend.on("message", dispatcher.dispatchMessage);
    spawnFrontEnd("websocket-json", frontend, port);
    log.print_verbose("Spawned WebSocket front-end with JSON message encoding on port "+port+".");
  };

  /**
    Destroys a WebSocket front-end with JSON encoding on the specified port.
    @param port The specified port.
   */
  this.killWebSocketJsonFrontEnd = function (port) {
    killFrontEnd(port);
    log.print_verbose("Killed WebSocket front-end with JSON message encoding on port "+port+".");
  };

  this.attachStdoutProbe = function () {
    if (probe !== null) {
      log.print_debug("Cannot add probe: There is a probe already attached.");
      return false;
    }
    probe = new wmsg_frontends.Probe();
    dispatcher.on(wmsg.MessageTypes.Echo.event, probe.handleEchoMessage);
    dispatcher.on(wmsg.MessageTypes.Broadcast.event, probe.handleBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.EchoBroadcast.event, probe.handleEchoBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.Relay.event, probe.handleRelayMessage);
    dispatcher.on("error", probe.handleDispatcherError);
    log.print_verbose("Spawned stdout message probe.");
  };

  this.detachStdoutProbe = function () {
    probe = null;
    log.print_verbose("Killed stdout message probe.");
  };

  this.spawnSocketIoFrontEnd = function (port) {
    var frontend = new wmsg_frontends.SocketIoFrontEnd(new wmsg.JsonMessageCodec());
    dispatcher.on(wmsg.MessageTypes.Echo.event, frontend.handleEchoMessage);
    dispatcher.on(wmsg.MessageTypes.Broadcast.event, frontend.handleBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.EchoBroadcast.event, frontend.handleEchoBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.Relay.event, frontend.handleRelayMessage);
    dispatcher.on("error", frontend.handleDispatcherError);
    frontend.on("message", dispatcher.dispatchMessage);
    spawnFrontEnd("socketio", frontend, port);
    log.print_verbose("Spawned SocketIO front-end with JSON message encoding on port "+port+".");
  };

  this.killSocketIoFrontEnd = function (port) {
    killFrontEnd(port);
    log.print_verbose("Killed SocketIO front-end with JSON message encoding on port "+port+".");
  };

  this.spawnRestInterface = function (port) {
    var frontend = new wmsg_frontends.RestInterface();
    dispatcher.on(wmsg.MessageTypes.Echo.event, frontend.handleEchoMessage);
    dispatcher.on(wmsg.MessageTypes.Broadcast.event, frontend.handleBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.EchoBroadcast.event, frontend.handleEchoBroadcastMessage);
    dispatcher.on(wmsg.MessageTypes.Relay.event, frontend.handleRelayMessage);
    self.on("spawnedFrontEnd", frontend.handleFrontEndSpawned);
    self.on("killedFrontEnd", frontend.handleFrontEndKilled);
    frontend.on("spawnFrontEnd", handleSpawnFrontEnd);
    frontend.on("killFrontEnd", handleKillFrontEnd);
    spawnFrontEnd("rest", frontend, port);
    log.print_verbose("Spawned REST front-end on port "+port+".");
  };

  this.killRestInterface = function (port) {
    killFrontEnd(port);
    log.print_verbose("Killed REST front-end on port "+port+".");
  };

  initializeDispatcher();
};
util.inherits(WebMessageServer, EventEmitter);

module.exports = WebMessageServer;
