/**
	Logging utilties.
	@package SynchronizedPlayback
	@subpackage server.util
	@version 1.0.0
	@author Chris Eineke
	@copyright 2011 EK3 Technologies Inc.
 */
var util = require("util");

function log() { };
log.verbose = false;
log.debug = false;

/**
	Prints the specified verbose string to stdout.
	@param str The specified string.
	@return void
 */
log.print_verbose = function (str) {
	if (log.verbose) {
		util.log(str);
	}
};

/**
	Prints the specified debufg string to stdout.
	@param str The specified string.
	@return void
 */
log.print_debug = function (str) {
	if (log.debug) {
		util.log(str);
	}
};

module.exports = log;
