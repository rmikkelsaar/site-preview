/**
  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */

/**
  Encodes/decodes messages using JSON.
 */
function JsonMessageCodec() {

  this.encodeMessage = function (msg) {
    return JSON.stringify(msg);
  };

  this.decodeMessage = function (data) {
    var msg = null;
    try {
      msg = JSON.parse(data);
    }
    catch (e) {
    }
    finally {
      return msg;
    }
  };

};

module.exports = JsonMessageCodec;
