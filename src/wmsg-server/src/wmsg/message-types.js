/**
	@package SynchronizedPlayback
	@subpackage server
	@version 1.0.0
	@author Chris Eineke
	@copyright 2011 EK3 Technologies Inc.
 */

var MessageType = require("./message-type");

module.exports = {
  "Echo": new MessageType(0, "echoMessage"),
  "EchoDebug": new MessageType("echo", "echoMessage"),
  "Broadcast": new MessageType(1, "broadcastMessage"),
  "BroadcastDebug": new MessageType("broadcast", "broadcastMessage"),
  "EchoBroadcast": new MessageType(2, "echobroadcastMessage"),
  "EchoBroadcastDebug": new MessageType("echobroadcast", "echobroadcastMessage"),
  "Relay": new MessageType(3, "relayMessage"),
  "RelayDebug": new MessageType("relay", "relayMessage")
};
