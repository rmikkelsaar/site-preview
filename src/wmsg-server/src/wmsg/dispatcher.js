/**
  The Dispatcher dispatches generic MessageS by type so that individual
  controllers can handle them.

  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var EventEmitter = require("events").EventEmitter;
var Message = require("./message");
var MessageType = require("./message-type");
var util = require("util");

/**
  Constructs a new instance of Dispatcher.
  @emit mappedType A type mapping was added.
  @emit unmappedType A type mapping was removed.
  @emit unhandledMessageType A message with an unknown type was encountered.
  @emit error An error was encountered.
 */
function Dispatcher() {

  this.constructor.super_.call(this);
  var self = this;

  /** A 1-to-1 mapping of message types to events. */
  var typeMap = {};

  /**
    Adds a type->event mapping.
    @param type The message type to remove.
    @return Whether or not the mapping was created.
   */
  this.addType = function (type) {
    if (!(type instanceof MessageType)) {
      self.emit("error", "type must be an instance of MessageType.");
      return false;
    }
    if (typeMap[type.key] !== undefined) {
      self.emit("error", "'"+type.key+"' is already dispatched as '"+typeMap[type.key]+"'.");
      return false;
    }
    typeMap[type.key] = type.event;
    self.emit("mappedType", type);
    return true;
  };

  /**
    Removes a type->event mapping.
    @param type The message type to remove.
    @return Whether or not the mapping was destroyed.
   */
  this.removeType = function (type) {
    if (!(type instanceof MessageType)) {
      self.emit("error", "type must be an instance of MessageType.");
      return false;
    }
    if (typeMap[type.key] !== undefined) {
      self.emit("error", "'"+type.key+"' is not mapped to any event.");
      return false;
    }
    delete typeMap[type.key];
    self.emit("unmappedType", type);
    return true;
  };

  /**
    Dispatches a message.
    @param message The message.
    @return Whether or not the message was successfully dispatched.
   */
  this.dispatchMessage = function (message) {
    if (!(message instanceof Message)) {
      self.emit("error", "message must be an instance of type Message.");
      return false;
    }
    if (message.type === null) {
      self.emit("error", "message.type must be non-null.");
      return false;
    }
    var event = typeMap[message.type];
    if (event === undefined) {
      self.emit("unhandledMessageType", message);
      return false;
    }
    self.emit(event, message);
    return true;
  };
};
util.inherits(Dispatcher, EventEmitter);

module.exports = Dispatcher;
