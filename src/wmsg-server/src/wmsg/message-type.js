/**
  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */

/**
  A mapping from message type to event to be fired.
  @param key The message type.
  @param event The event name.
 */
function MessageType(key, event) {
  if (key == null) {
    throw new TypeError('key must be non-null.');
  }
  if (event == null) {
    throw new TypeError('event must be non-null.');
  }

  this.key = key;
  this.event = event;
};

module.exports = MessageType;
