/**
  The Probe logs messages to stdout.

  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var log = require("../log");
var sys = require("sys");

/**
  Constructs a new instance of Probe.
 */
function Probe() {

  this.handleEchoMessage = function (msg) {
    log.print_debug(sys.inspect(msg));
  };

  this.handleBroadcastMessage = function (msg) {
    log.print_debug(sys.inspect(msg));
  };

  this.handleEchoBroadcastMessage = function (msg) {
    log.print_debug(sys.inspect(msg));
  };

  this.handleRelayMessage = function (msg) {
    log.print_debug(sys.inspect(msg));
  };

  this.handleDispatcherError = function (error) {
    log.print_debug(sys.inspect(error));
  };

};

module.exports = Probe;
