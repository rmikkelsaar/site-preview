/**
	@package SynchronizedPlayback
	@subpackage server
	@version 1.0.0
	@author Chris Eineke
	@copyright 2011 EK3 Technologies Inc.
 */

/**
	Constructs a new instance of Message.
	@param type The message type.
	@param source The source peer of the message.
	@param target The target peer(s) of the message.
  @oaram data The payload of the message.
 */
function Message(type, source, target, data) {
  this.type = type;
  this.source = source;
  this.target = target;
  this.data = data;
};

/**
  Constructs a new instance of Message from the specified object.
	@param that The specified object.
 */
Message.copy = function (that) {
  return new Message(
      that.type !== undefined ? that.type : null,
      that.source !== undefined ? that.source : null,
      that.target !== undefined ? that.target : null,
      that.data !== undefined ? that.data : null);
};

module.exports = Message;
