/**
  Node.JS "entry point" for the WebMessage server.

  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var argv = require("optimist")
  .usage("Usage: $0 [-v] [-g] [--probe] [--rest=<port>] [--websocket-json=<port>] [--socketio=<port>]")
  .boolean("v")
  .boolean("g")
  .string("probe")
  .string("rest")
  .string("websocket-json")
  .string("socketio")
  .argv;
var WebMessageServer = require("./server");
var log = require("./log");

try {
  log.verbose = argv.v;
  log.debug = argv.g;
  log.print_verbose("WebMessage Server v1.0.0");
  log.print_verbose("Copyright 2011 EK3 Technologies Inc.");
  log.print_verbose("Verbose messages enabled.");
  log.print_debug("Debug messages enabled.");
  var server = new WebMessageServer(argv.g);
 
  if (Boolean(argv.probe)) {
    server.attachStdoutProbe();
  }
  if (Boolean(argv["rest"])) {
    server.spawnRestInterface(Number(argv["rest"]));
  }
  if (Boolean(argv["websocket-json"])) {
    server.spawnWebSocketJsonFrontEnd(Number(argv["websocket-json"]));
  }
  if (Boolean(argv["socketio"])) {
    server.spawnSocketIoFrontEnd(Number(argv["socketio"]));
  }
}
catch (e) {
  console.log("Error: "+e.message);
}
