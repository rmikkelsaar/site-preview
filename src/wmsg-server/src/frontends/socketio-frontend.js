/**
  SocketIO protocol front-end.
  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var events = require("events");
var io = require('socket.io');
var log = require("../log");
var sys = require("sys");
var util = require("util");
var Message = require("../wmsg").Message;

/**
  Constructs a new instance of SocketIoFrontEnd.
  @emit message The front-end has a new message for processing.
  @param codec What message codec to use.
 */
function SocketIoFrontEnd(codec) {

  this.constructor.super_.call(this);
  var self = this;

  var sio = null;
  var clients = {};
  var curConnection = null;

  /**
    Event handler when a client has encountered an error.
    @param error The error.
   */
  function handleConnectionError(connection, error) {
    connection.close();
    log.print_debug("Socket.IO server: Connection to client #"+connection.id+" closed due to error: "+error);
  };

  /**
    Event handler when a SocketIO client has sent data.
    @param connection The connection.
    @param frame The frame.
   */
  function handleConnectionMessage(connection, frame) {
    try {
      curConnection = connection;
      var message = codec.decodeMessage(frame);
      if (message == null) {
        throw new Error("Not a message.");
      }
      message.source = connection.id;
      message = Message.copy(message);
      self.emit("message", message);
    }
    catch (e) {
      connection.disconnect();
      log.print_debug("Socket.IO server: Connection to client #"+connection.id+" closed due to unparseable message.");
      log.print_debug("Socket.IO server: Contents of message: "+sys.inspect(message));
      log.print_debug(e.toString());
    }
    finally {
      curConnection = null;
    }
  };

  /**
    Event handler when a new SocketIO client has connected.
    @param connection The new connection.
   */
  function handleSocketConnection(connection) {
    clients[connection.id] = connection;
    log.print_debug("Socket.IO server: Client connected.");
    connection.addListener("message", function (message) {
        handleConnectionMessage(connection, message);
    });
    log.print_debug("Socket.IO server: Attached message listener.");
    connection.addListener("error", function (error) {
        handleConnectionError(connection, error);
    });
    connection.addListener('disconnect', handleSocketDisconnect);
  };

  /**
    Event handler when a SocketIO client has disconnected.
    @param connection The connection.
   */
  function handleSocketDisconnect(connection) {
    if (connection !== undefined && connection.id !== undefined) {
      delete clients[connection.id];
      log.print_debug("Socket.IO server: Client #"+connection.id+" disconnected.");
    }
  };

  /**
    Echoes the message back to the sending client.
   */
  this.handleEchoMessage = function (message) {
    var data = codec.encodeMessage(message);
    clients[message.source].send(data);
  };

  /**
    Broadcasts the message to every connection, excluding the sender.
   */
  this.handleBroadcastMessage = function (message) {
    var data = codec.encodeMessage(message);
    for (var i in clients) {
      var client = clients[i];
      if (client.id !== message.source) {
        client.send(data);
      }
    }
  };

  /**
    Broadcasts the message to every connection, including the sender.
   */
  this.handleEchoBroadcastMessage = function (message) {
    var data = codec.encodeMessage(message);
    for (var i in clients) {
      var client = clients[i];
      client.send(data);
    }
  };

  /**
    Relays the message from one connection to another.
   */
  this.handleRelayMessage = function (message) {
    var targets = message.target;
    if (typeof targets == "string") {
      targets = [targets];
    }
    var data = codec.encodeMessage(message);
    targets.forEach(function (target) {
        for (var i in clients) {
          var client = clients[i];
          if (client.id === target) {
            client.send(data);
          }
        }
      });
  };

  /**
    Event handler when the dispatcher has encountered an error.
   */
  this.handleDispatcherError = function (error) {
    /*
       curConnection is only set if we are the front-end that invoked
       dispatchMessage. If not, then another front-end is the source of the
       error. And we ain't not handlin' that!
     */
    if (curConnection == null) {
      return;
    }
    sio.find(curConnection.id, function (connection) {
        connection.close();
        log.print_debug("Socket.IO server: Disconnected client #"+connection.id+" due to invalid message.");
        log.print_debug("\tContents: "+sys.inspect(message));
    });
  };

  /**
    Starts the front-end on the specified port.
    @param port The port to listen on.
   */
  this.start = function (port) {
    sio = io.listen(port);
    sio.set("log level", 1);
    sio.sockets.addListener('connection', handleSocketConnection);
    log.print_debug("Now accepting SocketIO connections on port "+port+".");
  };

  /**
    Stop the front-end.
   */
  this.stop = function (port) {
    sio.sockets.removeListener('connection', handleSocketDisconnect);
    sio.server.close();
    log.print_debug("No longer accepting SocketIO connections on port "+port+".");
  };
};
util.inherits(SocketIoFrontEnd, events.EventEmitter);

module.exports = SocketIoFrontEnd;
