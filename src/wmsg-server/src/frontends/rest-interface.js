/**
  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var express = require("express");
var sys = require("sys");
var url = require("url");
var util = require("util");
var log = require("../log");
var EventEmitter = require("events").EventEmitter;

/**
  Constructs a new instance of WebMessageServer.
 */
function RestInterface() {

  this.constructor.super_.call(this);
  var self = this;
  var app = express.createServer();
  app.use(express.bodyParser());
  var stats = {
    "echos": 0,
    "broadcasts": 0,
    "echobroadcasts": 0,
    "relays": 0,
    "total": 0,
    "memoryUsage": process.memoryUsage()
  };
  var frontends = {};
  var resources = {
    "get" : {
      "/": function (req, res) {
        var base = makeBaseUrl(req);
        res.send({
            "version": base + "/version",
            "frontends": base + "/frontends",
            "stats": base + "/stats"
          });
      },
      "/version": function (req, res) {
        res.send({
            "major": 1,
            "minor": 0,
            "subminor": 0
          });
      },
      "/frontends": function (req, res) {
        var base = makeBaseUrl(req);
        var data = {};
        for (var port in frontends) {
          data[port] = base + "/frontends/" + port;
        }
        res.send(data);
      },
      "/frontends/:port": function (req, res) {
        res.send(frontends[req.params.port].type);
      },
      "/stats": function (req, res) {
        stats.memoryUsage = process.memoryUsage();
        res.send(stats);
      },
    },
    "post": {
      "/spawnFrontEnd": function (req, res) {
        var args = req.body;
        if (   args === undefined
            || args.type === undefined
            || args.port === undefined) {
          res.send(400);
          return;
        }
        try {
          fireSpawnFrontEnd(args.type, args.port);
          res.statusCode = 201;
          res.send(makeBaseUrl(req) + "/frontends/" + args.port);
        }
        catch (e) {
          res.statusCode = 500;
          res.send("Failed to spawn front-end.");
        }
      },
      "/killFrontEnd": function (req, res) {
        var args = req.body;
        if (   args === undefined
            || args.type === undefined
            || args.port === undefined) {
          res.send(400);
          return;
        }
        try {
          fireKillFrontEnd(args.type, args.port);
          res.statusCode = 200;
        }
        catch (e) {
          res.statusCode = 500;
        }
      }
    }
  };

  function fireSpawnFrontEnd(type, port) {
    self.emit("spawnFrontEnd", type, port);
  };

  function fireKillFrontEnd(type, port) {
    self.emit("killFrontEnd", type, port);
  };

  function makeBaseUrl(request) {
    return "http://" + request.headers.host;
  };

  this.handleFrontEndSpawned = function (type, frontend, port) {
    frontends[port] = { "type": type, "frontend": frontend };
  };

  this.handleFrontEndKilled = function (type, frontend, port) {
    delete frontends[port];
  };

  this.handleEchoMessage = function (message) {
    stats.echos++;
    stats.total++;
  };

  this.handleBroadcastMessage = function (message) {
    stats.broadcasts++;
    stats.total++;
  };

  this.handleEchoBroadcastMessage = function (message) {
    stats.echobroadcasts++;
    stats.total++;
  };

  this.handleRelayMessage = function (message) {
    stats.relays++;
    stats.total++;
  };

  this.start = function (port) {
    for (var method in resources) {
      for (var uri in resources[method]) {
        app[method](uri, resources[method][uri]);
      }
    }
    app.listen(port);
    log.print_debug("Now accepting HTTP requests on port "+port+".");
  };

  this.stop = function (port) {
    app.close();
    log.print_debug("No longer accepting HTTP requests on port "+port+".");
  };
};
util.inherits(RestInterface, EventEmitter);

module.exports = RestInterface;
