/**
  WebSocket protocol front-end.
  @package SynchronizedPlayback
  @subpackage server
  @version 1.0.0
  @author Chris Eineke
  @copyright 2011 EK3 Technologies Inc.
 */
var WSServer = require("websocket-server");
var Message = require("../wmsg").Message;
var events = require("events");
var util = require("util");
var log = require("../log");
var sys = require("sys");

/**
  Constructs a new instance of WebSocketFrontEnd.
  @emit message The front-end has a new message for processing.
  @param codec What message codec to use.
 */
function WebSocketFrontEnd(codec) {

  this.constructor.super_.call(this);
  var self = this;

  var server = WSServer.createServer();
  var curConnection = null;

  /**
    Event handler when a WebSocket client has sent data.
    @param connection The connection.
    @param frame The frame.
   */
  function handleConnectionMessage(connection, frame) {
    try {
      curConnection = connection;
      var message = codec.decodeMessage(frame);
      if (message == null) {
        throw new Error("Not a message.");
      }
      message.source = connection.id;
      message = Message.copy(message);
      self.emit("message", message);
    }
    catch (e) {
      curConnection.close();
      log.print_debug("Web socket server: Connection to client #"+connection.id+" closed due to unparseable message.");
      log.print_debug("Web socket server: Contents of message: "+sys.inspect(message));
      log.print_debug(e.toString());
    }
    finally {
      curConnection = null;
    }
  };

  /**
    Event handler when a client has encountered an error.
    @param error The error.
   */
  function handleConnectionError(connection, error) {
    connection.close();
    log.print_debug("Web socket server: Connection to client #"+connection.id+" closed due to error: "+error);
  };

  /**
    Event handler when a new WebSocket client has connected.
    @param connection The new connection.
   */
  function handleServerConnection(connection) {
    log.print_debug("Web socket server: Client #"+connection.id+" connected.");
    connection.on("message", function (message) {
        handleConnectionMessage(connection, message);
    });
    log.print_debug("Web socket server: Attached message listener to #"+connection.id+".");
    connection.on("error", function (error) {
        handleConnectionError(connection, error);
    });
    log.print_debug("Web socket server: Attached error listener to #"+connection.id+".");
  };

  /**
    Event handler when a WebSocket client has disconnected.
    @param connection The connection.
   */
  function handleServerDisconnect(connection) {
    log.print_debug("Web socket server: Client #"+connection.id+" disconnected.");
  };

  /**
    Starts the front-end on the specified port.
    @param port The port to listen on.
   */
  this.start = function (port) {
    server.listen(port);
    log.print_debug("Now accepting WebSocket connections on port "+port+".");
  };

  /**
    Stop the front-end.
   */
  this.stop = function (port) {
    server.close();
    log.print_debug("No longer accepting WebSocket connections on port "+port+".");
  };

  /**
    Echoes the message back to the sending client.
   */
  this.handleEchoMessage = function (message) {
    server.send(message.source, codec.encodeMessage(message));
  };

  /**
    Broadcasts the message to every connection, excluding the sender.
   */
  this.handleBroadcastMessage = function (message) {
    var data = codec.encodeMessage(message);
    server.manager.forEach(function (connection) {
        if (connection.id !== message.source) {
          connection.send(data);
        }
      });
  };

  /**
    Broadcasts the message to every connection, including the sender.
   */
  this.handleEchoBroadcastMessage = function (message) {
    server.broadcast(codec.encodeMessage(message));
  };

  /**
    Relays the message from one connection to another.
   */
  this.handleRelayMessage = function (message) {
    var targets = message.target;
    if (typeof targets == "string") {
      targets = [targets];
    }
    var data = codec.encodeMessage(message);
    targets.forEach(function (target) {
        server.send(target, data);
      });
  };

  /**
    Event handler when the dispatcher has encountered an error.
   */
  this.handleDispatcherError = function (error) {
    /*
       curConnection is only set if we are the front-end that invoked
       dispatchMessage. If not, then another front-end is the source of the
       error. And we ain't not handlin' that!
     */
    if (curConnection == null) {
      return;
    }
    server.find(curConnection.id, function (connection) {
        connection.close();
        log.print_debug("Web socket server: Disconnected client #"+connection.id+" due to invalid message.");
        log.print_debug("\tContents: "+sys.inspect(message));
    });
  };

  server.on("connection", handleServerConnection);
  server.on("disconnect", handleServerDisconnect);
};
util.inherits(WebSocketFrontEnd, events.EventEmitter);

module.exports = WebSocketFrontEnd;
