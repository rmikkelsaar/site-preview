#!/bin/bash

if [ "$1" = "" ]; then
  echo "The first argument must be the destination directory."
  exit
fi

mkdir -p $1
if [ $? -eq 1 ]; then
  echo "Couldn't create directory $1"
  exit
fi

cp -R wmsgd src lib $1
find $1 -name '.git' -or -name '.svn' | xargs rm -rf 
echo "Files installed in $1"
cp distros/ubuntu/wmsgd.conf /etc/init
echo "Service wmsgd ready to be started"
