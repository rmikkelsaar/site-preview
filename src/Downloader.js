/**
 * Copyright 2015 EK3 Technologies Inc.
 */

var request = require('request'),
	winston = require('winston'),
	when = require('when'),
	http = require('http'),
	fs = require('fs'),
	execSync = require('exec-sync');

/**
 * @param {string} base url for document downloading
 * Downloader will be responsible for all document downloading
 */
function Downloader(url) {
	this.url = url;
	this.path = './public/downloaded_assets/';
}

/**
 * getDocument and save to disk
 * @param {string} assetID
 * @param {string} type of document i.e playscript, location, realm, mz etc.
 * @return {object} returns response to the download including the document, status code and message
 * Responsible for the web request for this document
 */
Downloader.prototype.getDocument = function(assetID, type) {
	var response = new Object(),
		self = this,
		d = when.defer();

	var url = self.url + '/' + type + '/' + assetID;

	response.statusCode = -1;
	response.message = "Empty return message, something went wrong";

	winston.debug('Called Downloader::getDocument(', assetID,type, ') Called:', url);

	request(url, function(err, res, body) {
		if( err ) {
			response.statusCode = 404; // res.statusCode does not exist for this error
			response.error = err;
			response.message = "Could not reach server.";
			d.reject(response);
		} else if ( res.statusCode !== 200 ) {
			response.statusCode = res.statusCode;
			response.message = 'Error finding document.';
			d.reject(response);
		} else {			
			if( body === "" ) { // Empty Document		
				response.statusCode = 204; // Request responds with a 200 when the RESTcall finds nothing
				response.message = "Error downloading location document, does not exist. Attempt: " + url;
				d.reject(response);
			} else {				
				response.statusCode = res.statusCode;
				response.message = "Download Successfull: Location Document " + url;
				response.obj = body;
				d.resolve(response); // Resolve promise if all is good
			}
		}
	});	

	return d.promise;
};

/**
 * getMedia and save to disk
 * @param {string} assetID
 * @return {object} returns response to the
 * Responsible for the download of this media id, 
 */
Downloader.prototype.getMedia = function( assetID ) {
	var response = new Object(),
		self = this,
		d = when.defer();
	var filePath = self.path + assetID + '_tmp';
	var folderPath = self.path + assetID;
    

	try {		
		var file = fs.createWriteStream(filePath);
		var url = self.url + assetID;
		http.get(url, function(response) {
			response.on('data', function(data) {
				file.write(data);
			}).on('end', function() {
				try {
					file.end();
					setTimeout(function() { // Allow the missing callback to completely finish for file.end()
						winston.debug('Downloader::getMedia() Successfully downloaded assetID', assetID, 'to', self.path);
						fs.mkdirSync(folderPath);
						var cmd = 'unzip ' + filePath + ' -d ' + folderPath;
						winston.debug('Running command', cmd);
						execSync(cmd);
						fs.unlinkSync(filePath);
						d.resolve();

						// Link sublinks
						for( i=0; i<mzCount; i++ ) {
							
						}
					}, 500);
				} catch (e) {
					winston.error('Extracting zip file', e);
					d.reject();
				}
			}).on('error', function(error) {
				winston.error('Downloader::getMedia() Download', url, error);
				d.reject(error);
			});
		});
		

	} catch( error ) {
		winston.error('Downloader::getMedia()', error);
		d.reject(error);
	}
	return d.promise;
};

/**
 * @param {string} Path to zip file
 * @param {string} Path to unzip location
 * unzipFile is used to repeatedly attempt tp unzip a file based on a threshold of attempts
 */
Downloader.prototype.unzipFile = function(filePath, folderPath) {

};


module.exports = Downloader;