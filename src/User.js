/**
 * Copyright 2015 EK3 Technologies Inc.
 */

var util = require('util');
/**
 * User
 * Class for storing session information
 * @param {string} UID of socket for user
 */
function User(sock) {
	this.sockID = sock;
	this.referer = "";
	this.documents = new Object();
	this.media_uri = "";
	this.document_url = "";
	this.tenant_id = "";
	this.is_orchestration = false;
};

/**
 * toString
 * Prints all relevant information
 */
User.prototype.toString = function() {
	console.log('\nUser:', this.sockID);
	console.log('Referer:', this.referer);
	console.log('DocumentURL:', this.document_url);
	// console.log('Documents: \n', util.inspect(this.documents));
};

module.exports = User;