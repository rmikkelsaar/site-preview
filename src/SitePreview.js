/**
 * Copyright 2015 EK3 Technologies Inc.
 */

var winston = require('winston'),
	when = require('when'),
	Manager = require('./Manager'),
	util = require('util'),
	fs = require('fs'),
	url = require('url'),
	http = require('http'),
	User = require('./User');

http.globalAgent.maxSockets = 10;

/**
 * SitePreview, the root application
 * @param {module} Socket.io module
 * @param {module} Express App module 
 */
function SitePreview(_io, _app, _http) {
	if( !_io || !_app || !_http ) {
		winston.debug('Invalid parameters to instantiate SitePreview');
		return null;
	}
	this.io = _io;
	this.app = _app;
	this.http = _http;
};


/**
 * Sets up all restful interface calls for generic 
 * requests for any user
 */
SitePreview.prototype.initialize = function(_env) {
	winston.debug('SitePreview::initializeSitePreview()');
	var self = this;
	self.manager = new Manager(_env);
	self.InitializeRestful();
	self.InitializeSocket();
	self.ScanDownloadedFiles();	
};

/**
 * ScanDownloadedFiles
 * Scans the public downloaded file folder to update the list of currently downloaded assets
 */
SitePreview.prototype.ScanDownloadedFiles = function() {
	var self = this;

	self.manager.downloaded_assets = fs.readdirSync('./public/downloaded_assets/');
	if( self.manager.downloaded_assets.length > 0 ) {
		winston.debug('Found downloaded assets', self.manager.downloaded_assets);
	}
};

/**
 * Initialize Socket handling for users
 */
SitePreview.prototype.InitializeSocket = function() {
	winston.debug('SitePreview::InitializeSocket()');
	var self = this;

	// New Connection
	self.io.on('connection', function(socket) {

		// Browsers that have this application cached and type the url but do not hit enter
		// Some browsers will preemptively start the socket connection, but do not call disconnect
		// If that user does not enter the site
		socket.on('fully connected', function() {
			winston.info('New User Connected. UserID:', socket.id);
			var newUser = new User(socket.id);
			newUser.referer = socket.handshake.headers.host;
			self.manager.users.push(newUser);
			winston.debug('Number of users currently connected:', self.manager.users.length);
			self.ScanDownloadedFiles();
		});

		// Disconnection event
		socket.on('disconnect', function() {
			winston.info('User has disconnected');
			// This will remove the user from the array and destroy its documents
			self.manager.removeUser(socket);
			winston.debug('Number of users currently connected:', self.manager.users.length);
		});
	});
};




/**
 * InitializeRestful will set up all redirects and restfull api for the application
 */
SitePreview.prototype.InitializeRestful = function() {
	winston.debug('SitePreview::InitializeRestful()');
	var self = this;

	/**
	 * RESTful web call for location
	 * @param {string} Tenant
	 * @param {int} Location ID
	 */
	self.app.get('/location', function(req, res) {
		var locationID = req.query.location,
			tenant = req.query.tenant,
			sockID = req.query.sock_id;

		if( typeof(locationID) !== 'undefined' || typeof(tenant) !== 'undefined' ) {
			self.manager.getLocation(tenant, locationID, sockID).then(function(data) {
				res.status(200).send(data);
			}, function(data) { // Error
				res.status(200).send(data);
			});
		} else {
			res.status(200).send({ 'statusCode': 500, 'message': 'Invalid or missing location and tenant ID' });	
		}
	});

	/**
	 * RESTful web call selecting connection ID
	 * @param {}
	 */
	self.app.get('/preview', function(req, res) { 

		var connectionID = req.query.connection,
			sockID = req.query.sock_id,
			returnSet = null;
		winston.info('User', sockID, ' viewing connection:', connectionID);
		if( typeof(connectionID) !== 'undefined' || connectionID !== "" ) { // Check for null values
			self.manager.getDocuments(connectionID, sockID).then(function(data) {
				returnSet = data;
				return self.manager.getCompressedFiles( sockID );
			}).then(function() {
				self.ScanDownloadedFiles(); // Rebuild list of downloaded assets
				res.status(200).send(returnSet);
			}).catch(function(error) { // Error
				winston.error('SitePreview::Preview', error);
				error.statusCode = 404;
				error.message = error;
				res.status(200).send(error);
			});
		} else { // No connection ID Provided
			res.status(200).send({ 'statusCode': 500, 'message': 'No connection document' });
		}
	});

	// Media.json Request
	self.app.get('/media.json', function(req, res) {
		try {
			winston.debug('media.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].media);
		} catch (e) {
			winston.error('Error requesting media.json from', req.headers.referer, e );
			res.status(500).send("Error getting media.json");
		}
	});

	// Playlist.json Request
	self.app.get('/playlist.json', function(req, res) {
		try {
			winston.debug('playlist.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].playlist);
		} catch (e) {
			winston.error('Error requesting playlist.json from', req.headers.referer, e );
			res.status(404).send("Error getting playlist.json");
		}		
	});

	// Config_VGA-0.json Request
	self.app.get('/config_VGA-0.json', function(req, res) {
		try {
			winston.debug('config_VGA-0.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;


			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];				
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].config_vga);
		} catch (e) {
			winston.error('Error requesting config_VGA-0.json from', req.headers.referer, e );
			res.status(404).send("Error getting config_VGA-0.json");
		}	
	});

	// Config.json Request
	self.app.get('/config.json', function(req, res) {
		try {
			winston.debug('config.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].config);	
		} catch (e) {
			winston.error('Error requesting config.json from', req.headers.referer, e );
			res.status(404).send("Error getting config.json");
		}		
	});

	// Schedule.json Request
	self.app.get('/schedule.json', function(req, res) {
		try {
			winston.debug('schedule.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].schedule);
		} catch (e) {
			winston.error('Error requesting schedule.json from', req.headers.referer, e );
			res.status(404).send("Error getting schedule.json");
		}		
	});

	// Time_to_play.json Request
	self.app.get('/time_to_play.json', function(req, res) {
		try {
			winston.debug('time_to_play.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].time_to_play);	
		} catch (e) {
			winston.error('Error requesting time_to_play.json from', req.headers.referer, e );
			res.status(404).send("Error getting time_to_play.json");
		}		
	});

	// Region.json Request
	self.app.get('/region.json', function(req, res) {
		try {
			winston.debug('region.json Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[_screen - 1].region);
		} catch (e) {
			winston.error('Error requesting region.json from', req.headers.referer, e );
			res.status(404).send("Error getting region.json");
		}		
	});
	
	// Index-0.html Requests
	self.app.get('/index-0.html', function(req, res) {
		try {
			winston.debug('index-0.html Request:', req.headers.referer);
			var url_parts = url.parse(req.headers.referer, true);
			var query = url_parts.query;
			var _screen = 0;
			if( typeof(query.screen) !== 'undefined' ) {
				_screen = query.screen;
			} else {
				_screen = req.headers.referer[req.headers.referer.length - 1];
			}
			res.status(200).send(self.manager.users[0].documents.mzs[0].index);
		} catch (e) {
			winston.error('Error requesting index-0.html from', req.headers.referer, e );
			res.status(404).send("Error getting index-0.html");
		}		
	});

	// All media request is run through here, so we can parse for zip files and host them seperately
	self.app.get('/media/*', function(req, res) {
		try {
			console.log('Receiving media request', req._parsedOriginalUrl.path );
			var paths = req._parsedOriginalUrl.path.split('/');
			var mediaID = "",
				mediaURL = "",
				sockID = "";

			if( paths[paths.length - 1] === 'index.html' ) { // Check if some things reference index instead of the ID
				paths.splice(paths.length - 1, 1);			
			}

			_screen = paths[2][6]; // Get screen number
			sockID = paths[3]; // Socket ID
			mediaID = paths[4]; // Asset ID

			if( typeof(mediaID) === 'undefined' ) {
				mediaID = paths[paths.length -1];
			}

			if( self.manager.downloaded_assets.indexOf(mediaID) >= 0 ) { // Is a downloaded asset, redirect to root
				mediaURL = req.headers.referer.split('screen')[0] + mediaID + '?screen=' + _screen + '&sock_id=' + sockID;
				winston.debug(req.headers.referer, 'Requesting downloaded zip file', req._parsedOriginalUrl.path, 'Redirecting ->', mediaURL);
				res.redirect(mediaURL);	
			} else {
				mediaURL = self.manager.users[0].media_uri + mediaID;
				winston.debug(req.headers.referer, 'Requesting media', req._parsedOriginalUrl.path, 'Redirecting ->', mediaURL);				
				
				// Check for syncrhronized Locations
				// var user = self.manager.getUser(sockID);
				// if( user && user.is_orchestration ) {
				var data = [];
				http.get(mediaURL, function(response) {
					response.on('data', function(partial) {
						data.push(partial);
					}).on('end', function() {
						console.log('Data download finished', req._parsedOriginalUrl.path);
						var buffer = Buffer.concat(data);
						res.status(200).send(buffer);
					});
				});	
				// } else {
				// 	res.redirect(mediaURL);
				// }
			}
			
		} catch (e) {
			winston.error('Error requesting media file', req.headers.referer, e );
			res.status(404).send("Error getting region.json");
		}
	});
		
	// Screen requests from iframes
	self.app.get('/screen*', function(req, res) {
		try {
			res.status(200).send(self.manager.users[0].documents.mzs[0].index);
		} catch (e) {
			winston.error('Error requesting screen', req.headers.referer, e );
			res.status(404).send("Error getting media.json");
		}
	});

	// Requests for menuboardv2.xml from content and orchestrator
	self.app.get('/opt/data/*', function(req, res) {
		try {
			var requestedPath = req._parsedOriginalUrl.pathname.split('/');
			var dataFile = requestedPath[requestedPath.length - 1];
			var redirectPath = 'http://' + req.headers.host + '/'  + dataFile;
			winston.debug(req.headers.referer, 'Request for menuboard file', dataFile, 'redirecting ->', redirectPath);

			res.redirect(redirectPath);
		} catch (e) {
			winston.error('Error requesting menuboard data', e);
		}
	});

	// Requests for menuboardv2 used frmo legacy and socket.io
	self.app.get('/support*', function(req, res) {
		try {
			var url_parts = url.parse(req._parsedOriginalUrl.path, true);
			var query = url_parts.query;
			var dataFile = query.dataname;
			
			var redirectPath = 'http://' + req.headers.host + '/'  + dataFile + '.xml';
			winston.debug(req.headers.referer, 'Request for /support/Data.php?dataname= menuboard file', dataFile, 'redirecting ->', redirectPath);

			res.redirect(redirectPath);
		} catch (e) {
			winston.error('Error requesting menuboard data from /support/Data.php?dataname=', e);
		}
	});

	//TODO
	// ADD View Module
	// ADD user based requests
	// ADD more tools such as checkusers

	self.app.get('/checkusers', function(req, res) {
		// self.manager.printUsers();
		res.status(200).send(self.manager.users);
	});
};

/**
 * Start the server
 */
SitePreview.prototype.start = function() {
	this.http.listen(3000, function() {
		winston.info('SitePreview running on port 3000');
	});	
};


module.exports = SitePreview;